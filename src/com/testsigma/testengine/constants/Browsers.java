package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum Browsers {
	INTERNETEXPLORER(1, "INTERNETEXPLORER", "Internet Explorer"),
	FIREFOX(1, "FIREFOX", "Mozilla Firefox"),
	GOOGLECHROME(1, "GOOGLECHROME", "Google Chrome"),
	SAFARI(1, "SAFARI", "Safari"),
	EDGE(1, "EDGE", "Edge");
		
	private Integer id;
	private String key;
	private String dispName;
	
	Browsers(Integer id, String key, String dispName){
		this.id = id;
		this.key = key;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	
	public String getKey(){
		return key;
	}
	
	public static Browsers getBrowser(String key){
		switch(key){
			case "INTERNETEXPLORER":
				return Browsers.INTERNETEXPLORER;
			case "FIREFOX":
				return Browsers.FIREFOX;
			case "GOOGLECHROME":
				return Browsers.GOOGLECHROME;
			case "SAFARI":
				return Browsers.SAFARI;
			case "EDGE":
				return Browsers.EDGE;
		}
		return null;
	}
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(Browsers type : Browsers.values()){
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}
	
	public static Map<String, String> getKeyVsDispNameMap(){
		Map<String, String> toReturn = new HashMap<String, String>();
		for(Browsers type : Browsers.values()){
			toReturn.put(type.getKey(), type.getDispName());
		}
		return toReturn;
	}
}
