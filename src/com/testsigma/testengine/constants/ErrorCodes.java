package com.testsigma.testengine.constants;

public class ErrorCodes {
	public static Integer UNKNOWN_BROWSER=2000;
	public static Integer ALL_WINDOWS_NOT_CLOSED= 2001;
	public static Integer BROWSER_CLOSED=2002;
	public static Integer USER_STOPPED_EXECUTION=2003;
	public static Integer DRIVER_NOT_CREATED=2004;
	public static Integer INVALID_UI_IDENTIFIER=2005;
	public static Integer EMPTY_UI_IDENTIFIER=2006;
}
