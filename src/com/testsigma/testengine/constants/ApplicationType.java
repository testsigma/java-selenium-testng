package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum ApplicationType {
	WebApplication(1, "Web Application"),
	AndroidWeb(2, "Android Web Application"),
	AndroidNative(3, "Android Native Application"),
	AndroidHybrid(4, "Android Hybrid Application"),
	IOSWeb(5, "iOS Web Application"),
	IOSNative(6, "iOS Native Application"),
	IOSHybrid(7, "iOS Hybrid Application"),
	Rest(8, "RESTful Web Service"),
	Database(9, "Database");
	
	private Integer id;
	private String dispName;
	
	ApplicationType(Integer type, String dispName){
		this.id = type;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	
	public static ApplicationType getType(Integer id){
		switch(id){
			case 1:
				return WebApplication;
			case 2:
				return AndroidWeb;
			case 3:
				return AndroidNative;
			case 4:
				return AndroidHybrid;
			case 5:
				return IOSWeb;
			case 6:
				return IOSNative;
			case 7:
				return IOSHybrid;
			case 8:
				return Rest;
			case 9:
				return Database;
		}
		return null;
	}
	

	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(ApplicationType type : ApplicationType.values()){
			if(!(type.equals(AndroidHybrid) || type.equals(IOSHybrid) || type.equals(Database))){
				toReturn.put(type.getId(), type.getDispName());	
			}
		}
		return toReturn;
	}

}
