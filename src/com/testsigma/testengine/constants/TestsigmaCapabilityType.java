package com.testsigma.testengine.constants;

import org.openqa.selenium.remote.CapabilityType;

public interface TestsigmaCapabilityType extends CapabilityType {
	String APPIUM_URL = "appiumUrl";
}
