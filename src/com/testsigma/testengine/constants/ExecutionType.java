package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum ExecutionType {
	Grid(1, "Selenium Grid"),
	Local(3, "Local"),
	SauceLabs(6, "Sauce Labs"),
	BrowserStack(7, "BrowserStack");
	
	private Integer id;
	private String dispName;
	
	ExecutionType(Integer type, String dispName){
		this.id = type;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	
	public static ExecutionType getType(Integer id){
		switch(id){
		case 1:
				return Grid;
			case 3:
				return Local;
			case 6:
				return SauceLabs;
			case 7:
				return BrowserStack;
		}
		return null;
	}
	
	
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(ExecutionType type : ExecutionType.values()){
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}

}
