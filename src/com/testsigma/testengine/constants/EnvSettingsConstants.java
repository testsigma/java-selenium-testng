package com.testsigma.testengine.constants;

public class EnvSettingsConstants {

	public static final String KEYS_BROWSER_VERSION_FOUND = "browser_version_found";
	private static final String KEYS_BROWSER = "browser";
	private static final String KEYS_BROWSER_VERSION = "browserVersion";
	private static final String KEYS_PLATFORM = "platform";
	private static final String KEYS_PLATFORM_VERSION = "osVersion";
	private static final String KEYS_TESTOBJECT_API_KEY = "testobjectApiKey";
	public static final String KEYS_SAUCELABS_URL = "saucelabs_url";
	private static final String KEY_BROWSERSTACK_URL = "browserstack_url";
	private static final String KEY_BROWSERSTACK_USERNAME = "browserstack_username";
	private static final String KEY_BROWSERSTACK_PASSWORD = "browserstack_password";
	private static final String KEY_APK_PATH = "apkPath";
	private static final String USER_NAME = "agent";
	private static final String USER_NAME_KEY = "user_name";
	private static final String PASSWORD = "testsigma";
	private static final String PASSWORD_KEY = "password";
	private static final String KEYS_BUNDLE_ID = "bundleId";
	
	private static final String ELEMENT_TIMEOUT = "elementTimeout";
	private static final String PAGE_TIMEOUT = "pageTimeout";
	private static final String SCREENSHOT_PATH= "screenshotPath";
	private static final String GLOBAL_PARAMETER= "globalParameter";
	private static final String RECOVERY_ACTIONS= "recoveryActions";
	private static final String ABORTED_ACTIONS= "abortedActions";
	private static final String APPLICATION_TYPE= "applicationType";
	private static final String BROWSER_PATH= "browserPath";
	private static final String EXECUTION_TYPE= "executionType";
	

	public static String getSaucelabsUrl(String username, String key){
		return "https://" + username + ":" + key + "@ondemand.saucelabs.com:443/wd/hub";
		
	}
	
	public static String getBrowserStackUrl(String username, String key){
		return "https://" + username + ":" + key + "@hub-cloud.browserstack.com/wd/hub";
		
	}
}
