package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum PreRequisiteFailActions {
	Abort(1, "Stop Execution"),
	Continue(2,"Report and continue ");
	
	private Integer id;
	private String dispName;
	
	PreRequisiteFailActions(Integer type, String dispName){
		this.id = type;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	public static PreRequisiteFailActions getActions(Integer id){
		switch(id){
			case 1:
				return Abort;
			case 2:
				return Continue;
		}
		return null;
	}
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(PreRequisiteFailActions type : PreRequisiteFailActions.values()){
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}

}
