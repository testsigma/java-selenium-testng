package com.testsigma.testengine.constants;

public class ResultConstants {
	
	public final static Integer illegalAcsses=1;
	public final static Integer invalidAgument=2;
	public final static Integer invocationTarget=3;
	public final static Integer invalidMethod=4;
	public final static Integer invalidCredentials=5;
	public final static Integer unKnownProblem=6;
	
	public final static Integer SUCCESS = 0;
	public final static Integer FAILURE = 1;
	public final static Integer ABORTED = 2;
	public final static Integer NOT_EXECUTED = 3;
	public final static Integer PRE_REQUISITE_FAILURE = 4;
	public final static Integer QUEUED = 5;
	public final static Integer STOPPED = 6;
	
	public final static Integer ERROR_NA = 0;
	public final static Integer ERROR_UNKNOWN_BROWSER = 2000;
	public final static Integer ALL_ENVIRONMENTS_FAILED = -1;
	
}
