package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum OnAbortActions {
	Reuse_Session(1, "Delete All Cookies & Reuse Current Session"),
	Restart_Session(2,"Restart Session");
	
	private Integer id;
	private String dispName;
	
	OnAbortActions(Integer type, String dispName){
		this.id = type;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	public static OnAbortActions getActions(Integer id){
		switch(id){
			case 1:
				return Reuse_Session;
			case 2:
				return Restart_Session;
		}
		return null;
	}
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(OnAbortActions type : OnAbortActions.values()){
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}

}
