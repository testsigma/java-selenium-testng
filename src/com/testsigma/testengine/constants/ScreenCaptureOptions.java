package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum ScreenCaptureOptions {	
	
	NONE(0,"None"),
	ALL_TYPES(1,"All Steps"),
	MANDATORY(2,"Mandatory"),
	FAILED_STEPS(3,"Failed Steps");
	
	private Integer id;
	private String displayName;
	ScreenCaptureOptions(Integer id,String displayName){
		this.id = id;
		this.displayName=displayName;
	}
	
	public static ScreenCaptureOptions getAccessLevel(Integer id){
		switch(id){
			case 1:
				return NONE;
			case 2:
				return ALL_TYPES;
			case 3:
				return MANDATORY;
			case 4:
				return FAILED_STEPS;
		}
		return null;
	}
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(ScreenCaptureOptions type : ScreenCaptureOptions.values()){
			toReturn.put(type.getId(), type.getDisplayName());
		}
		return toReturn;
	}

	public Integer getId(){
		return id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
