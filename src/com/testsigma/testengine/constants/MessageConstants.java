package com.testsigma.testengine.constants;

public class MessageConstants {
		
	public final static String MSG_SEPARATOR = ";";
	public final static String MSG_UNKNOWN_BROWSER = "Unknown Browser.....";
	public final static String MSG_SUCCESS = "Execution initiated";
	public final static String MSG_EXECUTION_SUCCESS = "Execution completed";
	public final static String MSG_ENVIRONMENT_SUCCESS = "Environment execution completed";
	public final static String MSG_STEP_NOT_EXECUTED = "Teststep not executed";
	public final static String MSG_STEP_PRE_REQUISITE_FAILED = "Teststep prerequisite failed at step no ";
	public final static String MSG_CASE_PRE_REQUISITE_FAILED = "Testcase prerequisite failed";
	public final static String MSG_GROUP_PRE_REQUISITE_FAILED = "Testcase group prerequisite failed";
	public final static String MSG_STEP_MAJOR_ABORTED = "Testcase execution aborted on major teststep failure";
	public final static String MSG_TEST_CASE_SUCCESS = "Testcase executed successfully";
	public final static String MSG_TEST_CASE_FAILURE = "Testcase execution failed. Check step results for more details";
	public final static String MSG_GROUP_SUCCESS = "Group executed successfully";
	public final static String MSG_GROUP_FAILED = "Group execution failed";
	public final static String MSG_STEP_SUCCESS = "Teststep executed successfully";
	public final static String MSG_STEP_FAILURE = "Teststep execution failed";
	public final static String MSG_USER_ABORTED_EXECUTION = "User stopped the execution";
	public final static String MSG_COMPONET_SUCCESS = "Test component executed successfully";
	public final static String MSG_COMPONET_FAILURE = "Test component execution failed";
	public final static String EXCEPTION_UNKNOWN_BROWSER = "Browser name given is unknown : \"?1\"";
	public final static String EXCEPTION_FAILED_TOCREATE_WEBDRIVER = "Error in creating webdriver instance for browser :: \"?1\"";
	public static final String EXCEPTION_DRIVER_NOTFOUND = "Browser Driver not found - Error in starting the ?1 session";
	public static final String EXCEPTION_WEBDRIVER_NOTCREATED = "Exception in initiating a browser session in path:, ?1";
	public static final String EXCEPTION_WEBDRIVER_NOTFOUND = "Webdriver session not found for executionID \"?1\"";	
	public static final String EXCEPTION_WEBDRIVER_RESET_SESSION_FAILED = "Reset WebDriver Session has failed. The webDriver session with executionID \"?1\" is not found";
	public final static String EXCEPTION_UNKNOWN_TESTDATA = "Runtime data not found with name : \"?1\"";
	
	public static final String EXCEPTION_VERIFICATION_FAILURE_STRINGS_EQUAL_FAILURE ="Failed to verify values are equal. \"" +"?1" + "\"" + " and " +"\"" + "?2" +"\"" + " are not equal.";
	public static final String EXCEPTION_VERIFICATION_FAILURE_STRINGS_NOT_EQUAL_FAILURE ="Failed to verify values are not equal. \"" +"?1" + "\"" + " and " +"\"" + "?2" +"\"" + " are equal.";
	public static final String EXCEPTION_VERIFICATION_FAILURE_VERIFYTRUE_BOOLEAN_FAILURE = "Failed to verify the condition returns true";
	public static final String EXCEPTION_VERIFICATION_FAILURE_VERIFYFALSE_BOOLEAN_FAILURE = "Failed to verify the condition returns false";
	
	public static final String EXCEPTION_ELEMENT_NOT_VISIBLE = "The element corresponding to the locator type \""+"?1"+"\" and locator \""+"?2"+"\" is hidden.";
	public static final String EXCEPTION_ELEMENT_INVALID_SELECTOR = "The element corresponding to the locator type \""+"?1"+"\" and locator \""+"?2"+"\" resulted in invalid selector exception.";
	public static final String EXCEPTION_ELEMENT_NOT_FOUND = "The element corresponding to the locator type \""+"?1"+"\" and locator \""+"?2"+"\" is not displayed.";
	public static final String EXCEPTION_ELEMENT_INVALID_STATE = "The element corresponding to the locator type \""+"?1"+"\" and locator \""+"?2"+"\" cannot be interacted with since it is either hidden or not enabled.";
	public static final String EXCEPTION_LOCATOR_STRATEGY_INVALID = "The locator type \""+"?1"+"\" corresponding to the element is no longer present in the page.";
	public static final String EXCEPTION_ELEMENT_STALE = "The element corresponding to the locator type \""+"?1"+"\" and locator \""+"?2"+"\" cannot be interacted with since it is either hidden or not enabled.";
	public static final String EXCEPTION_TEXT_NOT_FOUND ="Given text ?1 is not visible";
	
	
	public static final String EXCEPTION_ELEMENT_NOTCLICKABLE="Element is not clickable at point";
	public static final String EXCEPTION_ELEMENT_NOTCLICKABLE_EXCEPTION_MESSAGE=" \"" +"?1" + "\"" + " is not clickable. \"" +"?1" + "\"" + " is either blocked out of view or hidden";
	public static final String EXCEPTION_ELEMENT_SELECT_WRONG_TAG= "The element corresponding to the locator type \""+"?1"+"\" and locator \""+"?2"+"\" is not a select element.";
	public static final String EXCEPTION_ELEMENT_SELECT_OPTION_NOT_FOUND= "Option \"" + "?1"+"\" is not found in the list corresponding to locatortype \""+
			"?2" +"\" , and value \"" + "?3" + "\".";

	public static final String EXCEPTION_BROWSER_UNREACHABLE = "The browser connection is lost. Either the browser is closed by the user or the connection is terminated. Session will be reset.";
	
	public static final String EXCEPTION_WEBDRIVER_UNKNOWN = "Unknown WebDriver exception , Details :\n  ?1";
	public static final String EXCEPTION_INTERRUPTED_WAIT = "Thinking time is interrupted , details :\n ?1";
	public static final String EXCEPTION_METHOD_NOT_FOUND = "No implementation found for this template";
	public static final String EXCEPTION_INVALID_TESTDATA = "Invalid Testdata name ?1";
	public final static String INVALID_SERVER_CREDENTIALS="Invalid server credentials provided";

	public final static String TS_EXCEPTION_ = "";
	
	public static final String INTERNET_EXPLORER = "Internet Explorer";
	public static final String GOOGLE_CHROME = "Google Chrome";
	public static final String MOZILLA_FIREFOX = "Mozilla Firefox";
	public static final String EDGE = "Edge";
	public static final String MSG_REST_ERROR_STATUS = "Response status is not equal to the expected response";
	public static final String MSG_REST_ERROR_HEADERS = "Response headers doesn't match with the expected headers";
	public static final String MSG_REST_ERROR_CONTENT = "Response content doesn't match with the expected content";
	public static final String MSG_REST_ERROR_PATH = "Content for path ?1 doesn't match with the expected value";
	public static final String MSG_REST_ERROR_BODY_EMPTY = "No Test body provided to validate response";
	
	public static final String MSG_REST_SCHEMA_ERROR_EMPTY = "No schema provided to validate response";
	
	final public static String EMPTY_TESTDATA="nlpstep.message.empty.testdata.teststep";
	final public static String EMPTY_UI_IDENTIFIER="nlpstep.message.empty.uiidentifier.teststep";
	final public static String EMPTY_ATTRIBUTE="nlpstep.message.empty.attribute.teststep";
	final public static String EMPTY_FROM_UI_IDENTIFIER="nlpstep.message.empty.fromuiidentifier.teststep";
	final public static String EMPTY_TO_UI_IDENTIFIER="nlpstep.message.empty.touiidentifier.teststep";
	
	public static final String MSG_TESTCASE_FAILED = "TestCase  execution failed";
	public static final String MSG_TESTCASE_SUCCESS = "TestCase  execution successful";
	
}
