package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum TeststepType {
	
	NLP_TEXT(1, "NLP"),
	STEP_GROUP(2, "Step Group"),
	CUSTOM_FUNCTION(3, "Function");
	
	private Integer id;
	private String dispName;
	
	TeststepType(Integer type, String dispName){
		this.id = type;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(TeststepType type : TeststepType.values()){
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}

}
