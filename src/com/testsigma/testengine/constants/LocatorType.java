package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum LocatorType {
	xpath(1,"xpath", "xpath"),
	csspath(2,"csspath", "css selector"),
	id_value(3,"id_value", "id"),
	name(4,"name", "name"),
	link_text(5,"link_text","link text"),
	partial_link_text(6,"partial_link_text","partial link text"),
	class_name(7,"class_name","class name"),
	tag_name(8,"tag_name", "tag name");
	
	private Integer id;
	private String dispName;
	private String actName;
	LocatorType(Integer type, String actName,String dispName){
		this.id = type;
		this.actName = actName;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	
	public String getActName(){
		return actName;
	}
	
	public static LocatorType getActions(Integer id){
		switch(id){
			case 1:
				return xpath;
			case 2:
				return csspath;
			case 3:
				return id_value;
			case 4:
				return name;
			case 5:
				return link_text;
			case 6:
				return partial_link_text;
			case 7:
				return class_name;
			case 8:
				return tag_name;
			
		}
		return null;
	}

	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(LocatorType type : LocatorType.values()){
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}
	public static Map<String, Integer> getIdVsDispNameMap(){
		Map<String, Integer> toReturn = new HashMap<String, Integer>();
		for(LocatorType type : LocatorType.values()){
			toReturn.put(type.getDispName().toLowerCase(), type.getId());
		}
		return toReturn;
	}
	
	public static String getDisplayNameById(Integer id){
		String value = "";
		for(LocatorType type : LocatorType.values()){
			if(type.getId().equals(id)){
				value = type.getDispName();
			}
		}
		return value;
	}
}
