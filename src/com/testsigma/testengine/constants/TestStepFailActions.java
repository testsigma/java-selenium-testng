package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum TestStepFailActions {
	Run_Next_Testcase(1, "Abort Testcase and Run Next Testcase"),
	Run_Next_Step(2,"Report Error and Run Next Step");
	
	private Integer id;
	private String dispName;
	
	TestStepFailActions(Integer type, String dispName){
		this.id = type;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	public static TestStepFailActions getActions(Integer id){
		switch(id){
			case 1:
				return Run_Next_Testcase;
			case 2:
				return Run_Next_Step;
		}
		return null;
	}
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(TestStepFailActions type : TestStepFailActions.values()){
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}

}
