package com.testsigma.testengine.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum Platforms {
	
	Generic(1, "Generic Agent", "Generic/Cloud Agent"),
	Windows(2, "Windows", "Windows"),
	Mac(3, "OS X", "Mac OS"),
	Linux(4, "Linux",  "Linux"),
	Android(5,"Android", "Android"),
	iOS(6, "iOS", "iOS");
	
	/*Windows7(1, "Windows", "7", "Windows 7"),
	Windows8(2, "Windows", "8", "Windows 8"),
	Windows8_1(3, "Windows", "8.1", "Windows 8.1"),
	Windows10(4,"Windows", "10", "Windows 10"),
	Linux(5, "Linux", "Linux", "Linux"),
	Mac10_12(6, "OS X", "10.12", "Sierra", "Mac OS 10.12"),
	Mac10_11(7, "OS X", "10.11", "El Capitan", "Mac OS 10.11"),
	Mac10_10(8, "OS X", "10.10", "Yosemite", "Mac OS 10.10"),
	Mac10_9(9, "OS X", "10.9", "Mavericks", "Mac OS 10.9"),
	Mac10_8(10, "OS X", "10.8", "Mountain Lion", "Mac OS 10.8"),
	Android(11,"Android", "5", "Android 5"),*/
	//iOS(12,"iOS", "iOS", "iOS");
	
/*	private static Map<Integer, String> saucelabsOSMap = new HashMap<Integer, String>();
	
	static{
		saucelabsOSMap.put(6, "macOS 10.12");
		saucelabsOSMap.put(7, "OS X 10.11");
		saucelabsOSMap.put(8, "OS X 10.10");
		saucelabsOSMap.put(9, "OS X 10.9");
		saucelabsOSMap.put(10, "OS X 10.8");
		
	}*/
	
	private Integer id;
	private String os;
	private String dispName;
	
	Platforms(Integer type, String os, String dispName){
		this.id = type;
		this.os = os;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	
	public String getOs() {
		return os;
	}

/*	public static String getSauceLabsOSString(Integer id){
		if(saucelabsOSMap.containsKey(id)){
			return saucelabsOSMap.get(id);
		} else {
			Platforms p = getPlatform(id);
			return p.getDispName();
		}
	}*/
	
	public static Platforms getPlatform(Integer id){

		switch(id){
			case 1:
				return Generic;
			case 2:
				return Windows;
			case 3:
				return Mac;
			case 4:
				return Linux;
			case 5:
				return Android;
			case 6:
				return iOS;
		}
		return null;
	}
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(Platforms type : Platforms.values()){
			if(type.equals(Generic)){
				continue;
			}
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}
	
	public static Integer getIdByName(String displayname){
		Integer ids = null;
		for(Platforms type : Platforms.values()){
			if(type.getDispName().equals(displayname)){
				return ids= type.getId();
			}
		};
		return ids;
	}
	
	public static Map<Integer, String> getWebDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(Platforms type : Platforms.values()){
			if(type.equals(Generic) || type.equals(iOS) || type.equals(Android)){
				continue;
			}
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}
/*	
	public static Map<Integer, String> getPlatformMapById(Integer platformId){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(Platforms type : Platforms.values()){
			if(type.id.equals(platformId))
			toReturn.put(type.getId(), type.getDispName());
		};
		return toReturn;
	}*/
	
	public static  List<Integer> getOsTypes(Integer appType){
		 List<Integer> arrlist = new ArrayList<Integer>();
		
		 if(ApplicationType.WebApplication.getId().equals(appType)
				 || ApplicationType.Rest.getId().equals(appType) 
				 || ApplicationType.Database.getId().equals(appType)) {
			 for(Platforms type : Platforms.values()){
				 arrlist.add(type.getId());
			 }
			 arrlist.remove(Android.getId());
			 arrlist.remove(iOS.getId());
		 }
		 
		 if(ApplicationType.AndroidWeb.getId().equals(appType) || 
				 ApplicationType.AndroidNative.getId().equals(appType)) {
			 arrlist.add(Android.getId());
			 arrlist.add(Platforms.Generic.getId());
		 }
		 
		 if(ApplicationType.IOSWeb.getId().equals(appType) || 
				 ApplicationType.IOSNative.getId().equals(appType)) {
			 arrlist.add(iOS.getId()); 
			 arrlist.add(Platforms.Generic.getId());
		 }
		 
		return arrlist;
		
	}
	

}
