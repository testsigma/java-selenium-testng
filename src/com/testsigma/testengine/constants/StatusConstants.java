package com.testsigma.testengine.constants;

public class StatusConstants {
	
	public final static Integer STATUS_COMPLETED = 0;
	public final static Integer STATUS_INPROGRESS = 2;
	public final static Integer STATUS_ABORTED = 3;
	public final static Integer STATUS_NOT_EXECUTED = 4;
	public final static Integer STATUS_STOPPED = 5;
	
}
