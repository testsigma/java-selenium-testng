package com.testsigma.testengine.constants;

import java.util.HashMap;
import java.util.Map;

public enum TestdataType {
	
	//@|Parameter|, $|Runtime|, *|Global| ,%|Environment|, !|Function|, ~|Random|
	raw(1, "raw"),
	parameter(2, "parameter"),
	runtime(3, "runtime"),
	global(4, "global"),
	environment(5, "environment"),	
	random(6, "random"),
	function(7, "function");
	
	private Integer id;
	private String dispName;
	
	TestdataType(Integer type, String dispName){
		this.id = type;
		this.dispName = dispName;
	}
	 
	public Integer getId(){
		return id;
	}
	
	public String getDispName(){
		return dispName;
	}
	
	public static Map<Integer, String> getDispNameMap(){
		Map<Integer, String> toReturn = new HashMap<Integer, String>();
		for(TestdataType type : TestdataType.values()){
			toReturn.put(type.getId(), type.getDispName());
		}
		return toReturn;
	}

}
