package com.testsigma.testengine.runners;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.testsigma.testengine.constants.ApplicationType;
import com.testsigma.testengine.constants.EnvSettingsConstants;
import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.constants.ResultConstants;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.keywords.AndroidNativeKeywordsImpl;
import com.testsigma.testengine.keywords.AndroidWebKeywordsImpl;
import com.testsigma.testengine.keywords.IOSNativeKeywordsImpl;
import com.testsigma.testengine.keywords.IOSWebKeywordsImpl;
import com.testsigma.testengine.keywords.Keywords;
import com.testsigma.testengine.keywords.WebApplicationKeywordsImpl;
import com.testsigma.testengine.keywords.WebdriverKeywordsImpl;
import com.testsigma.testengine.providers.EnvSettingsProvider;


public class WebTestcaseStepRunner extends TestcaseStepRunner{

	final private static Logger _log = Logger.getLogger(WebTestcaseStepRunner.class);

	public WebTestcaseStepRunner(String executionId) throws NumberFormatException, TestEngineException {
		super(executionId);	
	}

	protected void execute(TestCaseStepResult result, TestCaseStepEntity testcaseStep){
		//TODO: IN all exception cases replace getMessage with custom message from Message constants file.

		try {	
			ApplicationType appType = EnvSettingsProvider.getInstance().getApplicationType(executionId);
			
			//setValue(testcaseStep, executionId, envSetting);

			_log.debug("Step Entity : "+new Gson().toJson(testcaseStep));
			Keywords k = null;
			
			switch (appType) {
			case WebApplication:	
				k = new WebApplicationKeywordsImpl(executionId, testcaseStep);
				break;
			case AndroidWeb:
				k = new AndroidWebKeywordsImpl(executionId, testcaseStep);
				break;
			case AndroidNative:
				k = new AndroidNativeKeywordsImpl(executionId, testcaseStep);
				break;
			case AndroidHybrid:
				break;
			case IOSWeb:
				k = new IOSWebKeywordsImpl(executionId, testcaseStep);
				break;
			case IOSNative:
				k = new IOSNativeKeywordsImpl(executionId, testcaseStep);
				break;
			default:
				break;
			}		
			if(k != null){		
				execute(result, k.getClass(),k, testcaseStep.getKeyword());
			}		
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage(MessageConstants.MSG_STEP_SUCCESS) ;
		} catch (IllegalAccessException e) {
			Throwable cause = e.getCause();
			result.setStatus(ResultConstants.FAILURE);
			result.setErrorCode(ResultConstants.illegalAcsses);
			result.setMessage(cause.getMessage());	
			_log.error(e, e);
		} catch (IllegalArgumentException e) {
			Throwable cause = e.getCause();
			result.setStatus(ResultConstants.FAILURE);
			result.setErrorCode(ResultConstants.invalidAgument);
			result.setMessage(cause.getMessage());
			_log.error(e, e);
		} catch (InvocationTargetException e) {	
			//TODO: Need to simulate and try all possible exceptions 
			Exception ex = (Exception)e.getCause();
			if(ex instanceof TestEngineException){
				TestEngineException cause = (TestEngineException)e.getCause();
				result.setStatus(ResultConstants.FAILURE);
				result.setErrorCode(cause.getErrorCode());
				result.setMessage(cause.getMessage());
				/*				try { 
					result.setStatus(ResultConstants.STATUS_FAILURE);
					result.setErrorCode(ResultConstants.invocationTarget);
					result.setMessage(e.getMessage());
					throw e.getCause(); 
				}catch (TestEngineException tex) {
					result.setStatus(ResultConstants.STATUS_FAILURE);
					result.setErrorCode(ResultConstants.invocationTarget);
					result.setMessage(tex.getMessage());
					result.setDetails(tex.getDetails());
				}catch (Throwable te) {
					result.setStatus(ResultConstants.STATUS_FAILURE);
					result.setErrorCode(ResultConstants.invocationTarget);
					result.setMessage(te.getMessage());
				}*/	
			}else{			
				Throwable cause = e.getCause();
				result.setMetadataFromStep(testcaseStep);
				result.setStatus(ResultConstants.FAILURE);
				result.setErrorCode(ResultConstants.invocationTarget);
				result.setMessage(cause.getMessage());
			}
			_log.error(e, e);
		}catch (NoSuchMethodException e) {
			result.setStatus(ResultConstants.FAILURE);
			result.setErrorCode(ResultConstants.invalidMethod);
			result.setMessage(MessageConstants.EXCEPTION_METHOD_NOT_FOUND);
			_log.error(e, e);
		} catch (SecurityException e) {
			Throwable cause = e.getCause();
			result.setStatus(ResultConstants.FAILURE);
			result.setErrorCode(ResultConstants.invalidCredentials);
			result.setMessage(cause.getMessage());
			_log.error(e, e);
		}catch (TestEngineException tex) {
			//Throwable cause = tex.getCause();
			result.setStatus(ResultConstants.FAILURE);
			result.setErrorCode(ResultConstants.unKnownProblem);
			result.setMessage(tex.getMessage());
			_log.error(tex, tex);
		} catch (Exception e) {
			result.setStatus(ResultConstants.FAILURE);
			result.setErrorCode(ResultConstants.unKnownProblem);
			result.setMessage(e.getMessage());
			_log.error(e, e);
		}
	}


	private void execute(TestCaseStepResult result, Class<?> k, Keywords keyImpl, String keyword) throws IllegalAccessException, 
	IllegalArgumentException, InvocationTargetException, SecurityException, NoSuchMethodException{
		try {
			Method method = k.getDeclaredMethod(keyword);
			/*Object result =*/ 
			_log.debug("Class Name : " + method.getClass().getName() + " , method : " +method.getTypeParameters());

			if(TestCaseStepResult.class.isAssignableFrom(method.getReturnType())){
				TestCaseStepResult res = (TestCaseStepResult) method.invoke(k.cast(keyImpl));
				result.getOutputData().putAll(res.getOutputData());
				_log.debug("Result : "+res);
			}else{
				Object res = method.invoke(k.cast(keyImpl));
				_log.debug("Response : " + res);
			}
		} catch (NoSuchMethodException e) {
			if(k.getName().equals(WebdriverKeywordsImpl.class.getName())){
				throw e;
			}else{
				execute(result, k.getSuperclass(),keyImpl, keyword);
			}			
		}		
	}

		
}