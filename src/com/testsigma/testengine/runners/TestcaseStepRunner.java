package com.testsigma.testengine.runners;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.testsigma.testengine.constants.ApplicationType;
import com.testsigma.testengine.constants.ResultConstants;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.keywords.Keyword;
import com.testsigma.testengine.providers.UIIndentifierProvider;


public abstract class TestcaseStepRunner {

	final private static Logger _log = Logger.getLogger(TestcaseStepRunner.class);

	protected String executionId;
	protected ApplicationType applicationType;

	public TestcaseStepRunner(String executionId) throws NumberFormatException, TestEngineException {
		this.executionId= executionId;
	}


		public TestCaseStepResult run(Keyword keyword, String testdata, String ui_identifier) throws Exception {

		//TODO: check test data type and update value for run time data. 
		//For all other types data ll be updated on server side
		Integer status = ResultConstants.SUCCESS;
		TestCaseStepEntity stepEntity = new TestCaseStepEntity();
		Map<String,String> envMap = new HashMap<String,String>();
		TestCaseStepResult result = new TestCaseStepResult();
		stepEntity.setTestDataValue(testdata);
		UIIndentifierProvider uiIdentifier = new UIIndentifierProvider();
		stepEntity.setFieldDefinition(uiIdentifier.getLocator(ui_identifier));
		stepEntity.setLocatorStrategy(uiIdentifier.getType(ui_identifier));
		stepEntity.setKeyword(keyword.getMethod());
		try{
			//TODO : Add dataset name to cover datadriven case	
			execute(result, stepEntity);
		} catch (Exception e) {
			_log.error(e, e);
			status = ResultConstants.FAILURE;
			result.setErrorCode(ResultConstants.unKnownProblem);
			result.setMessage(e.getMessage());
		}

		result.setStatus(status);
		result.setEndTime(new Timestamp(System.currentTimeMillis()));
		return result;
	}

	public TestCaseStepResult runWithTestdata(Keyword keyword, String testdata) throws Exception {

		//TODO: check test data type and update value for run time data. 
		//For all other types data ll be updated on server side
		Integer status = ResultConstants.SUCCESS;
		TestCaseStepEntity stepEntity = new TestCaseStepEntity();
		TestCaseStepResult result = new TestCaseStepResult();
		stepEntity.setTestDataValue(testdata);
		stepEntity.setKeyword(keyword.getMethod());

		try{
			//TODO : Add dataset name to cover datadriven case	
			execute(result, stepEntity);
		} catch (Exception e) {
			_log.error(e, e);
			status = ResultConstants.FAILURE;
			result.setErrorCode(ResultConstants.unKnownProblem);
			result.setMessage(e.getMessage());
		}

		result.setStatus(status);
		result.setEndTime(new Timestamp(System.currentTimeMillis()));
		return result;
	}

	public TestCaseStepResult run(Keyword keyword, String ui_identifier) throws Exception {

		//TODO: check test data type and update value for run time data. 
		//For all other types data ll be updated on server side
		Integer status = ResultConstants.SUCCESS;
		TestCaseStepEntity stepEntity = new TestCaseStepEntity();
		TestCaseStepResult result = new TestCaseStepResult();
		UIIndentifierProvider uiIdentifier = new UIIndentifierProvider();
		stepEntity.setFieldDefinition(uiIdentifier.getLocator(ui_identifier));
		stepEntity.setLocatorStrategy(uiIdentifier.getType(ui_identifier));
		stepEntity.setKeyword(keyword.getMethod());
		try{
			//TODO : Add dataset name to cover datadriven case	
			execute(result, stepEntity);
		} catch (Exception e) {
			_log.error(e, e);
			status = ResultConstants.FAILURE;
			result.setErrorCode(ResultConstants.unKnownProblem);
			result.setMessage(e.getMessage());
		}

		result.setStatus(status);
		result.setEndTime(new Timestamp(System.currentTimeMillis()));
		return result;
	}

	public TestCaseStepResult run(Keyword keyword) throws Exception {

		//TODO: check test data type and update value for run time data. 
		//For all other types data ll be updated on server side
		Integer status = ResultConstants.SUCCESS;
		TestCaseStepEntity stepEntity = new TestCaseStepEntity();
		TestCaseStepResult result = new TestCaseStepResult();
		stepEntity.setKeyword(keyword.getMethod());
		try{
			//TODO : Add dataset name to cover datadriven case	
			execute(result, stepEntity);
		} catch (Exception e) {
			_log.error(e, e);
			status = ResultConstants.FAILURE;
			result.setErrorCode(ResultConstants.unKnownProblem);
			result.setMessage(e.getMessage());
		}

		result.setStatus(status);
		result.setEndTime(new Timestamp(System.currentTimeMillis()));
		return result;
	}
	protected abstract void execute(TestCaseStepResult result,TestCaseStepEntity testcaseStep) throws TestEngineException;

}