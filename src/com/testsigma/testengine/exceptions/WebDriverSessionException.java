package com.testsigma.testengine.exceptions;

import org.apache.log4j.Logger;

public class WebDriverSessionException extends TestEngineException {
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(WebDriverSessionException.class);
	
	private Integer errorCode ;
	private String message;
	private String details;
	private String dispMessage;

	public WebDriverSessionException(Integer errorCode){
		super(errorCode);
		this.errorCode = errorCode;
		logger.error(errorCode);
	}
	
	public WebDriverSessionException(Exception ex){
		super(ex);		
		this.dispMessage = ex.getLocalizedMessage();
		this.message = ex.getMessage();
		logger.error(ex);
	}
	
	public WebDriverSessionException(String msg, Exception ex){
		super(msg, ex);		
		this.dispMessage = msg;
		this.message = msg;
		logger.error(msg, ex);
	}
	
	public WebDriverSessionException(String exceptionMessage){
		super(exceptionMessage);
		errorCode = 0;
		this.message = exceptionMessage;
		logger.error(message);
	}

	public WebDriverSessionException(Integer errorCode ,String message){
		super(errorCode, message);
		this.errorCode = errorCode;
		this.message = message;
		this.dispMessage = message;
		logger.error(message);
	}
	
	public WebDriverSessionException(Integer errorCode ,String message, String details){
		super(errorCode, message, details);
		this.errorCode = errorCode;
		this.message = message;
		this.dispMessage = message;
		this.details = details;
		logger.error(message);
		logger.error("Details :: " + details);
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDispMessage() {
		return dispMessage;
	}
	public void setDispMessage(String dispMessage) {
		this.dispMessage = dispMessage;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}
