package com.testsigma.testengine.drivers;


import java.net.URL;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.gson.Gson;
import com.testsigma.testengine.constants.EnvSettingsConstants;
import com.testsigma.testengine.constants.ErrorCodes;
import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.constants.Platforms;
import com.testsigma.testengine.constants.TestsigmaCapabilityType;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionCreateFailedException;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.providers.EnvSettingsProvider;
import com.testsigma.testengine.providers.RuntimeDataProvider;
import com.testsigma.testengine.utilities.StringUtil;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

/**
 * The Class AndroidDriverManager is used to manage WebDriver instance for execution.
 */
public class IOSNativeDriverManager extends DriverManager{
	static Logger logger = Logger.getLogger(IOSNativeDriverManager.class);
	private static IOSNativeDriverManager instance;
	private Map<String, IOSDriver<WebElement>> driverMap = new HashMap<String, IOSDriver<WebElement>>();
	
	/**
	 * Instantiates a new appium driver manager.
	 */
	private IOSNativeDriverManager() {
		// setExecutionBrowsers(ExecutionSettings.getInstance().getExecutionBrowsers());
	}

	/**
	 * Gets the single instance of AppiumDriverManager.
	 * 
	 * @return single instance of AppiumDriverManager
	 */
	public static synchronized IOSNativeDriverManager getInstance() {
		if (instance == null) {
			instance = new IOSNativeDriverManager();
		}
		return instance;
	}

	/**
	 * Gets the web driver.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return the web driver
	 */
	public IOSDriver<WebElement> getDriver(String executionID) throws TestsigmaDriverSessionNotFoundException {
		IOSDriver<WebElement> driver = null;
		
		
		if (driverMap.containsKey(executionID)) {
			driver = driverMap.get(executionID);
		} else {			
			logger.error("There is no WebDriver instance associated with the executionID:: " + executionID);
			logger.error("WebDriver instance is set to null");
			throw new TestsigmaDriverSessionNotFoundException("Webdriver session not found for executionID \""+ executionID + "\"");
		}

		return driver;
	}

	/**
	 * Start session.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the string
	 */
	public String startSession(String env) throws TestEngineException {
		String executionID;
		IOSDriver<WebElement> iOSDriver = getDriverInstance(env);
		if (iOSDriver != null) {
			SecureRandom random = new SecureRandom();
			executionID = env;
			driverMap.put(executionID, iOSDriver);
			logger.info("Driver instance:: " + iOSDriver);
			return executionID;
		} else {
			executionID = "NOT_CREATED";
			throw new TestsigmaDriverSessionCreateFailedException("Error in creating iOS Driver instance for Android");
		}
	}


	/**
	 * Gets the web driver instance.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the web driver instance
	 * @throws TestsigmaDriverSessionCreateFailedException 
	 */
	private IOSDriver<WebElement> getDriverInstance(String env) throws TestsigmaDriverSessionCreateFailedException {
		IOSDriver<WebElement> iOSDriver = null;
		try {
			ExecutionType exeType= EnvSettingsProvider.getInstance().getExecutionType(env);
			if(exeType.equals(ExecutionType.SauceLabs)){
				iOSDriver = getSauceLabsWebDriver(env);
			}else if(exeType.equals(ExecutionType.BrowserStack)){
				iOSDriver = getBrowserStackDriver(env);
			} else {
				iOSDriver = createDriver(env);
			}
			
		} catch (Exception e) {
			throw new TestsigmaDriverSessionCreateFailedException(ErrorCodes.DRIVER_NOT_CREATED,
					StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_NOTCREATED, e.getMessage()));
		}

		return iOSDriver;
	}

	private IOSDriver<WebElement> getBrowserStackDriver(String env) throws Exception {
		 IOSDriver<WebElement> driver = null;
		   
		try {
			DesiredCapabilities caps = new DesiredCapabilities();
			/*if(!StringUtil.isEmpty(settings.get("app"))){
				caps.setCapability("app", uploadFileToBrowserStack(settings.get("app")));
			}*/
		     caps.setCapability("realMobile", true);
		     caps.setCapability("device", EnvSettingsProvider.getInstance().getDeviceName(env));
		     //caps.setCapability("app", "bs://003cf41ffdce9e2d1912694267e3c4744c65fe25");
		      driver = new IOSDriver<WebElement>(new URL(EnvSettingsProvider.getInstance().getBrowserStackUrl(env)), caps);
		     // driver.manage().timeouts().implicitlyWait(elementTimeout, TimeUnit.SECONDS);
		      
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e, e);
			throw e;
		}

		return driver;
	}
	
	
	private IOSDriver getSauceLabsWebDriver(String env) throws Exception {
		IOSDriver iosDriver = null;
		   
		try {
			
			DesiredCapabilities caps = DesiredCapabilities.iphone();
			//caps.setCapability("appiumVersion", "1.6.4");
			caps.setCapability("deviceName", EnvSettingsProvider.getInstance().getDeviceName(env));
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("platformVersion", EnvSettingsProvider.getInstance().getOsVersion(env));
			caps.setCapability("platformName", Platforms.iOS.name());
			caps.setCapability("testobject_api_key", EnvSettingsProvider.getInstance().getTestObjectKey(env));

			iosDriver = new IOSDriver<>(new URL(EnvSettingsProvider.getInstance().getSauseLabUrl(env)
					.replace("ondemand.saucelabs.com:443", "us1.appium.testobject.com")), caps);
			
			iosDriver.manage().timeouts().implicitlyWait(EnvSettingsProvider.getInstance().getElementTimeout(env), TimeUnit.SECONDS);	
			
		} catch (Exception e) {
			logger.error(e, e);
			throw e;
		}

		return iosDriver;
	}
	
	
	private IOSDriver createDriver(String env) {
		IOSDriver<WebElement> iOSDriver = null;
		try {
				Capabilities desiredCapabilities = getDesiredCapabilities(env);
				URL remoteAddress = new URL(EnvSettingsProvider.getInstance().getAppiumUrl(env));
				iOSDriver = new IOSDriver<WebElement>(remoteAddress, desiredCapabilities);
				iOSDriver.manage().timeouts().implicitlyWait(EnvSettingsProvider.getInstance().getElementTimeout(env), TimeUnit.SECONDS);
				
				
				
				
		} catch (Exception e) {
			logger.error(e, e);
		}

		return iOSDriver;
	}
	
	/**
	 * Gets the desired ie capabilities.
	 * 
	 * @return the desired ie capabilities
	 * @throws TestEngineException 
	 */
	private DesiredCapabilities getDesiredCapabilities(String env) throws TestEngineException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platforms.iOS.name());
		capabilities.setCapability("platformVersion", EnvSettingsProvider.getInstance().getOsVersion(env));
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, EnvSettingsProvider.getInstance().getDeviceName(env));
		capabilities.setCapability("bundleId", EnvSettingsProvider.getInstance().getBundleId(env));
		capabilities.setCapability(MobileCapabilityType.APP, EnvSettingsProvider.getInstance().getApp(env));
		capabilities.setCapability(MobileCapabilityType.UDID, EnvSettingsProvider.getInstance().getUDID(env));
		capabilities.setCapability("automationName", "XCUITest");
		
	
		return capabilities;
	}

	/**
	 * End session.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return true, if successful
	 */
	public boolean endSession(String executionID) throws Exception{
		boolean sessionEnded;
		IOSDriver<WebElement> driver = getDriver(executionID);
		try {
			driver.quit();
			sessionEnded = true;
		} catch (Exception e) {
			logger.error("Exception in ending WebDriver session: ", e);
			logger.info("Quitting WebDriver in order to end the browser session");
			driver.quit();
			logger.info("WebDriver browser session ended (Browser closed)");
			sessionEnded = true;
		}

		RuntimeDataProvider.getInstance().clearRunTimeData(executionID);
		
		return sessionEnded;
	}

	@Override
	public void performCleanUpAction(ExecutionType exeType, String executionID, Integer actionType)
			throws TestEngineException {
		// TODO Auto-generated method stub
		
	}
	
}
