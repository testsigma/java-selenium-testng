package com.testsigma.testengine.drivers;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.google.gson.Gson;
import com.testsigma.testengine.constants.EnvSettingsConstants;
import com.testsigma.testengine.constants.ErrorCodes;
import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.constants.Platforms;
import com.testsigma.testengine.constants.TestsigmaCapabilityType;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionCreateFailedException;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.providers.EnvSettingsProvider;
import com.testsigma.testengine.providers.RuntimeDataProvider;
import com.testsigma.testengine.utilities.StringUtil;


/**
 * The Class IOSWebDriverManager is used to manage WebDriver instance for execution.
 */
public class IOSWebDriverManager extends DriverManager{
	static Logger logger = Logger.getLogger(IOSWebDriverManager.class);
	private static IOSWebDriverManager instance;
	
	private static String CHROME = "googlechrome";
	private static String SAFARI = "safari";
	private static String CHROMIUM = "";
	
/*	private static String ACTIVITY_CHROME = "com.google.android.apps.chrome.Main";
	private static String ACTIVITY_SAFARI = "";
	private static String ACTIVITY_CHROMIUM = "";*/
	
	
	private Map<String, IOSDriver<WebElement>> driverMap = new HashMap<String, IOSDriver<WebElement>>();
	private Map<String, String> browserAppPackage = new HashMap<String, String>();
	private Map<String, String> browserAppActivity= new HashMap<String, String>();
	
	/**
	 * Instantiates a new appium driver manager.
	 */
	private IOSWebDriverManager() {
	}

	/**
	 * Gets the single instance of AppiumDriverManager.
	 * 
	 * @return single instance of AppiumDriverManager
	 */
	public static synchronized IOSWebDriverManager getInstance() {
		if (instance == null) {
			instance = new IOSWebDriverManager();
		}
		return instance;
	}

	/**
	 * Gets the web driver.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return the web driver
	 */
	public IOSDriver<WebElement> getDriver(String executionID) throws TestsigmaDriverSessionNotFoundException {

		if (driverMap.containsKey(executionID)) {
			return driverMap.get(executionID);
		} else {
			logger.error("There is no WebDriver instance associated with the executionID:: " + executionID);
			logger.error("WebDriver instance is set to null");
			throw new TestsigmaDriverSessionNotFoundException("Webdriver session not found for executionID \""+ executionID + "\"");
		}

	}

	/**
	 * Start session.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the string
	 */
	public String startSession(String env) throws TestEngineException {
		String executionID;
		IOSDriver<WebElement> iOSDriver = getDriverInstance(env);
		if (iOSDriver != null) {
			//SecureRandom random = new SecureRandom();
			executionID = env;
			driverMap.put(executionID, iOSDriver);
			logger.info("Driver instance:: " + iOSDriver);
			return executionID;
		} else {
			executionID = "NOT_CREATED";
			throw new TestsigmaDriverSessionCreateFailedException("Error in creating iOS Driver instance for IOS");
		}
	}


	/**
	 * Gets the web driver instance.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the web driver instance
	 * @throws TestsigmaDriverSessionCreateFailedException 
	 */
	private IOSDriver<WebElement> getDriverInstance(String env) throws TestsigmaDriverSessionCreateFailedException {
		IOSDriver<WebElement> iOSDriver = null;
		try {
			ExecutionType exeType= EnvSettingsProvider.getInstance().getExecutionType(env);
			 if(exeType.equals(ExecutionType.SauceLabs)){
				iOSDriver =  getSauceLabsWebDriver(env);
			}else if(exeType.equals(ExecutionType.BrowserStack)){
				iOSDriver = getBrowserStackWebDriver(env);
			} else {
				iOSDriver = createDriver(env);
			}
			
		} catch (Exception e) {
			logger.error(e,e);
			throw new TestsigmaDriverSessionCreateFailedException(ErrorCodes.DRIVER_NOT_CREATED,
					StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_NOTCREATED, e.getMessage()));

		}

		return iOSDriver;

	}

	private IOSDriver createDriver(String env) {
		IOSDriver<WebElement> iOSDriver = null;
		try {
				Capabilities desiredCapabilities = getDesiredCapabilities(env);
				URL remoteAddress = new URL(EnvSettingsProvider.getInstance().getApkUrl(env));
				iOSDriver = new IOSDriver<WebElement>(remoteAddress, desiredCapabilities);
				iOSDriver.manage().timeouts().implicitlyWait(EnvSettingsProvider.getInstance().getElementTimeout(env), TimeUnit.SECONDS);
				
		} catch (Exception e) {
			logger.error(e, e);
		}

		return iOSDriver;
	}

	
	/**
	 * Gets the desired ie capabilities.
	 * 
	 * @return the desired ie capabilities
	 * @throws TestEngineException 
	 */
	private DesiredCapabilities getDesiredCapabilities(String env) throws TestEngineException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platforms.iOS.name());
		capabilities.setCapability("platformVersion", EnvSettingsProvider.getInstance().getOsVersion(env));
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, EnvSettingsProvider.getInstance().getDeviceName(env));
		capabilities.setCapability(CapabilityType.BROWSER_NAME,EnvSettingsProvider.getInstance().getBrowser(env));
		capabilities.setCapability("browserVersion", EnvSettingsProvider.getInstance().getBrowserVersion(env));
		capabilities.setCapability(MobileCapabilityType.UDID, EnvSettingsProvider.getInstance().getUDID(env));
		
		return capabilities;
	}

	private IOSDriver<WebElement> getBrowserStackWebDriver(String env) throws Exception {
		 IOSDriver<WebElement> driver = null;
		   
		try {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("browser", EnvSettingsProvider.getInstance().getBrowser(env));
			caps.setCapability("device", EnvSettingsProvider.getInstance().getDeviceName(env));
		      caps.setCapability("realMobile", "true");
		      caps.setCapability("browserVersion", EnvSettingsProvider.getInstance().getBrowserVersion(env));
		      caps.setCapability("os_version", EnvSettingsProvider.getInstance().getOsVersion(env));
		      
		      driver = new IOSDriver<WebElement>(new URL(EnvSettingsProvider.getInstance().getBrowserStackUrl(env)), caps);
		     // driver.manage().timeouts().implicitlyWait(elementTimeout, TimeUnit.SECONDS);				
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e, e);
			throw e;
		}

		return driver;
	}

	
	/**
	 * Gets the web driver instance.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the web driver instance
	 * @throws Exception 
	 */
	private IOSDriver getSauceLabsWebDriver(String env) throws Exception {
		IOSDriver iosDriver = null;
		   
		try {
			
			DesiredCapabilities caps = DesiredCapabilities.iphone();
			//caps.setCapability("testobject_session_creation_timeout", 1000);
			//caps.setCapability("appiumVersion", "1.6.4");
			caps.setCapability("deviceName", EnvSettingsProvider.getInstance().getDeviceName(env));
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("browserName", EnvSettingsProvider.getInstance().getBrowser(env));
			caps.setCapability("platformVersion", EnvSettingsProvider.getInstance().getOsVersion(env));
			caps.setCapability("platformName", Platforms.iOS.name());
			caps.setCapability("testobject_api_key", EnvSettingsProvider.getInstance().getTestObjectKey(env));
			logger.debug("creating android manager");
			iosDriver = new IOSDriver<>(new URL(EnvSettingsProvider.getInstance().getSauseLabUrl(env).replace("ondemand.saucelabs.com:443", "us1.appium.testobject.com")), caps);
			
			iosDriver.manage().timeouts().implicitlyWait(EnvSettingsProvider.getInstance().getElementTimeout(env), TimeUnit.SECONDS);
			logger.debug("setting implicit time out");
			
		} catch (Exception e) {
			logger.error(e, e);
			throw e;
		}

		return iosDriver;
	}
	
	
	/**
	 * End session.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return true, if successful
	 */
	public boolean endSession(String executionID) throws Exception{
		boolean sessionEnded;
		IOSDriver<WebElement> driver = getDriver(executionID);
		try {
			driver.quit();
			sessionEnded = true;
		} catch (Exception e) {
			logger.error("Exception in ending WebDriver session: ", e);
			logger.info("Quitting WebDriver in order to end the browser session");
			driver.quit();
			logger.info("WebDriver browser session ended (Browser closed)");
			sessionEnded = true;
		}

		RuntimeDataProvider.getInstance().clearRunTimeData(executionID);
		
		return sessionEnded;
	}

	@Override
	public void performCleanUpAction(ExecutionType exeType, String executionID, Integer actionType)
			throws TestEngineException {
		// TODO Auto-generated method stub
		
	}

	
}
