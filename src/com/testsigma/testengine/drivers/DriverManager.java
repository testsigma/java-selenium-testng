package com.testsigma.testengine.drivers;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.exceptions.TestEngineException;

public abstract class DriverManager {
	public static String NOT_CREATED = "NOT_CREATED"; 
	public abstract void performCleanUpAction(ExecutionType exeType, String executionID,Integer actionType) throws TestEngineException;
    
}
