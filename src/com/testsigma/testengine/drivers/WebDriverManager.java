package com.testsigma.testengine.drivers;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.testsigma.testengine.constants.Browsers;
import com.testsigma.testengine.constants.ErrorCodes;
import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.constants.OnAbortActions;
import com.testsigma.testengine.constants.Platforms;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionCreateFailedException;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestsigmaUnknownBrowserNameException;
import com.testsigma.testengine.exceptions.TestsigmaWebDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.providers.EnvSettingsProvider;
import com.testsigma.testengine.providers.RuntimeDataProvider;
import com.testsigma.testengine.utilities.StringUtil;



/**
 * The Class WebDriverManager is used to manage WebDriver instance for execution.
 */
public class WebDriverManager extends DriverManager{
	static Logger logger = Logger.getLogger(WebDriverManager.class);
	private static WebDriverManager instance;
	
	//TODO: All these fields can cause memory leak. Delete entries from these variable 
	private Map<String, String> windowHandles = new HashMap<String, String>();
	private Map<String, WebDriver> driverMap = new HashMap<String, WebDriver>();
	private Map<String, WebDriverSession> sessionMap = new HashMap<String, WebDriverSession>();
	//private String browserName = "UNKNOWN";

	/**
	 * Instantiates a new web driver manager.
	 */
	private WebDriverManager() {
		// setExecutionBrowsers(ExecutionSettings.getInstance().getExecutionBrowsers());
	}

	/**
	 * Gets the single instance of WebDriverManager.
	 * 
	 * @return single instance of WebDriverManager
	 */

	public static synchronized WebDriverManager getInstance() {
		if (instance == null) {
			instance = new WebDriverManager();
		}
		return instance;
	}

	/**
	 * Gets the web driver.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return the web driver
	 */
	public WebDriver getDriver(String executionId) throws TestsigmaDriverSessionNotFoundException {

		if (driverMap.containsKey(executionId)) {
			return driverMap.get(executionId);
		} else {
			throw new TestsigmaDriverSessionNotFoundException(
					StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_NOTFOUND, executionId));
		}
	}

	/**
	 * Start session.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the string
	 */
	public String startSession(String env) throws TestEngineException{
		String executionID = DriverManager.NOT_CREATED;
		/*try {*/
		WebDriver webdriver = getWebDriverInstance(env);
		Capabilities cap = ((RemoteWebDriver) webdriver).getCapabilities();
		String browserVersion = cap.getVersion();

		if((browserVersion.indexOf("."))>-1){
			browserVersion = browserVersion.substring(0, browserVersion.indexOf(".")+2);
		}

		if (webdriver != null) {
			executionID = env;
			driverMap.put(executionID, webdriver);
			windowHandles.put(executionID, webdriver.getWindowHandle());

			/*webSession.setBrowserName(browserName);
				webSession.setVersionNumber(browserVersion);*/
			//TODO: check and report browser version
			//webSession.setVersion(browserVersion);
			logger.info("Webdriver instance with sessionID :: " + executionID + 
					" is created for browser :: " + EnvSettingsProvider.getInstance().getBrowser(env));

		}
		
		return executionID;
		/*		} catch (Exception e) {
			logger.error("Error in starting Webdriver instance for browser:: " + executionBrowser);
			executionID = "NOT_CREATED";
		}*/
	}


	public void resetSession(String executionID) throws TestEngineException {

		if(getDriver(executionID) != null){
			getDriver(executionID).quit();/*
			Integer platform = sessionMap.get(executionID).getPlatform();
			String browser = sessionMap.get(executionID).getBrowser();
			String version = sessionMap.get(executionID).getVersion();*/
			int elmentTimeOut = sessionMap.get(executionID).getElementTimeout();
			int pageTimeOut = sessionMap.get(executionID).getPageloadTimeout();
			WebDriver webdriver = getWebDriverInstance(executionID);
			driverMap.put(executionID, webdriver);
			logger.info("The WebDriver Session for the execution ID \"" + executionID+ "\"has been successfully reset");
		}else{
			throw new TestsigmaWebDriverSessionNotFoundException(StringUtil.getMessage(
					MessageConstants.EXCEPTION_WEBDRIVER_RESET_SESSION_FAILED, executionID));
		}
	}

	/**
	 * Gets the web driver instance.
	 * 
	 * @param browser
	 *            the execution browser
	 * @return the web driver instance
	 * @throws TestEngineException 
	 */
	public WebDriver getWebDriverInstance(String env) throws TestEngineException {
		
		Platforms os = EnvSettingsProvider.getInstance().getPlatform(env);
		String osVersion = EnvSettingsProvider.getInstance().getOsVersion(env);
		String browser = EnvSettingsProvider.getInstance().getBrowser(env);
		String browserVersion = EnvSettingsProvider.getInstance().getBrowserVersion(env);
		Integer pageloadTimeout = EnvSettingsProvider.getInstance().getPageTimeout(env);
		Integer elementTimeout = EnvSettingsProvider.getInstance().getElementTimeout(env);
		ExecutionType exeType = EnvSettingsProvider.getInstance().getExecutionType(env);
		WebDriver driver = null;

		try {
			if(exeType.equals(ExecutionType.Grid)){
				driver = getGridWebDriver(env, browser, os);
			}else if(exeType.equals(ExecutionType.SauceLabs)){
				driver = getSauceLabsWebDriver(EnvSettingsProvider.getInstance().getSauseLabUrl(env), 
						os, osVersion, browser, browserVersion, elementTimeout, pageloadTimeout);
			}else if(exeType.equals(ExecutionType.BrowserStack)){
				driver = getBrowserStackWebDriver(EnvSettingsProvider.getInstance().getBrowserStackUrl(env), 
						os, osVersion, browser, browserVersion, elementTimeout, pageloadTimeout);
			} else {
				driver = getWebDriver(env, browser, os);
			}
		}catch (Exception e) {
			logger.error(e,e);
			throw new TestsigmaDriverSessionCreateFailedException(ErrorCodes.DRIVER_NOT_CREATED,
					StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_NOTCREATED, e.getMessage()));

		}

		return driver;
	}

	private WebDriver getWebDriver(String env, String browser, Platforms platform) throws TestEngineException{
		WebDriver driver = null; 
		String browserDriverEXEpath = "NOT_FOUND";
		
		try {		
			switch (Browsers.valueOf(browser)){		
			case INTERNETEXPLORER:
				
				browserDriverEXEpath = EnvSettingsProvider.getInstance().getDriverPath(env);
				logger.debug("Driver Exec Path: "+ browserDriverEXEpath);				
				System.setProperty("webdriver.ie.driver",browserDriverEXEpath);
				driver = new InternetExplorerDriver(getDesiredIECapabilities());
				driver.manage().deleteAllCookies();
				driver.manage().window().maximize();
				logger.info("Internet Explorer browser is set for test suite execution");
	
				break;
	
			case FIREFOX:
	
				browserDriverEXEpath = EnvSettingsProvider.getInstance().getDriverPath(env);

				logger.debug("Driver Exec Path: "+ browserDriverEXEpath);				
				System.setProperty("webdriver.gecko.driver",browserDriverEXEpath);
	
				//FirefoxProfile firefoxProfile = getFirefoxProfile();
				//driver = new FirefoxDriver(new FirefoxBinary(new File(exepath)), getFirefoxProfile());
				//TODO:: Custom firefox installations and firefox profiles need to be handled here. 
				driver = new FirefoxDriver();
				driver.manage().window().maximize();
				logger.info("Firefox browser is set for test suite execution");
				break;
	
			case GOOGLECHROME:
	
				
				browserDriverEXEpath = EnvSettingsProvider.getInstance().getDriverPath(env);
				logger.info("Driver Exec Path:"+browserDriverEXEpath);
				System.setProperty("webdriver.chrome.driver", browserDriverEXEpath);	
				driver = new ChromeDriver();
				
				if(platform.equals(Platforms.Mac)){
					driver.manage().window().fullscreen();
				}else{
					driver.manage().window().maximize();
				}
				
				logger.info("Google Chrome browser is set for test suite execution");				
	
				break;
	
			case SAFARI:
				driver= new SafariDriver();
				driver.manage().window().maximize();
				break;
	
			case EDGE:
	
				
				browserDriverEXEpath = EnvSettingsProvider.getInstance().getDriverPath(env);
				
				logger.info("Driver Exec Path:" + browserDriverEXEpath);
				System.setProperty("webdriver.edge.driver", browserDriverEXEpath);	
				driver = new EdgeDriver();
				driver.manage().window().maximize();
				logger.info("Edge browser is set for test suite execution");				
	
				break;
	
			default:
				throw new TestsigmaUnknownBrowserNameException(ErrorCodes.DRIVER_NOT_CREATED,
						StringUtil.getMessage(MessageConstants.EXCEPTION_UNKNOWN_BROWSER, browser));
				//break;
			}
			Integer elementTimeout = EnvSettingsProvider.getInstance().getElementTimeout(env);
			driver.manage().timeouts().implicitlyWait(elementTimeout, TimeUnit.SECONDS);
		} catch (Exception ex){
			logger.error(ex, ex);
			throw ex;
		}
		
		return driver;
	}

	
	private WebDriver getGridWebDriver(String env, String browser, Platforms platform) throws Exception{
		WebDriver driver = null; 
		String browserDriverEXEpath = "NOT_FOUND";
		

		String nodeURL = EnvSettingsProvider.getInstance().getNodeUrl(env);
		try {		
			switch (Browsers.valueOf(browser)) {		
			case INTERNETEXPLORER:
				
				browserDriverEXEpath = EnvSettingsProvider.getInstance().getDriverPath(env);
				logger.debug("Driver Exec Path: "+ browserDriverEXEpath);				
				System.setProperty("webdriver.ie.driver",browserDriverEXEpath);
				
		        DesiredCapabilities ieCapability = DesiredCapabilities.internetExplorer();
		        driver = new RemoteWebDriver(new URL(nodeURL), ieCapability); 
		        
				driver = new InternetExplorerDriver(getDesiredIECapabilities());
				driver.manage().deleteAllCookies();
				driver.manage().window().maximize();
				logger.info("Internet Explorer browser is set for test suite execution");
	
				break;
	
			case FIREFOX:
	
				browserDriverEXEpath = EnvSettingsProvider.getInstance().getDriverPath(env);

				logger.debug("Driver Exec Path: "+ browserDriverEXEpath);				
				System.setProperty("webdriver.gecko.driver",browserDriverEXEpath);
	
				DesiredCapabilities ffCapability = DesiredCapabilities.firefox(); 
		        driver = new RemoteWebDriver(new URL(nodeURL), ffCapability);  		
			        
			        
				//FirefoxProfile firefoxProfile = getFirefoxProfile();
				//driver = new FirefoxDriver(new FirefoxBinary(new File(exepath)), getFirefoxProfile());
				//TODO:: Custom firefox installations and firefox profiles need to be handled here. 
				driver.manage().window().maximize();
				logger.info("Firefox browser is set for test suite execution");
				break;
	
			case GOOGLECHROME:
	
				
				browserDriverEXEpath = EnvSettingsProvider.getInstance().getDriverPath(env);
				logger.info("Driver Exec Path:"+browserDriverEXEpath);
				System.setProperty("webdriver.chrome.driver", browserDriverEXEpath);	
				
				DesiredCapabilities chromeCapability = DesiredCapabilities.chrome();
		        driver = new RemoteWebDriver(new URL(nodeURL), chromeCapability);   
				
				if(platform.equals(Platforms.Mac)){
					driver.manage().window().fullscreen();
				}else{
					driver.manage().window().maximize();
				}
				
				logger.info("Google Chrome browser is set for test suite execution");				
	
				break;
	
			case SAFARI:
				driver= new SafariDriver();
				driver.manage().window().maximize();
				break;
	
			case EDGE:
	
				
				browserDriverEXEpath = EnvSettingsProvider.getInstance().getDriverPath(env);
				
				logger.info("Driver Exec Path:" + browserDriverEXEpath);
				System.setProperty("webdriver.edge.driver", browserDriverEXEpath);	
				driver = new EdgeDriver();
				driver.manage().window().maximize();
				logger.info("Edge browser is set for test suite execution");				
	
				break;
	
			default:
				throw new TestsigmaUnknownBrowserNameException(ErrorCodes.DRIVER_NOT_CREATED,
						StringUtil.getMessage(MessageConstants.EXCEPTION_UNKNOWN_BROWSER, browser));
				//break;
			}

			Integer pageloadTimeout = EnvSettingsProvider.getInstance().getPageTimeout(env);
			Integer elementTimeout = EnvSettingsProvider.getInstance().getElementTimeout(env);
			
	        driver.manage().timeouts().pageLoadTimeout(pageloadTimeout, TimeUnit.SECONDS);
	        driver.manage().timeouts().implicitlyWait(elementTimeout, TimeUnit.SECONDS);
		} catch (Exception ex){
			logger.error(ex, ex);
			throw ex;
		}
		
		return driver;
	}
	
	
	private WebDriver getBrowserStackWebDriver(String url, Platforms platform, String osVersion, String browser, String version, int elementTimeout, int pageloadTimeout) throws TestEngineException{
		WebDriver driver = null;
		DesiredCapabilities caps = new DesiredCapabilities();
				  
		try {
			
			caps.setCapability("platform", platform.getOs());
			caps.setCapability("version", version);
			caps.setCapability("os", platform.getOs());
			caps.setCapability("os_version", osVersion);
			caps.setCapability("resolution", "1920x1080");
			System.out.println(platform.getOs()+"_"+version+"_"+platform.getOs()+"_"+osVersion+"_"+url);
			switch (Browsers.valueOf(browser)) {
			case GOOGLECHROME:
				caps.setCapability("browser", "Chrome");
				break;
			case FIREFOX:
				caps.setCapability("browser", "Firefox");
				break;
			case INTERNETEXPLORER:
				caps.setCapability("browser", "IE");
				caps.setCapability("browserstack.ie.enablePopups", true);
				break;
			case SAFARI:
				caps.setCapability("browser", "Safari");
				caps.setCapability("browserstack.safari.enablePopups", true);
				caps.setCapability("browserstack.safari.allowAllCookies", true);
				break;
			case EDGE:
				caps.setCapability("browser", "Edge");
				caps.setCapability("browserstack.edge.enablePopups", true);
				break;
			}
			
			caps.setCapability("browser_version", version);
			caps.setCapability("acceptSslCerts", true);
			
			driver = new RemoteWebDriver(new URL(url), caps);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
		} catch (MalformedURLException e) {
			logger.error(e, e);
			throw new TestEngineException("Failed to create web driver.", e);
		} catch (Exception e) {
			logger.error(e, e);
			throw e;
		}
		
		driver.manage().timeouts().implicitlyWait(elementTimeout, TimeUnit.SECONDS);

		return driver;
	}
	
	private WebDriver getSauceLabsWebDriver(String url,  Platforms platform, String osVersion, String browser, String version, int elementTimeout, int pageloadTimeout) throws TestEngineException{
		WebDriver driver = null;
		DesiredCapabilities caps = new DesiredCapabilities();
				  
		switch (Browsers.valueOf(browser)) {		
		case INTERNETEXPLORER:		
			caps = getDesiredIECapabilities();
			logger.info("Internet Explorer browser is set for test suite execution");
			break;

		case FIREFOX:
			caps = DesiredCapabilities.firefox();
			logger.info("Firefox browser is set for test suite execution");
			break;

		case GOOGLECHROME:
			caps = DesiredCapabilities.chrome();
			logger.info("Google Chrome browser is set for test suite execution");				
			break;

		case SAFARI:
			caps = DesiredCapabilities.safari();
			logger.info("Safari browser is set for test suite execution");		
			break;

		case EDGE:
			caps = DesiredCapabilities.edge();
			logger.info("Edge browser is set for test suite execution");				

			break;

		default:
			throw new TestsigmaUnknownBrowserNameException(ErrorCodes.DRIVER_NOT_CREATED,
					StringUtil.getMessage(MessageConstants.EXCEPTION_UNKNOWN_BROWSER, browser));
			//break;
		}
		
		try {
			caps.setCapability("platform", osVersion);
			caps.setCapability("version", version);
			caps.setCapability("screenResolution", "1600x1200");
			driver = new RemoteWebDriver(new URL(url), caps);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
		} catch (MalformedURLException e) {
			logger.error(e, e);
			throw new TestEngineException("Failed to create web driver.");
		} catch (Exception e) {
			logger.error(e, e);
			throw e;
		}
		
		driver.manage().timeouts().implicitlyWait(elementTimeout, TimeUnit.SECONDS);

		return driver;
	}

	/**
	 * Gets the desired ie capabilities.
	 * 
	 * @return the desired ie capabilities
	 */
	private DesiredCapabilities getDesiredIECapabilities() {
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability("ie.ensureCleanSession", true);
		//Ask IE to ignore zoom level, otherwise IE may have problem starting
		capabilities.setCapability("ignoreZoomSetting", true);
		capabilities.setCapability("handlesAlerts", false);
		capabilities.setCapability("ignoreProtectedModeSettings", true);
		capabilities.setCapability("cssSelectorsEnabled", true);
		capabilities.setCapability("nativeEvents", true);
		capabilities.setCapability("takesScreenshot", true);
		capabilities.setCapability("unexpectedAlertBehaviour", "dismiss");

		//capabilities.setCapability("platform", Platform.WINDOWS);
		//capabilities.setCapability("javascriptEnabled", true);
		//capabilities.setCapability("elementScrollBehavior", 0);
		//capabilities.setCapability("ignoreZoomSetting", true);
		//capabilities.setCapability("enablePersistentHover", true);
		//capabilities.setCapability("ie.ensureCleanSession", true);
		//capabilities.setCapability("browserName", "internet explorer");
		//capabilities.setCapability("enableElementCacheCleanup", true);
		//capabilities.setCapability("unexpectedAlertBehaviour", "dismiss");
		//capabilities.setCapability("version", 10);
		//capabilities.setCapability("ie.usePerProcessProxy", false);
		//capabilities.setCapability("cssSelectorsEnabled", true);
		//capabilities.setCapability("ignoreProtectedModeSettings", true);
		//capabilities.setCapability("requireWindowFocus", false);
		//capabilities.setCapability("handlesAlerts", false);
		//capabilities.setCapability("initialBrowserUrl", "http://localhost:19309/");
		//capabilities.setCapability("ie.forceCreateProcessApi", false);
		//capabilities.setCapability("nativeEvents",true);
		//capabilities.setCapability("browserAttachTimeout", 0);
		//capabilities.setCapability("ie.browserCommandLineSwitches", "");
		//capabilities.setCapability("takesScreenshot", true);

		return capabilities;
	}

	/**
	 * Maximize windows using JS.
	 * 
	 * @param driver
	 *            the driver
	 */
	private static void maximizeBrowserWindow(final WebDriver driver) {
		try {
			((JavascriptExecutor) driver).executeScript("if (window.screen) { window.moveTo(0, 0);"
					+ " window.resizeTo(window.screen.availWidth,window.screen.availHeight); };");
			/*driver.manage().window().setPosition(new Point(0, 0));
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Dimension screenResolution = new Dimension((int) 
			                    toolkit.getScreenSize().getWidth(), (int) 
			                    toolkit.getScreenSize().getHeight());
			driver.manage().window().setSize(screenResolution);*/
		} catch (Exception e) {
			logger.error("Exception in maximizeWindowsUsingJS: ", e);
		} //Ignore
	}

	/**
	 * Resets zoom level.
	 */
	private void resetZoomLevel() {
		try {
			Robot ignoreZoom = new Robot();
			ignoreZoom.keyPress(KeyEvent.VK_CONTROL);
			ignoreZoom.keyPress(KeyEvent.VK_0);
			ignoreZoom.keyRelease(KeyEvent.VK_CONTROL);
		} catch (Exception e) {
			logger.error("Exception in resetZoomLevel: ", e);
		} //Ignore
	}

	/**
	 * End session.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return true, if successful
	 */
	public boolean endSession(String executionID) throws Exception{
		boolean sessionEnded;
		WebDriver driver = getDriver(executionID);
		try {
			closeAllWindows(driver);
			sessionEnded = true;
			
			windowHandles.remove(executionID);
			driverMap.remove(executionID);
			sessionMap.remove(executionID);
			
		} catch (Exception e) {
			logger.error("Exception in ending WebDriver session: ", e);
			logger.info("WebDriver browser session ended (Browser closed)");
			sessionEnded = true;
		} finally{
			logger.info("Quitting WebDriver in order to end the browser session");
			if(driver != null)
				driver.quit();
		}

	  RuntimeDataProvider.getInstance().clearRunTimeData(executionID); 

		return sessionEnded;
	}

	/**
	 * Close all windows.
	 * 
	 * @param driver
	 *            the driver
	 */
	public void closeAllWindows(WebDriver driver) {
		logger.info("Closing all windows...");
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			logger.info("Closing window with title:: " + driver.getTitle());
			driver.close();
		}
	}

	/**
	 * Close windows if more than one exist.
	 * 
	 * @param executionID
	 *            the execution id
	 * @throws TestsigmaDriverSessionNotFoundException 
	 */
	public void closeAllWindowsExceptMainWindow(String executionID) throws TestEngineException {

		String mainWindowHandle= getMainWindowHandle(executionID);
		if(mainWindowHandle!=null){
			WebDriver sessionDriver = getDriver(executionID);
			Set<String> sessionHandles = sessionDriver.getWindowHandles();
			logger.info("Windows currently open before cleanup : \""+sessionDriver.getWindowHandles().size() +"\"");
			for(String sessionHandle : sessionHandles){
				if(!sessionHandle.equals(mainWindowHandle)){
					try{
						sessionDriver.switchTo().window(sessionHandle);
						sessionDriver.close();
						logger.info("Window with window handle \""+ sessionHandle + "\" is closed");
					}catch(Exception e){
						logger.error("Error in closing window with window handle \""+sessionHandle + "\" , details::"+ e.getMessage());
					}
				}
			}
			logger.info("Windows open after cleanup : \""+sessionDriver.getWindowHandles().size() +"\"");
			// Retaining focus back to the main window
			try{
				sessionDriver.switchTo().window(mainWindowHandle);
				logger.info("The focus is changed to the window with window handle \""+ mainWindowHandle + "\"");
			}catch(Exception e){
				logger.error("Error in switching to the window with window handle \""+mainWindowHandle + "\" , details::"+ e.getMessage());
			}

		}else{
			logger.error("There is no main window handle associated with the given execution id \""+executionID +"\"");
		}

	}

	private String getMainWindowHandle(String executionID) {
		if(windowHandles.containsKey(executionID)){
			return windowHandles.get(executionID);
		}else{
			logger.error("There is no window handle stored for execution id \""+ executionID + "\"");
			return null;
		}
	}

	/**
	 * Delete All cookies.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return true, if successful
	 */
	public boolean deleteCookies(String executionID) {
		boolean successful;
		try {
			getDriver(executionID).manage().deleteAllCookies();
			logger.info("Deleted all cookies associated for webdriver instance associated with executionID:: " + executionID);
			successful = true;
		} catch (Exception e) {
			logger.error("Error in deleting all cookies for webdriver instance associated with executionID:: " + executionID + ". Details:"+e.getMessage());
			successful = false;
		}
		return successful;
	}

	/**
	 * Gets the browser details.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return the browser details
	 */
	public String getBrowserDetails(String executionID) {
		String browserDetails = "";
		try {
			WebDriver webdriver = getDriver(executionID);
			/*if(webdriver instanceof FirefoxDriver) {
				browserDetails= FrameworkConstants.browserName.FIREFOX.toString();
			} else if(webdriver instanceof ChromeDriver) {
				browserDetails= FrameworkConstants.browserName.GOOGLECHROME.toString();
			} else if(webdriver instanceof InternetExplorerDriver) {
				browserDetails= FrameworkConstants.browserName.INTERNETEXPLORER.toString();
			} else {
				browserDetails= FrameworkConstants.browserName.UNKNOWN.toString();
			}*/
			Capabilities caps = ((RemoteWebDriver) webdriver).getCapabilities();
			String browserName = caps.getBrowserName();
			String browserVersion = caps.getVersion();
			logger.info("BrowserDetails: " + browserName.toUpperCase() + " - V" + browserVersion);
			browserDetails = browserName.toUpperCase() + " - V" + browserVersion;
		} catch (Exception e) {
			String UNKNOWN="UNKNOWN";
			browserDetails = UNKNOWN;
			logger.error("Error in fetching browser details for executionID:: " + executionID + " , Details:: " + e.getMessage());
		}
		return browserDetails;
	}


	/**
	 * Gets the execution i ds.
	 * 
	 * @return the execution i ds
	 */
	public List<String> getExecutionIDs() {
		List<String> executionIDs = new ArrayList<String>();
		for (String executionID : driverMap.keySet()) {
			executionIDs.add(executionID);
		}
		return executionIDs;
	}

	/**
	 * Gets the browser name.
	 * 
	 * @return the browser name
	 *//*
	public String getBrowserName() {
		return browserName;
	}

	  *//**
	  * Sets the browser name.
	  * 
	  * @param browserName
	  *            the new browser name
	  *//*
	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}
	   */
	/**
	 * Gets the driver map.
	 * 
	 * @return the driver map
	 */
	public Map<String, WebDriver> getDriverMap() {
		return driverMap;
	}

	/**
	 * Sets the driver map.
	 * 
	 * @param driverMap
	 *            the driver map
	 */
	public void setDriverMap(Map<String, WebDriver> driverMap) {
		this.driverMap = driverMap;
	}

	/**
	 * Gets the window handles.
	 * 
	 * @return the window handles
	 */
	public Map<String, String> getWindowHandles() {
		return windowHandles;
	}

	/**
	 * Sets the window handles.
	 * 
	 * @param windowHandles
	 *            the window handles
	 */
	public void setWindowHandles(Map<String, String> windowHandles) {
		this.windowHandles = windowHandles;
	}

	@Override
	public void performCleanUpAction(ExecutionType exeType, String executionID,Integer actionType) throws TestEngineException {

		switch(OnAbortActions.getActions(actionType)){
		case Restart_Session:

			logger.info("On abort action - restarting session for execution id \""+ executionID + "\"");
			try{
				resetSession(executionID);
			}catch(TestsigmaDriverSessionNotFoundException e){
				logger.error("Error in restarting session for execution id \""+executionID +"\". The session corresponding to the execution id is not found.");
				try{	
					resetSession(executionID);
				}catch(TestsigmaDriverSessionNotFoundException ex){
					logger.error("Failed to restart session in retry also.");
					throw new TestEngineException("Failed to restart session");
				}
			}
			break;
		case Reuse_Session:

			logger.info("On abort action - reusing session by performing actions: all windows except"
					+ " main window will be closed and cookies will be deleted  for execution id \""+ executionID + "\"" );
			closeAllWindowsExceptMainWindow(executionID);
			deleteCookies(executionID);
			break;
		default:
			logger.error("Invalid action type is provided for on abort recovery actions");
		}

	}

}
