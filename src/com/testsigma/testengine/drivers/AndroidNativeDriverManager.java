package com.testsigma.testengine.drivers;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.net.URL;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.testsigma.testengine.constants.EnvSettingsConstants;
import com.testsigma.testengine.constants.ErrorCodes;
import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.constants.Platforms;
import com.testsigma.testengine.constants.TestsigmaCapabilityType;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionCreateFailedException;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.providers.EnvSettingsProvider;
import com.testsigma.testengine.providers.RuntimeDataProvider;
import com.testsigma.testengine.utilities.StringUtil;



/**
 * The Class AndroidDriverManager is used to manage WebDriver instance for execution.
 */
public class AndroidNativeDriverManager extends DriverManager{
	static Logger logger = Logger.getLogger(AndroidNativeDriverManager.class);
	private static AndroidNativeDriverManager instance;
	private Map<String, AndroidDriver> driverMap = new HashMap<String, AndroidDriver>();
	
	/**
	 * Instantiates a new appium driver manager.
	 */
	private AndroidNativeDriverManager() {
		// setExecutionBrowsers(ExecutionSettings.getInstance().getExecutionBrowsers());
	}

	/**
	 * Gets the single instance of AppiumDriverManager.
	 * 
	 * @return single instance of AppiumDriverManager
	 */
	public static synchronized AndroidNativeDriverManager getInstance() {
		if (instance == null) {
			instance = new AndroidNativeDriverManager();
		}
		return instance;
	}

	/**
	 * Gets the web driver.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return the web driver
	 */
	public AndroidDriver getDriver(String executionID) throws TestsigmaDriverSessionNotFoundException {
		AndroidDriver driver = null;
		
		
		if (driverMap.containsKey(executionID)) {
			driver = driverMap.get(executionID);
		} else {			
			logger.error("There is no WebDriver instance associated with the executionID:: " + executionID);
			logger.error("WebDriver instance is set to null");
			throw new TestsigmaDriverSessionNotFoundException("Webdriver session not found for executionID \""+ executionID + "\"");
		}

		return driver;
	}

	/**
	 * Start session.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the string
	 */
	public String startSession(String env) throws TestEngineException {
		String executionID;
		AndroidDriver androidDriver = getDriverInstance(env);
		if (androidDriver != null) {
			SecureRandom random = new SecureRandom();
			executionID = env;
			driverMap.put(executionID, androidDriver);
			logger.info("Driver instance:: " + androidDriver);
			return executionID;
		} else {
			executionID = "NOT_CREATED";
			throw new TestsigmaDriverSessionCreateFailedException("Error in creating android Driver instance for Android");
		}
	}


	public AndroidDriver getDriverInstance(String env) throws TestEngineException {
		
		AndroidDriver driver = null;
		ExecutionType exeType= EnvSettingsProvider.getInstance().getExecutionType(env);
		try {
			
			if(exeType.equals(ExecutionType.SauceLabs)){
				driver = getSauceLabsWebDriver(env);
			}else if(exeType.equals(ExecutionType.BrowserStack)){
				driver = getBrowserStackWebDriver(env);
			} else {
				driver = createDriver(env);
			}
		}catch (Exception e) {
			logger.error(e,e);
			throw new TestsigmaDriverSessionCreateFailedException(ErrorCodes.DRIVER_NOT_CREATED,
					StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_NOTCREATED, e.getMessage()));

		}

		return driver;
	}
	

	
	private AndroidDriver createDriver(String env) {
		AndroidDriver androidDriver = null;
		try {
			Capabilities desiredCapabilities = getDesiredCapabilities(env);
			URL remoteAddress = new URL(EnvSettingsProvider.getInstance().getAppiumUrl(env));
			androidDriver = new AndroidDriver<WebElement>(remoteAddress, desiredCapabilities);
			androidDriver.manage().timeouts().implicitlyWait(EnvSettingsProvider.getInstance().getElementTimeout(env), TimeUnit.SECONDS);
			
			//androidDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),desiredCapabilities);
		} catch (Exception e) {
			logger.error(e, e);
			//System.exit(-1);
		}

		return androidDriver;
	}
	
	private AndroidDriver getSauceLabsWebDriver(String env) throws Exception {
		AndroidDriver androidDriver = null;
		   
		try {
			Integer elementTimeout = EnvSettingsProvider.getInstance().getElementTimeout(env);
			Integer  pageloadTimeout = EnvSettingsProvider.getInstance().getPageTimeout(env);
			DesiredCapabilities caps = DesiredCapabilities.android();
			//caps.setCapability("appiumVersion", "1.6.4");
			caps.setCapability("deviceName", EnvSettingsProvider.getInstance().getDeviceName(env));
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("platformVersion", EnvSettingsProvider.getInstance().getOsVersion(env));
			caps.setCapability("platformName", Platforms.Android.name());
			caps.setCapability("testobject_api_key", EnvSettingsProvider.getInstance().getTestObjectKey(env));
			caps.setCapability("testobject_device", EnvSettingsProvider.getInstance().getDeviceName(env));

			androidDriver = new AndroidDriver<>(new URL(EnvSettingsProvider.getInstance().getSauseLabUrl(env)
					.replace("ondemand.saucelabs.com:443", "us1.appium.testobject.com")), caps);
			
			androidDriver.manage().timeouts().implicitlyWait(elementTimeout, TimeUnit.SECONDS);
			
			
		} catch (Exception e) {
			logger.error(e, e);
			throw e;
		}

		return androidDriver;
	}
	
	
	private AndroidDriver getBrowserStackWebDriver(String env) throws Exception {
		AndroidDriver androidDriver = null;
		   
		try {
			DesiredCapabilities caps = DesiredCapabilities.android();
			
				
		    caps.setCapability("app",EnvSettingsProvider.getInstance().getAppUrl(env));
				
			
		    caps.setCapability("realMobile", "true");
			caps.setCapability("deviceOrientation", "portrait");
			caps.setCapability("os", Platforms.Android.name().toLowerCase());
			caps.setCapability("device", EnvSettingsProvider.getInstance().getDeviceName(env));
			caps.setCapability("platform", Platforms.Android.name().toUpperCase());
			
			//Capabilities desiredCapabilities = getDesiredCapabilities(settings);
			androidDriver = new AndroidDriver<>(new URL(EnvSettingsProvider.getInstance().getBrowserStackUrl(env)), caps);
			androidDriver.manage().timeouts().implicitlyWait(EnvSettingsProvider.getInstance().getElementTimeout(env), TimeUnit.SECONDS);
			
		} catch (Exception e) {
			logger.error(e, e);
			throw e;
		}

		return androidDriver;
	}

	/**
	 * Gets the desired ie capabilities.
	 * 
	 * @return the desired ie capabilities
	 * @throws TestEngineException 
	 */
	private DesiredCapabilities getDesiredCapabilities(String env) throws TestEngineException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		String apkPath = EnvSettingsProvider.getInstance().getApkUrl(env);
		if(!StringUtil.isEmpty(apkPath)){
			capabilities.setCapability("app", apkPath);
		}
		capabilities.setCapability(MobileCapabilityType.VERSION, EnvSettingsProvider.getInstance().getOsVersion(env));
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platforms.Android.name());
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, EnvSettingsProvider.getInstance().getDeviceName(env));
		capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, EnvSettingsProvider.getInstance().getAppPackageName(env));
		capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, EnvSettingsProvider.getInstance().getAppActivityName(env));
		
		return capabilities;
	}

	/**
	 * End session.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return true, if successful
	 */
	public boolean endSession(String executionID) throws Exception{
		boolean sessionEnded;
		AndroidDriver driver = getDriver(executionID);
		try {
			driver.quit();
			sessionEnded = true;
		} catch (Exception e) {
			logger.error("Exception in ending WebDriver session: ", e);
			logger.info("Quitting WebDriver in order to end the browser session");
			driver.quit();
			logger.info("WebDriver browser session ended (Browser closed)");
			sessionEnded = true;
		}

		RuntimeDataProvider.getInstance().clearRunTimeData(executionID);
		
		return sessionEnded;
	}

	@Override
	public void performCleanUpAction(ExecutionType exeType, String executionID, Integer actionType)
			throws TestEngineException {
		// TODO Auto-generated method stub
		
	}
	
}
