package com.testsigma.testengine.drivers;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

import java.math.BigInteger;
import java.net.URL;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.testsigma.testengine.constants.TestsigmaCapabilityType;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionCreateFailedException;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.providers.RuntimeDataProvider;
/**
 * The Class AndroidDriverManager is used to manage WebDriver instance for execution.
 */
public class MobileDriverManager {
	static Logger logger = Logger.getLogger(MobileDriverManager.class);
	private static MobileDriverManager instance;
	private Map<String, AndroidDriver> driverMap = new HashMap<String, AndroidDriver>();
	
	/**
	 * Instantiates a new appium driver manager.
	 */
	private MobileDriverManager() {
		// setExecutionBrowsers(ExecutionSettings.getInstance().getExecutionBrowsers());
	}

	/**
	 * Gets the single instance of AppiumDriverManager.
	 * 
	 * @return single instance of AppiumDriverManager
	 */
	public static synchronized MobileDriverManager getInstance() {
		if (instance == null) {
			instance = new MobileDriverManager();
		}
		return instance;
	}

	/**
	 * Gets the web driver.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return the web driver
	 */
	public AndroidDriver getDriver(String executionID) throws TestsigmaDriverSessionNotFoundException {
		AndroidDriver driver = null;

		if (driverMap.containsKey(executionID)) {
			driver = driverMap.get(executionID);
		} else {
			logger.error("There is no WebDriver instance associated with the executionID:: " + executionID);
			logger.error("WebDriver instance is set to null");
			throw new TestsigmaDriverSessionNotFoundException("Webdriver session not found for executionID \""+ executionID + "\"");
		}

		return driver;
	}

	/**
	 * Start session.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the string
	 */
	public String startSession(int appType, int elementTimeout, int pageloadTimeout, Map<String, String> settings) throws Exception {
		String executionID;
		AndroidDriver androidDriver = getDriverInstance(appType, elementTimeout, pageloadTimeout, settings);
		if (androidDriver != null) {
			SecureRandom random = new SecureRandom();
			executionID = new BigInteger(130, random).toString(32);
			driverMap.put(executionID, androidDriver);
			logger.info("Driver instance:: " + androidDriver);
			return executionID;
		} else {
			executionID = "NOT_CREATED";
			throw new TestsigmaDriverSessionCreateFailedException("Error in creating android Driver instance for Android");
		}
	}


	/**
	 * Gets the web driver instance.
	 * 
	 * @param executionBrowser
	 *            the execution browser
	 * @return the web driver instance
	 */
	private AndroidDriver getDriverInstance(int appType, int elementTimeout, int pageloadTimeout, Map<String, String> settings) {
		AndroidDriver androidDriver = null;
		   
		try {
			Capabilities desiredCapabilities = getDesiredCapabilities(appType, settings);
			URL remoteAddress = new URL(settings.get(TestsigmaCapabilityType.APPIUM_URL));
			androidDriver = new AndroidDriver<>(remoteAddress, desiredCapabilities);
			androidDriver.manage().timeouts().implicitlyWait(elementTimeout, TimeUnit.SECONDS);
			
		} catch (Exception e) {
			logger.error(e, e);
			//System.exit(-1);
		}

		return androidDriver;
	}

	/**
	 * Gets the desired ie capabilities.
	 * 
	 * @return the desired ie capabilities
	 */
	private DesiredCapabilities getDesiredCapabilities(int appType, Map<String, String> settings) {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		   
		capabilities.setCapability(MobileCapabilityType.VERSION, settings.get(MobileCapabilityType.VERSION));
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, settings.get(MobileCapabilityType.PLATFORM_NAME));
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, settings.get(MobileCapabilityType.DEVICE_NAME));
		capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, settings.get(AndroidMobileCapabilityType.APP_PACKAGE));
		capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, settings.get(AndroidMobileCapabilityType.APP_ACTIVITY));
		
		return capabilities;
	}

	/**
	 * End session.
	 * 
	 * @param executionID
	 *            the execution id
	 * @return true, if successful
	 */
	public boolean endSession(String executionID) throws Exception{
		boolean sessionEnded;
		AndroidDriver driver = getDriver(executionID);
		try {
			driver.quit();
			sessionEnded = true;
		} catch (Exception e) {
			logger.error("Exception in ending WebDriver session: ", e);
			logger.info("Quitting WebDriver in order to end the browser session");
			driver.quit();
			logger.info("WebDriver browser session ended (Browser closed)");
			sessionEnded = true;
		}

		RuntimeDataProvider.getInstance().clearRunTimeData(executionID);
		
		return sessionEnded;
	}
	
}
