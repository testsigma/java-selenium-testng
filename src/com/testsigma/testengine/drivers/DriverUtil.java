package com.testsigma.testengine.drivers;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.testsigma.testengine.constants.ApplicationType;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.providers.EnvSettingsProvider;


public class DriverUtil {
	static final Logger log  = Logger.getLogger(DriverUtil.class);
 
	public static String createDriverManager(String env) throws TestEngineException{
		
		ApplicationType appType = EnvSettingsProvider.getInstance().getApplicationType(env);
		
			String executionID = null;
			switch (appType) {
			
				case WebApplication:	
					
					executionID = WebDriverManager.getInstance().startSession(env);
					break;
				
				case AndroidWeb:
				
					executionID = AndroidWebDriverManager.getInstance().startSession(env);
					break;
				
				case AndroidNative:
					
					executionID = AndroidNativeDriverManager.getInstance().startSession(env);
					break;
				
				case AndroidHybrid:
					executionID = AndroidNativeDriverManager.getInstance().startSession(env);				
					break;
				case IOSWeb:
					executionID = IOSWebDriverManager.getInstance().startSession(env);				
					break;
				case IOSNative:
					executionID = IOSNativeDriverManager.getInstance().startSession(env);				
					break;
					
				default:
					break;
			}	
			log.debug("Execution Id : " + executionID);	
			return executionID;
	}
	
	
public static WebDriver getDriverManager(String executionID) throws TestEngineException{
	ApplicationType appType = EnvSettingsProvider.getInstance().getApplicationType(executionID);
			WebDriver driver =null;
			switch (appType) {
			
				case WebApplication:	
					
					driver = WebDriverManager.getInstance().getDriver(executionID);
					
					break;
				
				case AndroidWeb:
				
					driver = AndroidWebDriverManager.getInstance().getDriver(executionID);
		
					break;
				
				case AndroidNative:
					
					driver = AndroidNativeDriverManager.getInstance().getDriver(executionID);
					
					break;
					
				case IOSWeb:
					
					driver = IOSWebDriverManager.getInstance().getDriver(executionID);
					
					break;
					
				case IOSNative:
					
					driver = IOSNativeDriverManager.getInstance().getDriver(executionID);	
					
					break;
					
				default:
					
					break;
			}	
			log.debug("Execution Id : " + executionID);	
			return driver;
	}
}
