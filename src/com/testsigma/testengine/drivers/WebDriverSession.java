package com.testsigma.testengine.drivers;

import java.util.HashMap;
import java.util.Map;

public class WebDriverSession {
	
/*	private Integer platform;
	private String browser;*/
	private int elementTimeout;
	private int pageloadTimeout;
/*	private String version;*/
	private Map<String, String> settings = new HashMap<String, String>();
	
	public  WebDriverSession(int elementTimeout,int pageloadTimeout){
/*		this.platform = platform;
		this.browser= browser;*/
		this.elementTimeout = elementTimeout;
		this.pageloadTimeout = pageloadTimeout;
	}
	
/*	public Integer getPlatform() {
		return platform;
	}

	public void setPlatform(Integer platform) {
		this.platform = platform;
	}
	
	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}*/

	public int getElementTimeout() {
		return elementTimeout;
	}
	public void setElementTimeout(int elementTimeout) {
		this.elementTimeout = elementTimeout;
	}
	public int getPageloadTimeout() {
		return pageloadTimeout;
	}
	public void setPageloadTimeout(int pageloadTimeout) {
		this.pageloadTimeout = pageloadTimeout;
	}

	private Map<String, String> getSettings() {
		return settings;
	}

	private void setSettings(Map<String, String> settings) {
		this.settings = settings;
	}
	
}
