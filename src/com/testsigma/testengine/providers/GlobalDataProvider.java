package com.testsigma.testengine.providers;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.exceptions.TestsigmaTestdataNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.utilities.StringUtil;


/**
 * This class help the user create a singleton instance of runtimedata, which is re-used per
 * testSuite, per Execution-session to store and access runtime data across various classes and
 * functions.
 * 
 */

public class GlobalDataProvider {
	
	private static GlobalDataProvider _instance=null; 
	
	private GlobalDataProvider(){
		
	}
	public static GlobalDataProvider getInstance(){
		if(_instance==null){
			_instance= new GlobalDataProvider();
		}
		return _instance;
	}
    private Map<String,Map<String,String>> globalData = new HashMap<String,Map<String,String>>();
	static Logger logger = Logger.getLogger(GlobalDataProvider.class);

	public void clearRunTimeData(String executionID) {
		
      //TODO: remove from database		
	}
	
	public String getGlobalData(String environmentId, String varibleName) throws TestsigmaTestdataNotFoundException,TestEngineException {
		
		if (globalData.containsKey(environmentId)) {
			logger.debug(new Gson().toJson(globalData));
			Map<String, String> eRuntimeData = globalData.get(environmentId);
			String data = null;
			if (eRuntimeData.containsKey(varibleName)) {
				data = eRuntimeData.get(varibleName);
				logger.debug(data);
				return data;
			} else {
				throw new TestsigmaTestdataNotFoundException(
						StringUtil.getMessage(MessageConstants.EXCEPTION_UNKNOWN_TESTDATA,varibleName));

			}
		} else {
			throw new TestsigmaTestdataNotFoundException(
					StringUtil.getMessage(MessageConstants.EXCEPTION_UNKNOWN_TESTDATA,varibleName));
		}
	
	}

	
	public void storeGlobalData(String executionID, String variableName, String value) throws TestEngineException{
		
		
		if(globalData.containsKey(executionID)){
			logger.info("runtimedata is found for execution with id \""+ executionID + "\"");
			if(globalData.get(executionID).containsKey(variableName)){
				String oldValue = globalData.get(executionID).get(variableName);
				globalData.get(executionID).put(variableName, value);
				logger.info("Old value \"" + oldValue + "\" is replaced with new value for variable name \""+ variableName+ "\"");
			}else{
				globalData.get(executionID).put(variableName, value);
				logger.info("new variable \""+variableName + "\" is added to runtimedata with a value \"" +value+ "\"");
				// new key and value are added
			}
		}else{
			// no data for the sessionID found, hence must be added to store runtimevariable.
			logger.info("runtimedata is not found for execution with id \""+ executionID + "\" , hence will be created");
			HashMap<String, String> pair = new HashMap<String, String>();
			pair.put(variableName, value);
			globalData.put(executionID, pair);
			logger.info("new variable \""+variableName + "\" is added to runtimedata with a value \"" +value+ "\"");
		}
	}



}
