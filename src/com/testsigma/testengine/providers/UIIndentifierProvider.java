package com.testsigma.testengine.providers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UIIndentifierProvider {
	String fileName = "uiidentifiers/UIIdentifiers.json";
	private String type = "type";
	private String value ="value";
	private Map<String,Map<String,String>> uiIdentifierMap=new HashMap<String,Map<String,String>>();
	public UIIndentifierProvider(){
		load();
	}
	public void load(){
		
		try {
			
			String configPath = System.getProperty("user.dir") + File.separator+ fileName ;
			InputStream is = new FileInputStream(new File(configPath));
			BufferedReader toReturn = new BufferedReader(new InputStreamReader(is));
			
			Type type = new TypeToken<Map<String,Map<String,String>>>(){}.getType();
			uiIdentifierMap = new Gson().fromJson(toReturn,type);
			
		} catch (FileNotFoundException e) {
			
		}
		
	}
	
	public String getType(String name){
		return uiIdentifierMap.get(name).get(type);
	}
	
	public String getLocator(String name){
		return uiIdentifierMap.get(name).get(value);
	}
}
