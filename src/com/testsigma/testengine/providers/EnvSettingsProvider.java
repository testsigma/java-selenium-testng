package com.testsigma.testengine.providers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.testsigma.testengine.constants.ApplicationType;
import com.testsigma.testengine.constants.Browsers;
import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.Platforms;
import com.testsigma.testengine.exceptions.TestEngineException;

public class EnvSettingsProvider {
	private static EnvSettingsProvider _instance;
	private final static String ENV_SETTINGS_PATH = "environments";
	private final static String ENV_SETTINGS_TYPE = ".properties";
	
	public static final String KEYS_BROWSER_VERSION_FOUND = "browser_version_found";
	private static final String KEYS_BROWSER = "browser";
	private static final String KEYS_BROWSER_VERSION = "browserVersion";
	private static final String KEYS_PLATFORM = "platform";
	private static final String KEYS_PLATFORM_VERSION = "osVersion";
	private static final String KEYS_TESTOBJECT_API_KEY = "testobjectApiKey";
	public static final String KEYS_SAUCELABS_URL = "saucelabs_url";
	private static final String KEY_BROWSERSTACK_URL = "browserstack_url";
	private static final String KEY_BROWSERSTACK_USERNAME = "browserstack_username";
	private static final String KEY_BROWSERSTACK_PASSWORD = "browserstack_password";
	private static final String KEY_APK_PATH = "apkPath";
	private static final String USER_NAME = "agent";
	private static final String USER_NAME_KEY = "user_name";
	private static final String PASSWORD = "testsigma";
	private static final String PASSWORD_KEY = "password";
	private static final String KEYS_BUNDLE_ID = "bundleId";
	
	private static final String ELEMENT_TIMEOUT = "elementTimeout";
	private static final String PAGE_TIMEOUT = "pageTimeout";
	private static final String SCREENSHOT_PATH= "screenshotPath";
	private static final String GLOBAL_PARAMETER= "globalParameter";
	private static final String RECOVERY_ACTIONS= "recoveryActions";
	private static final String ABORTED_ACTIONS= "abortedActions";
	private static final String APPLICATION_TYPE= "applicationType";
	private static final String DRIVER_PATH= "driverPath";
	private static final String EXECUTION_TYPE= "executionType";
	private static final String NODE_URL = "nodeUrl";
	
	private static final String DEVICE_NAME = "deviceName";
	private static final String APP_PACKAGE_NAME = "appPackageName";
	private static final String APP_ACTIVITY_NAME = "appActivityName";
	private static final String APPIUM_URL = "appiumUrl";
	private static final String APP_URL = "appUrl";
	private static final String APK_PATH = "apkPath";
	private static final String UDID = "UDID";
	private static final String BUNDLE_ID = "bundleId";
	private static final String APP = "app";
	



	public static String getSaucelabsUrl(String username, String key){
		return "https://" + username + ":" + key + "@ondemand.saucelabs.com:443/wd/hub";
		
	}
	
	public static String getBrowserStackUrl(String username, String key){
		return "https://" + username + ":" + key + "@hub-cloud.browserstack.com/wd/hub";
		
	}
	private static Map<String,Map<String,String>> envsSetting = new HashMap<String,Map<String,String>>();
	
	private EnvSettingsProvider() throws TestEngineException {
		
	}
	public synchronized static EnvSettingsProvider getInstance() throws TestEngineException{
		if(_instance==null){
			_instance = new EnvSettingsProvider();
		}
		return _instance;
	}
	public String getDriverFile(String name, String fileName){
		return null;
	}
	public synchronized void load(String envName) throws TestEngineException {
		
		try {   
			
			StringBuffer sb = new StringBuffer(System.getProperty("user.dir")).append( File.separator )
					.append(ENV_SETTINGS_PATH).append(File.separator).append( envName ).append( ENV_SETTINGS_TYPE );
			
			InputStream in = new FileInputStream(new File(sb.toString()));
			Properties prop = new Properties();
		       
			prop.load(in); 
			load(envName, prop);
		} catch (final IOException e) {           
			throw new TestEngineException( "Bad InputStream, failed to load properties from file.", e);
		}

	}
	private void load(String env_name, Properties properties){
		Map<String,String> toReturn = new HashMap<String,String>();
		for (final String name: properties.stringPropertyNames()){
			toReturn.put(name, properties.getProperty(name));
		}
		envsSetting.put(env_name, toReturn);
		//System.out.println("getEnvironmentSetting::"+new Gson().toJson(envsSetting));
	}

	public synchronized String getProperty(String env_name, String propertyName){
		//System.out.println("get Property::"+new Gson().toJson(envsSetting)+"  "+env_name+" "+propertyName+" "+envsSetting.get(env_name));
		return envsSetting.get(env_name).get(propertyName);
	}
	public synchronized Map<String,String> getEnvironmentSetting(String env_name){
		return envsSetting.get(env_name);
	}
	
	public String getBrowser(String envName){
		return getProperty(envName, KEYS_BROWSER);
	}
	public String getBrowserVersion(String envName){
		return getProperty(envName, KEYS_BROWSER_VERSION);
	}
	public Platforms getPlatform(String envName){
		return Platforms.valueOf(getProperty(envName, KEYS_PLATFORM));
	}
	public String getOsVersion(String envName){
		return getProperty(envName, KEYS_PLATFORM_VERSION);
	}
	public String getTestObjectKey(String envName){
		return getProperty(envName, KEYS_TESTOBJECT_API_KEY);
	}
	public String getSauseLabUrl(String envName){
		return getProperty(envName, KEYS_SAUCELABS_URL);
	}
	public String getBrowserStackUrl(String envName){
		return getProperty(envName, KEY_BROWSERSTACK_URL);
	}
	public String getBrowserStackUsername(String envName){
		return getProperty(envName, KEY_BROWSERSTACK_USERNAME);
	}
	public String getBrowserStackPassword(String envName){
		return getProperty(envName, KEY_BROWSERSTACK_PASSWORD);
	}
	public String getBrowserStackApkPath(String envName){
		return getProperty(envName, KEY_APK_PATH);
	}
	public Integer getElementTimeout(String envName){
		return Integer.parseInt(getProperty(envName, ELEMENT_TIMEOUT));
	}
	public Integer getPageTimeout(String envName){
		return Integer.parseInt(getProperty(envName, PAGE_TIMEOUT));
	}
	public String getScreenshotPath(String envName){
		return getProperty(envName, SCREENSHOT_PATH);
	}
	public ApplicationType getApplicationType(String envName){
		return ApplicationType.valueOf(getProperty(envName, APPLICATION_TYPE));
	}
	public String getDriverPath(String envName){
		return getProperty(envName, DRIVER_PATH);
	}
	public ExecutionType getExecutionType(String envName){
		return ExecutionType.valueOf(getProperty(envName, EXECUTION_TYPE));
	}
	public String getDeviceName(String envName){
		return getProperty(envName, DEVICE_NAME);
	}
	public String getAppPackageName(String envName){
		return getProperty(envName, APP_PACKAGE_NAME);
	}
	public String getAppActivityName(String envName){
		return getProperty(envName, APP_ACTIVITY_NAME);
	}
	public String getAppiumUrl(String envName){
		return getProperty(envName, APPIUM_URL);
	}
	public String getAppUrl(String envName){
		return getProperty(envName, APP_URL);
	}
	public String getApkUrl(String envName){
		return getProperty(envName, APK_PATH);
	}
	public String getUDID(String envName){
		return getProperty(envName, UDID);
	}
	public String getBundleId(String envName){
		return getProperty(envName, BUNDLE_ID);
	}
	
	public String getNodeUrl(String envName){
		return getProperty(envName, NODE_URL);
	}
	public String getApp(String envName){
		return getProperty(envName, APP);
	}
}
