package com.testsigma.testengine.providers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.testsigma.testengine.exceptions.TestEngineException;

public class TestDataProvider {
	
	public Map<String, String> getTestDataMap(String dataProfile) throws TestEngineException {
		
		Map<String, String> testdata = new HashMap<String, String>();
		
		try {
			
			String configPath = System.getProperty("user.dir") + File.separator+"testdata" + File.separator + dataProfile ;
			InputStream is = new FileInputStream(new File(configPath));
			BufferedReader toReturn = new BufferedReader(new InputStreamReader(is));
			
			Type type = new TypeToken<Map<String,String>>(){}.getType();
			testdata = new Gson().fromJson(toReturn,type);
		
		} catch (FileNotFoundException e) {
			throw new TestEngineException( "Bad InputStream, failed to load testdata file.", e);
		}
		
		
		return testdata;
	}
}