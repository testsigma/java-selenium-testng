package com.testsigma.testengine.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class TestCaseResult {
	static Logger logger = Logger.getLogger(TestCaseResult.class);
	private Long testCaseId;
	private Timestamp startTime;
	private Timestamp endTime;
	private Integer status;
	private String message;
	private String testDataId;
	private Boolean dataDriven;
	private String dataSetName;
	private Long duration;
	private Integer errorCode;
	private Boolean isManual;
	List<TestCaseStepResult> testCaseStepResults=new ArrayList<TestCaseStepResult>();
	
	public TestCaseResult() {
	}


	public TestCaseResult(Long testCaseId) {
		this.testCaseId = testCaseId;
	}


	public static Logger getLogger() {
		return logger;
	}


	public static void setLogger(Logger logger) {
		TestCaseResult.logger = logger;
	}
	
	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	
	public Boolean getIsManual() {
		return isManual;
	}


	public void setIsManual(Boolean isManual) {
		this.isManual = isManual;
	}


	public List<TestCaseStepResult> getTestCaseStepResults() {
		return testCaseStepResults;
	}


	public void setTestCaseStepResults(List<TestCaseStepResult> testCaseStepResults) {
		this.testCaseStepResults = testCaseStepResults;
	}


	public Long getTestCaseId() {
		return testCaseId;
	}


	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public Timestamp getEndTime() {
		return endTime;
	}


	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}


	public Integer getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public Long getDuration() {
		return duration;
	}


	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Boolean getDataDriven() {
		return dataDriven;
	}


	public void setDataDriven(Boolean dataDriven) {
		this.dataDriven = dataDriven;
	}

	public String getDataSetName() {
		return dataSetName;
	}

	public void setDataSetName(String dataSetName) {
		this.dataSetName = dataSetName;
	}
}
