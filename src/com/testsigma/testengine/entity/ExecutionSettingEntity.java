package com.testsigma.testengine.entity;

import java.io.Serializable;

public class ExecutionSettingEntity implements Serializable{
	/**
	 */
	private static final long serialVersionUID = -6817650049576085715L;
	private Long id;
	private String name;
	private Integer elementTimeOut;
    private Integer pageTimeOut;
    private Long appVersionId;
    private String mailList;
    private String screenshotPath;
    private Integer screenshot;
    private Long globalParameters;
	private Integer recoveryAction;
	private Integer onAbortedAction;
	private Integer onGroupPrequisiteFail;
	private Integer onTestcasePrequisiteFail;
	private Integer onStepPrequisiteFail;
	private Integer type;
	
	
	public ExecutionSettingEntity() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	public Integer getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(Integer screenshot) {
		this.screenshot = screenshot;
	}

	public Integer getElementTimeOut() {
		return elementTimeOut;
	}

	public void setElementTimeOut(Integer elementTimeOut) {
		this.elementTimeOut = elementTimeOut;
	}

	public Integer getPageTimeOut() {
		return pageTimeOut;
	}

	public void setPageTimeOut(Integer pageTimeOut) {
		this.pageTimeOut = pageTimeOut;
	}

	public Long getAppVersionId() {
		return appVersionId;
	}

	public void setAppVersionId(Long appVersionId) {
		this.appVersionId = appVersionId;
	}

	public String getMailList() {
		return mailList;
	}

	public void setMailList(String mailList) {
		this.mailList = mailList;
	}

	public Long getGlobalParameters() {
		return globalParameters;
	}

	public void setGlobalParameters(Long globalParameters) {
		this.globalParameters = globalParameters;
	}

	public String getScreenshotPath() {
		return screenshotPath;
	}

	public void setScreenshotPath(String screenshotPath) {
		this.screenshotPath = screenshotPath;
	}

	public Integer getOnGroupPrequisiteFail() {
		return onGroupPrequisiteFail;
	}

	public void setOnGroupPrequisiteFail(Integer onGroupPrequisiteFail) {
		this.onGroupPrequisiteFail = onGroupPrequisiteFail;
	}

	public Integer getOnTestcasePrequisiteFail() {
		return onTestcasePrequisiteFail;
	}

	public void setOnTestcasePrequisiteFail(Integer onTestcasePrequisiteFail) {
		this.onTestcasePrequisiteFail = onTestcasePrequisiteFail;
	}

	public Integer getOnStepPrequisiteFail() {
		return onStepPrequisiteFail;
	}

	public void setOnStepPrequisiteFail(Integer onStepPrequisiteFail) {
		this.onStepPrequisiteFail = onStepPrequisiteFail;
	}

	public Integer getRecoveryAction() {
		return recoveryAction;
	}
	

	public void setRecoveryAction(Integer recoveryAction) {
		this.recoveryAction = recoveryAction;
	}

	public Integer getOnAbortedAction() {
		return onAbortedAction;
	}

	public void setOnAbortedAction(Integer onAbortedAction) {
		this.onAbortedAction = onAbortedAction;
	}

	
}
