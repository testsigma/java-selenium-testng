package com.testsigma.testengine.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.testsigma.testengine.keywords.Keyword;

public class TestCaseStepEntity {

	static Logger logger = Logger.getLogger(TestCaseStepEntity.class);
	
	public static final String REST_DETAILS_KEY = "rest_details";
	public static final String DATABASE_DETAILS_KEY = "db_details";
	
	private Long executionID;
	private Long testcaseId;
	private Long id;
	private String keyword;
	private String parentFieldName;
	private String fieldName;
	private String fieldDefinition;
	private String testDataType;
	private String testDataName;
	private String testDataValue;
	private String checkPoint;
	private String comments;
	private String stepResult;
	private String keywordType;
	private String errDescription;
	private Integer errorCode;
	private String screenName;
	private String locatorStrategy;
	private Boolean skipped;
	private String filters;
	private String teststepKey;
	private String attribute;
	private boolean expectedToFail = false;
	private boolean isComponent = false;
	private boolean isMandatory=false;
	private Long parentId;
	private Integer priority;
	private Integer screenshot;
	private String screenshotPath;
	private Long preRequisite;
	private Long stepId;
	private Integer type;
	private Map<String, Object> additionalData = new HashMap<String, Object>();
	private List<TestCaseStepEntity> testSteps = new ArrayList<TestCaseStepEntity>();

	public TestCaseStepEntity() {
	}

	public TestCaseStepEntity(
			Long executionID, Long testcaseId,Long id, Integer type) {
		
		this.executionID = executionID;
		this.testcaseId = testcaseId;
		this.id = id;
		this.type = type;
	}

	/**
	 * Gets the keyword.
	 * 
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * Sets the keyword.
	 * 
	 * @param keyword
	 *            the new keyword
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * Gets the check point.
	 * 
	 * @return the check point
	 */
	public String getCheckPoint() {
		return checkPoint;
	}

	/**
	 * Sets the check point.
	 * 
	 * @param checkPoint
	 *            the new check point
	 */
	public void setCheckPoint(String checkPoint) {
		this.checkPoint = checkPoint;
	}

	/**
	 * Gets the field name.
	 * 
	 * @return the field name
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * Sets the field name.
	 * 
	 * @param fieldName
	 *            the new field name
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * Gets the field definition.
	 * 
	 * @return the field definition
	 */
	public String getFieldDefinition() {
		return fieldDefinition;
	}

	/**
	 * Sets the field definition.
	 * 
	 * @param fieldDefinition
	 *            the new field definition
	 */
	public void setFieldDefinition(String fieldDefinition) {
		this.fieldDefinition = fieldDefinition;
	}

	/**
	 * Gets the testcase id.
	 * 
	 * @return the testcase id
	 */
	public Long getTestcaseId() {
		return testcaseId;
	}

	/**
	 * Sets the testcase id.
	 * 
	 * @param testcaseID
	 *            the new testcase id
	 */
	public void setTestcaseId(Long testcaseId) {
		this.testcaseId = testcaseId;
	}

	/**
	 * Gets the step id.
	 * 
	 * @return the step id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the step id.
	 * 
	 * @param stepID
	 *            the new step id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public String getTeststepKey() {
		return teststepKey;
	}

	public void setTeststepKey(String teststepKey) {
		this.teststepKey = teststepKey;
	}

	/**
	 * Gets the comments.
	 * 
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 * 
	 * @param comments
	 *            the new comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the error code.
	 * 
	 * @return the error code
	 */
	public Integer getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code.
	 * 
	 * @param errorCode
	 *            the new error code
	 */
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the err description.
	 * 
	 * @return the err description
	 */
	public String getErrDescription() {
		return errDescription;
	}

	/**
	 * Sets the err description.
	 * 
	 * @param errDescription
	 *            the new err description
	 */
	public void setErrDescription(String errDescription) {
		this.errDescription = errDescription;
	}

	/**
	 * Gets the test data value.
	 * 
	 * @return the test data value
	 */
	public String getTestDataValue() {
		return testDataValue;
	}

	/**
	 * Sets the test data value.
	 * 
	 * @param testDataValue
	 *            the new test data value
	 */
	public void setTestDataValue(String testDataValue) {
		this.testDataValue = testDataValue;
	}

	/**
	 * Gets the step result.
	 * 
	 * @return the step result
	 */
	public String getStepResult() {
		return stepResult;
	}

	/**
	 * Sets the step result.
	 * 
	 * @param stepResult
	 *            the new step result
	 */
	public void setStepResult(String stepResult) {
		this.stepResult = stepResult;
	}

	public Long getPreRequisite() {
		return preRequisite;
	}

	public void setPreRequisite(Long preRequisite) {
		this.preRequisite = preRequisite;
	}

	public String getScreenshotPath() {
		return screenshotPath;
	}

	public void setScreenshotPath(String screenshotPath) {
		this.screenshotPath = screenshotPath;
	}

	public boolean getIsMandatory() {
		return isMandatory;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	/**
	 * Gets the step details.
	 * 
	 * @return the step details
	 */
	public String getStepDetails() {
		Map<String, Object> stepDetails = new LinkedHashMap<String, Object>();

		try {
			stepDetails.put("StepID", this.getId());
			stepDetails.put("FieldName", this.getFieldName());
			stepDetails.put("FieldDefinition", this.getFieldDefinition());
			stepDetails.put("Keyword", this.getKeyword());
			stepDetails.put("TestDataType", this.getTestDataType());
			stepDetails.put("TestDataName", this.getTestDataName());
			stepDetails.put("TestDataValue", this.getTestDataValue());
			stepDetails.put("Filters", this.getFilters());
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("Exception in fetching stepDetails - \n StacktraceInfo:: " + e.getMessage());
		}

		return stepDetails.toString();
	}

	/**
	 * Gets the keyword type.
	 * 
	 * @return the keyword type
	 */
	public String getKeywordType() {
		return keywordType;
	}

	/**
	 * Sets the keyword type.
	 * 
	 * @param keywordType
	 *            the new keyword type
	 */
	public void setKeywordType(String keywordType) {
		this.keywordType = keywordType;
	}

	/**
	 * Gets the test data type.
	 * 
	 * @return the test data type
	 */
	public String getTestDataType() {
		return testDataType;
	}



	/**
	 * Sets the test data type.
	 * 
	 * @param testDataType
	 *            the new test data type
	 */
	public void setTestDataType(String testDataType) {
		this.testDataType = testDataType;
	}

	/**
	 * Gets the test data name.
	 * 
	 * @return the test data name
	 */
	public String getTestDataName() {
		return testDataName;
	}

	/**
	 * Sets the test data name.
	 * 
	 * @param testDataName
	 *            the new test data name
	 */
	public void setTestDataName(String testDataName) {
		this.testDataName = testDataName;
	}

	/**
	 * Gets the parent field name.
	 * 
	 * @return the parent field name
	 */
	public String getParentFieldName() {
		return parentFieldName;
	}

	/**
	 * Sets the parent field name.
	 * 
	 * @param parentFieldName
	 *            the new parent field name
	 */
	public void setParentFieldName(String parentFieldName) {
		this.parentFieldName = parentFieldName;
	}

	/**
	 * Gets the screen name.
	 * 
	 * @return the screen name
	 */
	public String getScreenName() {
		return screenName;
	}

	/**
	 * Sets the screen name.
	 * 
	 * @param screenName
	 *            the new screen name
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	/**
	 * Gets the locator strategy.
	 * 
	 * @return the locator strategy
	 */
	public String getLocatorStrategy() {
		return locatorStrategy;
	}

	/**
	 * Sets the locator strategy.
	 * 
	 * @param locatorStrategy
	 *            the new locator strategy
	 */
	public void setLocatorStrategy(String locatorStrategy) {
		this.locatorStrategy = locatorStrategy;
	}

	/**
	 * Gets the execution id.
	 * 
	 * @return the execution id
	 */
	public Long getExecutionID() {
		return executionID;
	}

	/**
	 * Sets the execution id.
	 * 
	 * @param executionID
	 *            the new execution id
	 */
	public void setExecutionID(Long executionID) {
		this.executionID = executionID;
	}

	/**
	 * Checks if is skipped.
	 * 
	 * @return true, if is skipped
	 */

	/**
	 * Sets the skipped.
	 * 
	 * @param skipped
	 *            the new skipped
	 */
	public void setIsSkipped(Boolean skipped) {
		this.skipped = skipped;
	}

	public Boolean getIsSkipped() {
		return skipped;
	}

	

	public void setIsComponent(boolean isComponent) {
		this.isComponent = isComponent;
	}

	/**
	 * Gets the filters.
	 * 
	 * @return the filters
	 */
	public String getFilters() {
		return filters;
	}

	/**
	 * Sets the filters.
	 * 
	 * @param filters
	 *            the new filters
	 */
	public void setFilters(String filters) {
		this.filters = filters;
	}

	/**
	 * Checks if is filter criteria matched.
	 * 
	 * @return true, if is filter criteria matched
	 */
	public static boolean isFilterCriteriaMatched() {
		//ExecutionSettings execSettings = ExecutionSettings.getInstance();
		//ev.get
		//String applicationFilters = execSettings.getEnvironmentFilters("");

		return false;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Map<String, Object> getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(Map<String, Object> additionalData) {
		this.additionalData = additionalData;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public boolean getIsComponent() {
		return isComponent;
	}

	
	public List<TestCaseStepEntity> getTestSteps() {
		return testSteps;
	}

	public void setTestSteps(List<TestCaseStepEntity> testSteps) {
		this.testSteps = testSteps;
	}

	public void setExpectedToFail(boolean expectedToFail) {
		this.expectedToFail = expectedToFail;
	}

	public boolean getExpectedToFail() {
		return expectedToFail;
	}

	public Integer getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(Integer screenshot) {
		this.screenshot = screenshot;
	}
	
	public Long getStepId() {
		return stepId;
	}

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public TestCaseStepEntity clone() throws CloneNotSupportedException {
		TestCaseStepEntity exeStepEntity=(TestCaseStepEntity)super.clone();
		List<TestCaseStepEntity> steps = new ArrayList<TestCaseStepEntity>();
		for(TestCaseStepEntity stepEntity : testSteps){
			steps.add(stepEntity.clone());
		}
		exeStepEntity.setTestSteps(steps);
		return exeStepEntity;
	}

	
}
