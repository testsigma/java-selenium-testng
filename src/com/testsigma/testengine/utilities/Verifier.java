package com.testsigma.testengine.utilities;

import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.exceptions.TestsigmaVerificationFailure;



public class Verifier {
	
	
	static public void verifyEquals(String expected, String actual) throws TestsigmaVerificationFailure{

		if( !expected.equals(actual)){
			throw new TestsigmaVerificationFailure(StringUtil.getMessage(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_STRINGS_EQUAL_FAILURE, new Object[]{expected, actual}));
		}
	}
	
	static public boolean verifyEquals(String expected, String actual, boolean ignoreCase){
		//TODO::
		if(ignoreCase){
			return expected.toUpperCase().equals(actual.toUpperCase());
		}else{
			return expected.equals(actual);
		}

	}
	static public void verifyEquals(String expected, String actual , String message) throws TestsigmaVerificationFailure{
		if(!expected.equals(actual)){
			throw new TestsigmaVerificationFailure(StringUtil.getMessage(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_STRINGS_EQUAL_FAILURE, new Object[]{expected, actual}), message);
		}
		
	}
	
	static public void verifyEquals(int expected, int actual , String message) throws TestsigmaVerificationFailure{
		if(expected!=actual){
			throw new TestsigmaVerificationFailure(StringUtil.getMessage(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_STRINGS_EQUAL_FAILURE, new Object[]{expected, actual}), message);
		}
		
	}
	
	static public void verifyNotEquals(String unexpected, String actual) throws TestsigmaVerificationFailure{
		if(unexpected.equals(actual)){
			throw new TestsigmaVerificationFailure(StringUtil.getMessage(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_STRINGS_NOT_EQUAL_FAILURE, new Object[]{unexpected, actual}));
		}
	}
	
	static public void verifyNotEquals(int unexpected, int actual, String message) throws TestsigmaVerificationFailure{
		if(unexpected==actual){
			throw new TestsigmaVerificationFailure(StringUtil.getMessage(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_STRINGS_NOT_EQUAL_FAILURE, new Object[]{unexpected, actual}), message);
		}
	}
	static public void verifyNotEquals(String unexpected, String actual, String message) throws TestsigmaVerificationFailure{
		if(unexpected.equals(actual)){
			throw new TestsigmaVerificationFailure(StringUtil.getMessage(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_STRINGS_NOT_EQUAL_FAILURE, new Object[]{unexpected, actual}), message);
		}
	}
	static public void verifyTrue(boolean condition) throws TestsigmaVerificationFailure{
		if(!condition){
			throw new TestsigmaVerificationFailure(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_VERIFYTRUE_BOOLEAN_FAILURE);
		}		
	}
	static public void verifyFalse(boolean condition) throws TestsigmaVerificationFailure{
		if(condition){
			throw new TestsigmaVerificationFailure(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_VERIFYFALSE_BOOLEAN_FAILURE);
		}		
	}
	static public void verifyTrue(boolean condition, String message) throws TestsigmaVerificationFailure{
		if(!condition){
			throw new TestsigmaVerificationFailure(message,MessageConstants.EXCEPTION_VERIFICATION_FAILURE_VERIFYFALSE_BOOLEAN_FAILURE);
		}		
	}
	static public void verifyFalse(boolean condition, String message) throws TestsigmaVerificationFailure{
		if(condition){
			throw new TestsigmaVerificationFailure(message,MessageConstants.EXCEPTION_VERIFICATION_FAILURE_VERIFYFALSE_BOOLEAN_FAILURE);
		}		
	}

	public static void verifyContains(String partialText, String text , String message) throws TestsigmaVerificationFailure{
		if(!text.contains(partialText)){
			throw new TestsigmaVerificationFailure(StringUtil.getMessage(MessageConstants.EXCEPTION_VERIFICATION_FAILURE_STRINGS_EQUAL_FAILURE, new Object[]{partialText, text}), message);
		}
		
	}
}
