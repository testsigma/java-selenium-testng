package com.testsigma.testengine.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

import com.google.gson.Gson;

public class ResultUtil {
	String extention=".json";
	String folder = "reports";
	public void saveResult(String fileName, Map<String, Object> result) {
		try{
			File fol = new File(folder);
			if(!fol.exists()){
				fol.mkdir();
			}
			File file = new File(fol.getAbsolutePath()+File.separator+fileName+extention);
			file.createNewFile();
			FileOutputStream os = new FileOutputStream(file);
			System.out.println(file.getAbsolutePath());
			os.write(new Gson().toJson(result).getBytes());
			os.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
}
