package com.testsigma.testengine.utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

public class StringUtil {
   private static String msgParamIdentifier ="\\?";
	public static boolean isEmpty(String str) {
		if (str == null || str.trim().length() == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static String checkNull(String inputString, String defaultString) {
		if (inputString == null || inputString.equals("null")
				|| inputString.trim().equals("")) {
			return defaultString;
		}
		return inputString;
	}

	public static String checkNull(int inputString, String defaultString) {
		if (inputString == 0) {
			return defaultString;
		}
		return inputString + "";
	}

	public static String stringArrayToString(String[] inputString,
			String appendString) {
		String outputString = null;
		if (inputString != null) {
			outputString = new String();
			for (int i = 0; i < inputString.length; i++) {
				outputString = outputString + inputString[i] + appendString;
			}
		}
		return outputString;
	}

	public static Long convertStringToTime(String minutes) {

		Pattern p = Pattern.compile("\\d{0,4}");
		int mins = 60;
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		if (minutes != null && !minutes.trim().isEmpty()) {
			Matcher m = p.matcher(minutes);
			if (m.matches()) {
				int time = Integer.parseInt(minutes);
				if (time > 0 && time < 1440) {
					cal.set(Calendar.HOUR_OF_DAY, (time / mins));
					cal.set(Calendar.MINUTE, (time % mins));
				}
			}
		}
		return cal.getTimeInMillis();
	}

	public static String convertStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		try {
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
		} catch (Exception e) {
			// Toast.makeText(this, "Stream Exception",
			// Toast.LENGTH_SHORT).show();
		}
		return total.toString();
	}

	public static boolean isNotEmpty(String str) {
		if (str == null || str.trim().length() == 0) {
			return false;
		}
		return true;
	}

	public static String getString(Object[] obj) {
		StringBuilder toReturn = new StringBuilder();
		int index = 1;
		if (obj != null && obj.length > 0) {
			for (Object o : obj) {
				if (index++ > 1) {
					toReturn.append(",");
				}
				toReturn.append(o);
			}
		}
		return toReturn.toString();
	}
	
	public static String getStringWithQuotes(Object[] obj) {
		StringBuilder toReturn = new StringBuilder();
		int index = 1;
		if (obj != null && obj.length > 0) {
			for (Object o : obj) {
				if (index++ > 1) {
					toReturn.append(",");
				}
				toReturn.append("\"").append(o).append("\"");
			}
		}
		return toReturn.toString();
	}
	
	public static boolean checkContainsDigitInString(String idStr)
	{
		if(idStr==null)return false;
		return idStr.matches(".*\\d.*");	
	}
	
	public static String[] getStringArray(Object[] objs) {
		String[] toReturn = new String[objs.length];
		for(int i=0;i<objs.length;i++)
		{
			toReturn[i]=objs[i].toString();
		}
		return toReturn;
	}
	
	public static String removeDuplicates(String str){
		return str.replaceAll("(\\b\\w+\\b),(?=.*\\b\\1\\b)", "");
	}
	
	public static String removeSpace(String st){
		if(!isEmpty(st)){
			return st.replaceAll("\\s+","");
		}
		return null;
	}
		
	public static String getMessage(String replaceableMsg, Object... replaceParameters){
		replaceableMsg = replaceableMsg+" ";
		if((replaceParameters==null)||(replaceParameters.length <1))
			return replaceableMsg;
		int paramSize = replaceParameters.length;
		for(int i=0;i< paramSize; i++){
			String replacebleMsg[] =replaceableMsg.split(msgParamIdentifier+(i+1));
			String tempStr = replacebleMsg[0];
			int replaceLength = replacebleMsg.length;
			for(int j =1; j<replaceLength;j++){	
				String repMsg = replacebleMsg[j];	
				String parm = (replaceParameters[i]!=null) ? replaceParameters[i].toString() : "";
				tempStr = tempStr + parm + repMsg;
			}
			replaceableMsg = tempStr;
		}
		return replaceableMsg;
	}
	
	public static String substring(String string, int start, int end){
		if(string.length()>=(start+end)){
			string = string.substring(start, end);
		
		}
		return string;
	}
	
	public static List<String> getListFromCsv(String str){
		List<String> items = Arrays.asList(str.split("\\s*,\\s*"));
		return items;
	}
	
	public static String[] getArrayFromCsv(String str){
		return str.split("\\s*,\\s*");
	}
	
	public static String getUTF8String(String str){
		try {
			return new String(str.getBytes("iso-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return str;
		}
	}
	
	public static boolean isJSONValid(String JSON_STRING) {
	      try {
	          new Gson().fromJson(JSON_STRING, Object.class);
	          return true;
	      } catch(com.google.gson.JsonSyntaxException ex) { 
	          return false;
	      }
	  }
	
}
