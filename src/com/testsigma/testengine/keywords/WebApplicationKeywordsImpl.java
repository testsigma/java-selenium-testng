/**
 * 
 */
package com.testsigma.testengine.keywords;

import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.drivers.WebDriverManager;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;




//TODO:Create a constructor with Exception and message as arguments in all exceptions(Refer TestEngineException) class and call super class constructor. 
//This change will allow you to log errors automatically.
//Use the above constructor in all exception cases. Move all hard-coded message  messages to constants file.

public class WebApplicationKeywordsImpl extends WebdriverKeywordsImpl implements WebdriverKeywords {
	
	static Logger logger = Logger.getLogger(WebApplicationKeywordsImpl.class);
	
	public WebApplicationKeywordsImpl(ExecutionType exeType, String executionID){
		super(executionID);
	}
	
	public WebApplicationKeywordsImpl(String executionID, TestCaseStepEntity testCaseStepEntity) throws  TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
		super.setDriver(WebDriverManager.getInstance().getDriver(executionID));
	}
}
