/**
 * 
 */
package com.testsigma.testengine.keywords;

import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;



/**
 * @author krn
 *
 */
public interface IOSKeywords {
	
	public void iOSOnlyKeyword(String testdata) throws TestEngineException;
	
	public TestCaseStepResult lockScreen() throws TestEngineException;
	
	public TestCaseStepResult scrollTo() throws TestEngineException;
	
	public TestCaseStepResult scrollToElement() throws TestEngineException;

	
	//public void 
	
	
	
	
}
