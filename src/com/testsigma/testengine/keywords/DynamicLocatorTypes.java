package com.testsigma.testengine.keywords;

public enum DynamicLocatorTypes {
	
	LINK_WITH_TEXT(1,"//a[.='$PARAMETER']","Link with text "),
	LINK_WITH_PARTIALTEXT(1,"//a[contains(text(),'$PARAMETER')]","Link with partial text "),
	LINK_WITH_INDEX(1,"(//a)[$PARAMETER]","Link with index "),
	LINK_WITH_TITLE(1,"//a[@title='$PARAMETER']","Link with title "),
	TYPE_ELEMENT_WITH_LABEL(1,"//label[.='$PARAMETER']/following::*[1]","Element with label "),
	TYPE_ELEMENT_WITH_TEXT(1,"//text()[.='$PARAMETER']/following::*[1]","Element with text "),
	TYPE_ELEMENT_WITH_PLACEHOLDER(1,"//*[@placeholder='$PARAMETER']","Element with placeholder "),
	TYPE_ELEMENT_WITH_TITLE(1,"//*[@title='$PARAMETER']","Element with title "),
	ELEMENT_WITH_TEXT(1,"//*[.='$PARAMETER']","Element with text "),
	ELEMENT_WITH_PARTIALTEXT(1,"//*[contains(text(),'$PARAMETER')]","Element with partial text "),
	ELEMENT_WITH_TITLE(1,"//*[@title='$PARAMETER']","Element with title "),
	TEXTBOX_WITH_LABEL(1,"//label[.='$PARAMETER']/following::input[@type='text'][1]","Textbox with label "),
	TEXTBOX_WITH_TEXT(1,"//text()[.='$PARAMETER']/following::input[@type='text'][1]","Textbox with text "),
	TEXTBOX_WITH_PLACEHOLDER(1,"//input[@type='text'][@placeholder='$PARAMETER']","Textbox with placeholder "),
	TEXTBOX_WITH_INDEX(1,"(//input[@type='text'])[$PARAMETER]","Textbox with index "),
	TEXTBOX_WITH_TITLE(1,"//input[@type='text'][@title='$PARAMETER']","Textbox with title "),
	PASSWORDTEXTBOX_WITH_LABEL(1,"//label[.='$PARAMETER']/following::input[@type='password'][1]","Password textbox with label "),
	PASSWORDTEXTBOX_WITH_TEXT(1,"//text()[.='$PARAMETER']/following::input[@type='password'][1]","Password textbox with text "),
	PASSWORDTEXTBOX_WITH_PLACEHOLDER(1,"//input[@type='password'][@placeholder='$PARAMETER']","Password textbox with placeholder "),
	PASSWORDTEXTBOX_WITH_INDEX(1,"(//input[@type='password'])[$PARAMETER]","Password textbox with index "),
	PASSWORDTEXTBOX_WITH_TITLE(1,"//input[@type='password'][@title='$PARAMETER']","Password textbox with title "),
	EMAILTEXTBOX_WITH_LABEL(1,"//label[.='$PARAMETER']/following::input[@type='email'][1]","Email textbox with label "),
	EMAILTEXTBOX_WITH_TEXT(1,"//text()[.='$PARAMETER']/following::input[@type='email'][1]","Email textbox with text "),
	EMAILTEXTBOX_WITH_PLACEHOLDER(1,"//input[@type='email'][@placeholder='$PARAMETER']","Email textbox with placeholder "),
	EMAILTEXTBOX_WITH_INDEX(1,"(//input[@type='email'])[$PARAMETER]","Email textbox with index "),
	EMAILTEXTBOX_WITH_TITLE(1,"//input[@type='email'][@title='$PARAMETER']","Email textbox with title "),
	DATETEXTBOX_WITH_LABEL(1,"//label[.='$PARAMETER']/following::input[@type='date'][1]","Date textbox with label "),
	DATETEXTBOX_WITH_TEXT(1,"//text()[.='$PARAMETER']/following::input[@type='date'][1]","Date textbox with text "),
	DATETEXTBOX_WITH_PLACEHOLDER(1,"//input[@type='date'][@placeholder='$PARAMETER']","Date textbox with placeholder "),
	DATETEXTBOX_WITH_INDEX(1,"(//input[@type='date'])[$PARAMETER]","Date textbox with index "),
	DATETEXTBOX_WITH_TITLE(1,"//input[@type='date'][@title='$PARAMETER']","Date textbox with title "),
	BUTTON_WITH_TEXT(1,"//button[.='$PARAMETER']","Button with text "),
	BUTTON_WITH_PARTIALTEXT(1,"//button[contains(text(),'$PARAMETER')]","Button with partial text "),
	BUTTON_WITH_TITLE(1,"//button[@title='$PARAMETER']","Button with title "),
	BUTTON_WITH_INDEX(1,"(//button)[$PARAMETER]","Button with index "),
	LISTBOX_WITH_LABEL(1,"//label[.='$PARAMETER']/following::select[1]","Listbox with label "),
	LISTBOX_WITH_TEXT(1,"//text()[.='$PARAMETER']/following::select[1]","Listbox with text "),
	LISTBOX_WITH_TITLE(1,"//select[@title='$PARAMETER']","Listbox with title "),
	TEXTAREA_WITH_LABEL(1,"//label[.='$PARAMETER']/following::textarea[1]","Textarea with label "),
	TEXTAREA_WITH_TEXT(1,"//text()[.='$PARAMETER']/following::textarea[1]","Textarea with text "),
	TEXTAREA_WITH_PLACEHOLDER(1,"//textarea[@placeholder='$PARAMETER']","Textarea with placeholder "),
	TEXTAREA_WITH_INDEX(1,"(//textarea)[$PARAMETER]","Textarea with index "),
	IMAGE_WITH_ALT(1,"//img[@alt='$PARAMETER']","Image with alt "),
	IMAGE_WITH_TITLE(1,"//img[@title='$PARAMETER']","Image with title "),
	IMAGE_WITH_TEXT(1,"//img[.='$PARAMETER']","Image with text "),
	CHECKBOX_WITH_LABEL(1,"//label[.='$PARAMETER']/preceding::input[@type='checkbox'][1]","Checkbox with label "),
	CHECKBOX_WITH_TEXT(1,"//text()[.='$PARAMETER']/preceding::input[@type='checkbox'][1]","Checkbox with text "),
	CHECKBOX_WITH_INDEX(1,"(//input[@type='checkbox'])[$PARAMETER]","Checkbox with index "),
	CHECKBOX_WITH_TITLE(1,"//input[@type='checkbox'][@title='$PARAMETER']","Checkbox with title "),
	CHECKBOX_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/preceding::input[@type='checkbox'][1]","Checkbox with partial label "),
	CHECKBOX_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/preceding::input[@type='checkbox'][1]","Checkbox with partial text "),
	RADIOBUTTON_WITH_LABEL(1,"//label[.='$PARAMETER']/preceding::input[@type='radio'][1]","Radio button with label "),
	RADIOBUTTON_WITH_TEXT(1,"//text()[.='$PARAMETER']/preceding::input[@type='radio'][1]","Radio button with text "),
	RADIOBUTTON_WITH_INDEX(1,"(//input[@type='radio'])[$PARAMETER]","Radio button with index "),
	RADIOBUTTON_WITH_TITLE(1,"//input[@type='radio'][@title='$PARAMETER']","Radio button with title "),
	RADIOBUTTON_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/preceding::input[@type='radio'][1]","Radio button with partial label "),
	RADIOBUTTON_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/preceding::input[@type='radio'][1]","Radio button with partial text "),
	TEXTBOX_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/following::input[@type='text'][1]","Textbox with partial label "),
	TEXTBOX_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/following::input[@type='text'][1]","Textbox with partial text "),
	PASSWORDTEXTBOX_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/following::input[@type='password'][1]","Password textbox with partial label "),
	PASSWORDTEXTBOX_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/following::input[@type='password'][1]","Password textbox with partial text "),
	EMAILTEXTBOX_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/following::input[@type='email'][1]","Email textbox with partial label "),
	EMAILTEXTBOX_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/following::input[@type='email'][1]","Email textbox with partial text "),
	DATETEXTBOX_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/following::input[@type='date'][1]","Date textbox with partial label "),
	DATETEXTBOX_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/following::input[@type='date'][1]","Date textbox with partial text "),
	TYPE_ELEMENT_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/following::*[1]","Element with partial label "),
	TYPE_ELEMENT_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/following::*[1]","Element with partial text "),
	TEXTAREA_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/following::textarea[1]","Textarea with partial label "),
	TEXTAREA_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/following::textarea[1]","Textarea with partial text "),
	LISTBOX_WITH_PARTIALLABEL(1,"//label//text()[contains(.,'$PARAMETER')]/following::select[1]","Listbox with partial label "),
	LISTBOX_WITH_PARTIALTEXT(1,"//text()[contains(.,'$PARAMETER')]/following::select[1]","Listbox with partial text ");
	
	private Integer type;
	private String xpathSyntax;
	private String displayText;

	
	DynamicLocatorTypes(Integer type, String xpathSyntax, String displayText){
			this.type = type;
			this.xpathSyntax = xpathSyntax;
			this.displayText = displayText;
		}
	public Integer getType(){
		return type;
	}
	
	public String getXpathSyntax(){
		return xpathSyntax;
	}
	
	public String getDisplayText(){
		return displayText;
	}

}
