/**
 * 
 */
package com.testsigma.testengine.keywords;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.testsigma.testengine.constants.ErrorCodes;
import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.LocatorType;
import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.drivers.WebDriverManager;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.AllWindowsNotClosedException;
import com.testsigma.testengine.exceptions.BrowserInstanceNotFoundException;
import com.testsigma.testengine.exceptions.NoCookiePresentException;
import com.testsigma.testengine.exceptions.TestsigmaAcceptAlertException;
import com.testsigma.testengine.exceptions.TestsigmaBrowserConnectionException;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestsigmaElementNotClickableException;
import com.testsigma.testengine.exceptions.TestsigmaElementNotVisibleException;
import com.testsigma.testengine.exceptions.TestsigmaElementTimeoutException;
import com.testsigma.testengine.exceptions.TestsigmaInvalidElementStateException;
import com.testsigma.testengine.exceptions.TestsigmaInvalidLocatorStrategyException;
import com.testsigma.testengine.exceptions.TestsigmaInvalidSelectorException;
import com.testsigma.testengine.exceptions.TestsigmaNoAlertPresentException;
import com.testsigma.testengine.exceptions.TestsigmaNoElementFoundException;
import com.testsigma.testengine.exceptions.TestsigmaNoSuchAttributeNameException;
import com.testsigma.testengine.exceptions.TestsigmaNoSuchFrameException;
import com.testsigma.testengine.exceptions.TestsigmaNotSelectElementException;
import com.testsigma.testengine.exceptions.TestsigmaOptionNotFoundException;
import com.testsigma.testengine.exceptions.TestsigmaStaleElementReferenceException;
import com.testsigma.testengine.exceptions.TestsigmaWebDriverException;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.providers.RuntimeDataProvider;
import com.testsigma.testengine.utilities.StringUtil;
import com.testsigma.testengine.utilities.Verifier;




//TODO:Create a constructor with Exception and message as arguments in all exceptions(Refer TestEngineException) class and call super class constructor. 
//This change will allow you to log errors automatically.
//Use the above constructor in all exception cases. Move all hard-coded message  messages to constants file.

public class WebdriverKeywordsImpl implements WebdriverKeywords {
	
	static Logger logger = Logger.getLogger(WebdriverKeywordsImpl.class);
	
	protected ExecutionType exeType;
	protected String executionID;
	protected WebDriver webdriver;
	protected TestCaseStepEntity  testCaseStepEntity;
	protected TestCaseStepResult  testCaseStepResult;
	
	
	public WebdriverKeywordsImpl(String executionID){
		this.exeType = exeType;
		this.executionID= executionID;
	}
	
	public WebdriverKeywordsImpl(String executionID, TestCaseStepEntity testCaseStepEntity) throws  TestsigmaDriverSessionNotFoundException{
		this.executionID = executionID;
		this.testCaseStepEntity= testCaseStepEntity;
		this.testCaseStepResult = new TestCaseStepResult(testCaseStepEntity.getId());
		testCaseStepResult.setTeststepKey(testCaseStepEntity.getTeststepKey());
		testCaseStepResult.setStartTime(new Timestamp(System.currentTimeMillis()));
	}
	
	public void setDriver(WebDriver webdriver){
		this.webdriver = webdriver;
	}
	
	public WebDriver getDriver(){
		return webdriver;
	}
	
	protected WebElement getElement(LocatorType locatortype, String locator) throws TestEngineException{
		
		By by = getBy(locatortype, locator);
		WebElement targetElement;
		try {
			targetElement = webdriver.findElement(by);
			if(targetElement.isDisplayed()){
				return targetElement;
			}else{
				logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is not visible, might be hidden");
				throw new TestsigmaElementNotVisibleException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_NOT_VISIBLE, new Object[]{locatortype, locator}));
			}
		}catch(InvalidSelectorException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is not valid, Invalid Selector Exception.");
			throw new TestsigmaInvalidSelectorException(ErrorCodes.INVALID_UI_IDENTIFIER, StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_INVALID_SELECTOR, new Object[]{locatortype, locator}));
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is not found, No Such Element Found.");
			throw new TestsigmaNoElementFoundException(ErrorCodes.INVALID_UI_IDENTIFIER, StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_NOT_FOUND, new Object[]{locatortype, locator}));
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :"+ e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,"Browser Connection Error occurred.  Details :"+ e.getMessage());			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception.Details :"+ e.getMessage());
			throw new TestsigmaWebDriverException("Testsigma WebDriver Exception. Details :"+ e.getMessage());
		}
	}

	private By getBy(LocatorType locatortype, String locator) throws TestEngineException {
		
		if(StringUtil.isEmpty(locator)){
			throw new TestsigmaNoElementFoundException(ErrorCodes.EMPTY_UI_IDENTIFIER, StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_NOT_FOUND, new Object[]{locatortype, locator}));
		}
		
		By by ;
		switch(locatortype){
		case class_name:
			by = By.className(locator);
			break;
		case csspath:
			by = By.cssSelector(locator);
			break;
		case id_value:
			by = By.id(locator);
			break;
		case link_text:
			by = By.linkText(locator);
			break;
		case name:
			by = By.name(locator);
			break;
		case partial_link_text:
			by = By.partialLinkText(locator);
			break;
		case tag_name:
			by = By.tagName(locator);
			break;
		case xpath:
			by = By.xpath(locator);
			break;
		default:
			logger.error("The locator type \""+locatortype+"\" corresponding to the element is not valid, Invalid locator strategy exception.");
			throw new TestsigmaInvalidLocatorStrategyException(StringUtil.getMessage(MessageConstants.EXCEPTION_LOCATOR_STRATEGY_INVALID, locatortype));
		}
		
		return by;
	}


	/* (non-Javadoc)
	 * @see com.testsigma.test-engine.keywords.WebUI#navigateTo()
	 */
	public void navigateTo(String testdata) throws TestEngineException{
		try {
			webdriver.navigate().to(testdata);
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}

	
	public TestCaseStepResult think() throws TestEngineException{
		try {
			long millis = Integer.parseInt(testCaseStepEntity.getTestDataValue())*1000;
			Thread.sleep(millis);
		}catch(InterruptedException e){
			logger.error("Interrupted Exception. Details :" + e.getMessage());
			throw new TestEngineException(StringUtil.getMessage(MessageConstants.EXCEPTION_INTERRUPTED_WAIT,  e.getMessage()));
		}
		return testCaseStepResult;

	}


	/* (non-Javadoc)
	 * @see com.testsigma.test-engine.keywords.WebUI#getTitle()
	 */
	public String getTitle()  throws TestEngineException{
		try {
			String currentTitle = webdriver.getTitle();
			logger.info("The Title of the Current Browser Window is retrieved.");
			return currentTitle;
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Unable to retrieve the title of the Current Browser Window. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}

	/* (non-Javadoc)
	 * @see com.testsigma.test-engine.keywords.WebUI#maximizeWindow()
	 */
	public TestCaseStepResult maximizeWindow()  throws TestEngineException{		
		try {
			webdriver.manage().window().maximize();
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
		return testCaseStepResult;

	}


	/* (non-Javadoc)
	 * @see com.testsigma.test-engine.keywords.WebUI#enter(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void enter(LocatorType locatortype, String locator, String testdata) throws TestEngineException {
		WebElement targetElement=getElement(locatortype, locator);
		try{
			targetElement.clear();
			targetElement.sendKeys(testdata);
		}catch(InvalidElementStateException e){
			logger.error("Element is either hidden or disabled exception");
			throw new TestsigmaInvalidElementStateException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_INVALID_STATE, new Object[]{locatortype, locator}));
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}

	}

	/* (non-Javadoc)
	 * @see com.testsigma.test-engine.keywords.WebUI#clear(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void clear(LocatorType locatortype, String locator) throws TestEngineException {
		WebElement targetElement=getElement(locatortype, locator);
		try{
			targetElement.clear();
		}catch(InvalidElementStateException e){
			throw new TestsigmaInvalidElementStateException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_INVALID_STATE,  new Object[]{locatortype, locator}));
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}

	}
	/* (non-Javadoc)
	 * @see com.testsigma.test-engine.keywords.WebUI#click(java.lang.String, java.lang.String)
	 */
	public void click(LocatorType locatortype, String locator) throws TestEngineException {
		WebElement targetElement=getElement(locatortype, locator);
		try{
			targetElement.click();
			
		}catch(StaleElementReferenceException e){
			throw new TestsigmaStaleElementReferenceException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_STALE, new Object[]{locatortype, locator}));
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	

	@Override
	public void check(LocatorType locatortype, String locator) 	throws TestEngineException {
		WebElement targetElement=getElement(locatortype, locator);
		if(!targetElement.isSelected()){
			try{
				targetElement.click();  
			}catch(StaleElementReferenceException e){
				throw new TestsigmaStaleElementReferenceException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_STALE, new Object[]{locatortype, locator}));
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}		
		}else{
			logger.warn("The target element state is already checked, hence no action performed to check.");
		}
	}

	@Override
	public void unCheck(LocatorType locatortype, String locator) throws TestEngineException {		
		WebElement targetElement=getElement(locatortype, locator);
		if(targetElement.isSelected()){
			try{
				targetElement.click(); //TODO:: log -  if target element's state is selected, then click to perform uncheck operation. 
			}catch(StaleElementReferenceException e){
				throw new TestsigmaStaleElementReferenceException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_STALE, new Object[]{locatortype, locator}));
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}
		}else{
			logger.warn("The target element state is already unchecked, hence no action performed to uncheck.");
		}
	}

	@Override
	public void rightClick(LocatorType locatortype, String locator) throws TestEngineException {
		
		Actions actions = new Actions(webdriver);
		WebElement targetElement=getElement(locatortype, locator);
		actions.contextClick(targetElement).build().perform();
	}

	@Override
	public void doubleClick(LocatorType locatortype, String locator) throws TestEngineException {
		Actions actions = new Actions(webdriver);
		WebElement targetElement=getElement(locatortype, locator);
		actions.doubleClick(targetElement).build().perform();
		
	}

	@Override
	public String getText(LocatorType locatortype, String locator)throws TestEngineException {
		
		WebElement targetElement = getElement(locatortype, locator);
		try{
			String getText = targetElement.getText().trim();
			logger.info("The text from the element corresponding to the locatortype \""+locatortype+"\" and locator \""+locator+"\" is retrieved.");
			return getText;
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Unable to retrieve the text from the element corresponding to the locatortype \""+locatortype+"\" and locator \""+locator+"\".");
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
		
	}
	
	
	private List<String> getAttributeNames(WebElement targetElement)throws TestEngineException{
		JavascriptExecutor jsrunner = (JavascriptExecutor)webdriver;
		String javascript="var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;";
		Map<String, Object> attributes = (Map<String, Object>) jsrunner.executeScript(javascript, targetElement);	
		List<String> attributeList = new ArrayList<String>(attributes.keySet());
		return attributeList;		
	}
	private boolean isAttributeAvailable(WebElement targetElement, String attribute) throws TestEngineException{
		String attributesInList = "";	
		try{
			for(String attributeFromList:getAttributeNames(targetElement)){
				if(attribute.equalsIgnoreCase(attributeFromList.trim())){
					return true;
				}
				attributesInList=attributeFromList+","+attributesInList;
			}								
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Error in checking \""+attribute+"\" is  available. Details :"+ "Exception in checking the available attributes , details :"+e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}		
		return false;
	}
	
	public String getAttribute(LocatorType locatortype, String locator, String attribute) throws TestEngineException{
		WebElement targetElement=getElement(locatortype, locator);
		try{
			String attributeValue= targetElement.getAttribute(attribute);
			if(attributeValue!=null){
				return attributeValue.trim();
			}else{
				logger.error("The attribute name \""+attribute+"\" does not exist for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
				throw new TestsigmaNoSuchAttributeNameException("The attribute name \""+attribute+"\" does not exist for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
			}
/*			if(attribute.equalsIgnoreCase("value")){
			}
			boolean attributeAvailable = isAttributeAvailable(targetElement, attribute);
			if(attributeAvailable){
				logger.info("The attribute \""+attribute+"\" is available for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
				String getAttribute = targetElement.getAttribute(attribute).trim();
				logger.info("The attribute Value corresponding to the attribute name \""+attribute+"\" is retrieved as \""+ getAttribute+ "\"");
				return getAttribute;
			}else{				
				logger.error("The attribute name \""+attribute+"\" does not exist for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
				throw new TestsigmaNoSuchAttributeNameException("The attribute name \""+attribute+"\" does not exist for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
			}*/
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Unable to retrieve the attribute Value corresponding to the attribute name \""+attribute+"\".");
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	public void verifyTextPresent(String testdata) throws TestEngineException{
		Verifier.verifyTrue(isTextPresent(testdata), "The expected text "+ "\"" +testdata +"\"" + " should be displayed");
		logger.info("The expected text \""+testdata+"\" is found to be Present on the current page.");
	}

	public void verifyTextAbsent(String testdata) throws TestEngineException{
		Verifier.verifyFalse(isTextPresent(testdata), "The expected text "+ "\"" +testdata +"\"" + " should not be displayed");
		logger.info("The expected text \""+testdata+"\" is Found to be absent on the current page.");
	}

	private boolean isTextPresent(String testdata) throws  TestEngineException{
		
		try{
			String bodytext = webdriver.findElement(By.tagName("body")).getText();
			return bodytext.contains(testdata);
		}catch(UnreachableBrowserException e){
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	public void verifyText(LocatorType locatortype, String locator,  String testdata) throws TestEngineException {
		String textFromElement = getText(locatortype, locator);
		Verifier.verifyEquals(testdata, textFromElement, "The text from the element \"" + textFromElement +"\" should be equal to expected text \"" +testdata+"\"");
		logger.info("The text from the element \"" +  getText(locatortype, locator) +"\" found to be same as expected text \"" +testdata+"\"");
	}
	
	public void verifyValue(LocatorType locatortype, String locator,  String testdata) throws TestEngineException {
		String valueFromElement = getAttribute(locatortype, locator, "value");
		Verifier.verifyEquals(testdata, valueFromElement, "The value attribute of the element \"" + valueFromElement +"\" should be equal to expected value \"" +testdata+"\"");
		logger.info("The attribute value of the element \"" + getAttribute(locatortype, locator,"value") +"\" found to be same as expected value \"" +testdata+"\"");
	}
	public void verifyTitle(String testdata) throws TestEngineException {
		String titleOfThePage = getTitle();
		Verifier.verifyEquals(testdata, titleOfThePage, "The page title \"" + titleOfThePage +"\" should be equal to expected title \"" +testdata+"\"");
		logger.info("The title \""+testdata+"\" is found to be present for the current page.");
	}
	
	public TestCaseStepResult verifyTitle() throws TestEngineException {
		String titleOfThePage = getTitle().trim();
		Verifier.verifyEquals(testCaseStepEntity.getTestDataValue(), titleOfThePage, "The page title \"" + titleOfThePage +"\" should be equal to expected title \"" +testCaseStepEntity.getTestDataValue()+"\"");
		return testCaseStepResult;
	}

	public void selectOptionByVisibleText(LocatorType locatortype, String locator,  String testdata) throws TestEngineException {
		if(isSelectBox(locatortype, locator)){
			Select selectElement = new Select(getElement(locatortype, locator));
			try{
				selectElement.selectByVisibleText(testdata);
			}catch(NoSuchElementException e){
				throw new TestsigmaOptionNotFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_SELECT_OPTION_NOT_FOUND, new Object[]{testdata, locatortype, locator}));
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}
		}else{
			throw new TestsigmaNotSelectElementException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_SELECT_WRONG_TAG, new Object[]{locatortype, locator}));
		}
		
	}
	
	public void selectOptionByIndex(LocatorType locatortype, String locator,  String testdata) throws TestEngineException {
		if(isSelectBox(locatortype, locator)){
			Select selectElement = new Select(getElement(locatortype, locator));
			try{
				selectElement.selectByIndex(Integer.parseInt(testdata));
			}catch(NoSuchElementException e){
				throw new TestsigmaOptionNotFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_SELECT_OPTION_NOT_FOUND, new Object[]{testdata, locatortype, locator}));
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}
		}else{
			throw new TestsigmaNotSelectElementException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_SELECT_WRONG_TAG, new Object[]{locatortype, locator}));
		}
		
	}

	public void selectOptionByValue(LocatorType locatortype, String locator,  String testdata) throws TestEngineException {
		if(isSelectBox(locatortype, locator)){
			Select selectElement = new Select(getElement(locatortype, locator));
			try{
				selectElement.selectByValue(testdata);
			}catch(NoSuchElementException e){
				throw new TestsigmaOptionNotFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_SELECT_OPTION_NOT_FOUND, new Object[]{testdata, locatortype, locator}));
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}
		}else{
			throw new TestsigmaNotSelectElementException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_SELECT_WRONG_TAG, new Object[]{locatortype, locator}));
		}
		
	}


	public String getTagName(LocatorType locatortype, String locator) throws TestEngineException{
		
		WebElement targetElement=getElement(locatortype, locator);
		String getTagName = targetElement.getTagName();
		logger.info("The TagName for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is retrieved.");
		return getTagName;
		
	}
	
	private String getClassName(LocatorType locatortype, String locator) throws TestEngineException{
		//TODO:: Constants should be moved to a single location
		String className = getAttribute(locatortype, locator, "class");
		logger.info("The ClassName for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is retrieved.");
		return className;
	}
	
	private boolean isSelectBox(LocatorType locatortype, String locator) throws TestEngineException{
		//TODO:: Constants should be moved to a single location
		return getTagName(locatortype, locator).equalsIgnoreCase("SELECT");
	}
	
	public void verifySelectedOption(LocatorType locatortype, String locator,  String testdata) throws TestEngineException{
		if(isSelectBox(locatortype, locator)){
			Select selectElement = new Select(getElement(locatortype, locator));
			try{
				String selectedOptionText = selectElement.getFirstSelectedOption().getText().trim();
				Verifier.verifyEquals(testdata, selectedOptionText,"The selected option in the select element \"" + selectedOptionText +"\" should be equal to expected value \"" +testdata+"\"");
				logger.info("The selected option in the select element \"" + selectedOptionText +"\" found to be same as expected select option \"" +testdata+"\"");
			}catch(NoSuchElementException e){
				logger.error("No option is selected for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
				throw new TestsigmaOptionNotFoundException("Option not found in the list exception");
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}
		}else{
			logger.error("The element is not of html tag - SELECT , Invalid type for verifyingSelectedOption");
			throw new TestsigmaNotSelectElementException("The element is not of html tag - SELECT , Invalid type for verifyingSelectedOption");
		}
		
	}
	
	public String getSelectedOption(LocatorType locatortype, String locator) throws TestEngineException {
		
		if(isSelectBox(locatortype, locator)){
			Select selectElement = new Select(getElement(locatortype, locator));
			try{
				String selectedOptionText = selectElement.getFirstSelectedOption().getText().trim();
				logger.info("The selected option in the select element \"" + selectedOptionText +"\"");
				return selectedOptionText;
			}catch(NoSuchElementException e){
				logger.error("No option is selected for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
				throw new TestsigmaOptionNotFoundException("Option not found in the list exception");
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}
		}else{
			logger.error("The element is not of html tag - SELECT , Invalid type for verifyingSelectedOption");
			throw new TestsigmaNotSelectElementException("The element is not of html tag - SELECT , Invalid type for verifyingSelectedOption");
		}
	}
	
/*	public void verifyAlertPresent() throws TestEngineException {
		Verifier.verifyTrue(isAlertPresent(), "The alert should be displayed");
	}*/

/*	public void verifyAlertAbsent() throws TestEngineException {
		Verifier.verifyFalse(isAlertPresent(), "The alert should not be displayed");
	}*/
	private boolean isAlertPresent() throws TestEngineException{
		WebDriverWait waiter = new WebDriverWait(webdriver, 30); // TODO :: Timeout constant need to be changed to use settings variables.
		try{
			Alert alert =waiter.until(ExpectedConditions.alertIsPresent());
			if(alert!=null) {
				return true;
			}else{
				return false;
				//throw new TestsigmaNoAlertPresentException("Alert is not found to be present in the current page");
			}

		}catch(TimeoutException e){
			throw new TestsigmaElementTimeoutException("Timeout exception , Details:"+ e.getMessage());
		}

	}
	
	private Alert getAlert() throws TestEngineException{
		WebDriverWait waiter = new WebDriverWait(webdriver, 30); // TODO :: Timeout constant need to be changed to use settings variables.
		try{
			Alert alert =waiter.until(ExpectedConditions.alertIsPresent());
			if(alert!=null) {
				logger.info("Alert is found to be present in the current page.");
				return alert;
			}else{
				logger.error("Alert is not found to be present in the current page.");
				throw new TestsigmaNoAlertPresentException("Alert is not found to be present in the current page");
			}

		}catch(TimeoutException e){
			logger.error("The allocated time to wait for alert to be present is timed out.");
			throw new TestsigmaElementTimeoutException("Timeout exception");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}

	}
	
	public void verifyAlertText(String testdata) throws TestEngineException{
		Alert alert = getAlert();
		Verifier.verifyEquals(testdata, alert.getText().trim(), "The alert should display \"" + testdata +"\"");
		logger.info("The alert is found to be present and the text in the alert \""+alert.getText()+"\" is same as expected text \""+testdata+"\"");
	}

/*	public void acceptAlert() throws TestEngineException{
		Alert alert = getAlert();
		try{
			alert.accept();
		}catch(WebDriverException e){
			throw new TestsigmaAcceptAlertException("Exception in clicking OK button in the alert");		
		}
	}*/
	
/*	public void dismissAlert() throws TestEngineException{
		Alert alert = getAlert();
		try{
			alert.dismiss();

		}catch(WebDriverException e){
			throw new TestsigmaAcceptAlertException("Exception in clicking Cancel button in the alert");		
		}
	}*/
	
	public void verifyCurrentURL(String testdata) throws TestEngineException{
		Verifier.verifyEquals(testdata, webdriver.getCurrentUrl(), "The URL of the current window should be \""+ testdata + "\"");
		logger.info("The current url \""+webdriver.getCurrentUrl()+"\" is found to be same as expected url \""+testdata+"\"");
	}

	@Override
	public void addCookie(Cookie cookie) throws TestEngineException {
		// TODO exception handling not done
		webdriver.manage().addCookie(cookie);
	}


	@Override
	public void deleteCookieByName(String testdata) throws TestEngineException {
		// TODO exception handling not done
		webdriver.manage().deleteCookieNamed(testdata);
		
	}

	public void deleteCookie(Cookie cookie) throws TestEngineException {
		// TODO exception handling not done
		webdriver.manage().deleteCookie(cookie);
		
	}
	@Override
	public TestCaseStepResult navigateTo() throws TestEngineException {
		try {
			webdriver.navigate().to(testCaseStepEntity.getTestDataValue());
			return this.testCaseStepResult;
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
		}
	}
	@Override
	public TestCaseStepResult navigateBack() throws TestEngineException {
		try {
			webdriver.navigate().back();
			return this.testCaseStepResult;
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
		}

	}
	@Override
	public TestCaseStepResult navigateForward() throws TestEngineException {
		try {
			webdriver.navigate().forward();
			return testCaseStepResult;
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
		}

	}
	@Override
	public TestCaseStepResult closeCurrentWindow() throws TestEngineException {
		try {
			webdriver.close();
			return testCaseStepResult;
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
		}

	}
	@Override
	public TestCaseStepResult closeAllWindows() throws TestEngineException {
		try {
			Set<String> windowHandles = webdriver.getWindowHandles();
			int counter=0;
			for(String windowHandle: windowHandles){
				webdriver.switchTo().window(windowHandle);
				webdriver.close();
				counter++;
			}
			if(counter !=windowHandles.size()){
				WebDriverManager.getInstance().resetSession(executionID);
				throw new AllWindowsNotClosedException(ErrorCodes.ALL_WINDOWS_NOT_CLOSED, "Only \""+counter + " windows out of \""+ windowHandles.size() + "\" were closed. Trying to reset the webdriver session for the execution ID...");
			}
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
		}
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult reloadCurrentPage() throws TestEngineException {		
		
		try {
				webdriver.navigate().refresh();
				return testCaseStepResult;
			}catch(WebDriverException e){
				throw new TestsigmaWebDriverException("Testsigma webdriver exception , details :"+ e.getMessage());
			}
	}
/*	@Override
	public void maximizeWindow(TestCaseStepEntity testCaseStepEntity) throws TestEngineException {		
		
		try {
			webdriver.manage().window().maximize();
			}catch(WebDriverException e){
				throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
			}
	}*/
	@Override
	public TestCaseStepResult resizeWindow()  throws TestEngineException {
		// TODO Auto-generated method stub
		return null;
		
	}
	@Override
	public TestCaseStepResult enter() throws TestEngineException {
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		try{
			targetElement.clear();
			targetElement.sendKeys(testCaseStepEntity.getTestDataValue());
			return testCaseStepResult;
		}catch(InvalidElementStateException e){
			throw new TestsigmaInvalidElementStateException("Element is either hidden or disabled exception");
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
		}		
	}
	@Override
	public TestCaseStepResult clear()	throws TestEngineException{
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		try{
			targetElement.clear();
			return testCaseStepResult;

			//TODO:: LOG - the contents are cleared in the textbox 
		}catch(InvalidElementStateException e){
			throw new TestsigmaInvalidElementStateException("Element is either hidden or disabled exception");
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
		}		
	}
	@Override
	public TestCaseStepResult click() 	throws TestEngineException {
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		try{
			targetElement.click(); 
			return testCaseStepResult;//TODO:: log - the element is clicked. 
		}catch(StaleElementReferenceException e){
			throw new TestsigmaStaleElementReferenceException("Stale element reference exception");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
		
			if(e.getMessage().contains(MessageConstants.EXCEPTION_ELEMENT_NOTCLICKABLE)){
				throw new TestsigmaElementNotClickableException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_NOTCLICKABLE_EXCEPTION_MESSAGE, testCaseStepEntity.getFieldName()));
			}else{
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}		
		}
	}
	@Override
	public TestCaseStepResult check() 	throws TestEngineException {		
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		if(!targetElement.isSelected()){
			try{
				targetElement.click(); //TODO:: log -  if target element's state is selected, then click to perform uncheck operation. 
				//return testCaseStepResult;//TODO:: log - the element is clicked. 
			}catch(StaleElementReferenceException e){
				throw new TestsigmaStaleElementReferenceException("Stale element reference exception");
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}		
		}else{
			logger.info("The element is already checked.");
			//return testCaseStepResult;//TODO:: log - the element is clicked. 

		}
		return testCaseStepResult;//TODO:: log - the element is clicked. 

	}
	@Override
	public TestCaseStepResult unCheck() throws TestEngineException {
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		if(targetElement.isSelected()){
			try{
				targetElement.click(); //TODO:: log -  if target element's state is selected, then click to perform uncheck operation. 
				//return testCaseStepResult;//TODO:: log - the element is clicked. 

			}catch(StaleElementReferenceException e){
				throw new TestsigmaStaleElementReferenceException("Stale element reference exception");
			}catch(UnreachableBrowserException e){
				logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
				throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
			}catch(WebDriverException e){
				logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
				throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}
		}else{
			////TODO:: log -  target element's state is already unchecked, so do nothing
			logger.info("The element is already unchecked.");
			//return testCaseStepResult;//TODO:: log - the element is clicked. 

		}		
		return testCaseStepResult;//TODO:: log - the element is clicked. 

	}
	@Override
	public TestCaseStepResult rightClick()throws TestEngineException {
		Actions actions = new Actions(webdriver);
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		actions.contextClick(targetElement).build().perform();
		return testCaseStepResult;		
	}
	@Override
	public TestCaseStepResult doubleClick()  throws TestEngineException {
		Actions actions = new Actions(webdriver);
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		actions.doubleClick(targetElement).build().perform();
		return testCaseStepResult;		
	}
	@Override
	public TestCaseStepResult verifyTextPresent() 	throws TestEngineException {
		Verifier.verifyTrue(isTextPresent(testCaseStepEntity.getTestDataValue()), "The expected text "+ "\"" +testCaseStepEntity.getTestDataValue() +"\"" + " should be displayed");
		return testCaseStepResult;
		
	}
	@Override
	public TestCaseStepResult verifyTextAbsent() 	throws TestEngineException {
		Verifier.verifyFalse(isTextPresent(testCaseStepEntity.getTestDataValue()), "The expected text "+ "\"" +testCaseStepEntity.getTestDataValue() +"\"" + " should not be displayed");
		return testCaseStepResult;
		
	}
	@Override
	public TestCaseStepResult verifyText() 	throws TestEngineException {

		String textFromElement = getText(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		Verifier.verifyEquals(testCaseStepEntity.getTestDataValue(), textFromElement, "The text from the element \"" + textFromElement +"\" should be equal to expected text \"" +testCaseStepEntity.getTestDataValue()+"\"");
		return testCaseStepResult;
			
	}
	@Override
	public TestCaseStepResult verifyValue() 	throws TestEngineException {
		String valueFromElement = getAttribute(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), "value");
		Verifier.verifyEquals(testCaseStepEntity.getTestDataValue(), valueFromElement, "The value attribute of the element \"" + valueFromElement +"\" should be equal to expected value \"" +testCaseStepEntity.getTestDataValue()+"\"");
		return testCaseStepResult;
		
	}
	@Override
	public TestCaseStepResult selectOptionByVisibleText() 	throws TestEngineException {
		if(isSelectBox(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition())){
			Select selectElement = new Select(getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()));
			try{
				selectElement.selectByVisibleText(testCaseStepEntity.getTestDataValue());
			}catch(NoSuchElementException e){
				throw new TestsigmaOptionNotFoundException("Option not found in the list exception");
			}catch(WebDriverException e){
				throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
			}
		}else{
			throw new TestsigmaNotSelectElementException("The element is not of html tag - SELECT , Invalid type for selectOptionByVisibleText");
		}
		return testCaseStepResult;		
	}
	@Override
	public TestCaseStepResult selectOptionByIndex() 		throws TestEngineException {
		if(isSelectBox(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition())){
			Select selectElement = new Select(getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()));
			try{
				selectElement.selectByIndex(Integer.parseInt(testCaseStepEntity.getTestDataValue()));
			}catch(NoSuchElementException e){
				throw new TestsigmaOptionNotFoundException("Option not found in the list exception");
			}catch(WebDriverException e){
				throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
			}
		}else{
			throw new TestsigmaNotSelectElementException("The element is not of html tag - SELECT , Invalid type for selectOptionByVisibleText");
		}
		return testCaseStepResult;		
	}
	@Override
	public TestCaseStepResult selectOptionByValue() throws TestEngineException {
		if(isSelectBox(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition())){
			Select selectElement = new Select(getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()));
			try{
				selectElement.selectByValue(testCaseStepEntity.getTestDataValue());
			}catch(NoSuchElementException e){
				throw new TestsigmaOptionNotFoundException("Option not found in the list exception");
			}catch(WebDriverException e){
				throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
			}
		}else{
			throw new TestsigmaNotSelectElementException("The element is not of html tag - SELECT , Invalid type for selectOptionByVisibleText");
		}
		return testCaseStepResult;
			
	}
	@Override
	public TestCaseStepResult verifySelectedOption()  throws TestEngineException {
		if(isSelectBox(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition())){
			Select selectElement = new Select(getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()));
			try{
				String selectedOptionText = selectElement.getFirstSelectedOption().getText();
				Verifier.verifyEquals(testCaseStepEntity.getTestDataValue(), selectedOptionText,"The selected option in the select element \"" + selectedOptionText +"\" should be equal to expected value \"" +testCaseStepEntity.getTestDataValue()+"\"");
			}catch(NoSuchElementException e){
				throw new TestsigmaOptionNotFoundException("Option not found in the list exception");
			}catch(WebDriverException e){
				throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
			}
		}else{
			throw new TestsigmaNotSelectElementException("The element is not of html tag - SELECT , Invalid type for selectOptionByVisibleText");
		}
		return testCaseStepResult;		
	}
	
	@Override
	public TestCaseStepResult verifySelectedValue()  throws TestEngineException {
		if(isSelectBox(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition())){
			Select selectElement = new Select(getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()));
			try{
				String selectedOptionValue = selectElement.getFirstSelectedOption().getAttribute("value");
				Verifier.verifyEquals(testCaseStepEntity.getTestDataValue(), selectedOptionValue,"The selected option value in the select element \"" + selectedOptionValue +"\" should be equal to expected value \"" +testCaseStepEntity.getTestDataValue()+"\"");
			}catch(NoSuchElementException e){
				throw new TestsigmaOptionNotFoundException("Option not found in the list exception");
			}catch(WebDriverException e){
				throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
			}
		}else{
			throw new TestsigmaNotSelectElementException("The element is not of html tag - SELECT , Invalid type for selectOptionByVisibleText");
		}
		return testCaseStepResult;		
	}
	@Override
	public TestCaseStepResult verifyAlertPresent() throws TestEngineException {
		Verifier.verifyTrue(isAlertPresent(), "The alert should be displayed");
		return testCaseStepResult;
		
	}
	@Override
	public TestCaseStepResult verifyAlertAbsent()  throws TestEngineException {
		Verifier.verifyFalse(isAlertPresent(), "The alert should not be displayed");
		return testCaseStepResult;
		
	}
	@Override
	public TestCaseStepResult verifyAlertText() 	throws TestEngineException {
		Alert alert = getAlert();
		Verifier.verifyEquals(testCaseStepEntity.getTestDataValue(), alert.getText(), "The alert should display \"" + testCaseStepEntity.getTestDataValue() +"\"");
		return testCaseStepResult;		
	}
	@Override
	public TestCaseStepResult acceptAlert() 	throws TestEngineException {
		Alert alert = getAlert();
		try{
			alert.accept();
		}catch(WebDriverException e){
			throw new TestsigmaAcceptAlertException("Exception in clicking OK button in the alert");		
		}
		return testCaseStepResult;		
	}
	
	@Override
	public TestCaseStepResult dismissAlert() throws TestEngineException {
		Alert alert = getAlert();
		try{
			alert.dismiss();
		}catch(WebDriverException e){
			throw new TestsigmaAcceptAlertException("Exception in clicking Cancel button in the alert");		
		}
		return testCaseStepResult;		
	}
	
	@Override
	public TestCaseStepResult addCookie()  throws TestEngineException {
		return testCaseStepResult;
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public TestCaseStepResult deleteCookieByName() throws TestEngineException {
		return testCaseStepResult;
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public TestCaseStepResult verifyCurrentURL() throws TestEngineException {
		Verifier.verifyEquals(testCaseStepEntity.getTestDataValue(), webdriver.getCurrentUrl(), "The URL of the current window should be \""+ testCaseStepEntity.getTestDataValue() + "\"");
		return testCaseStepResult;
		
	}
	@Override
	public TestCaseStepResult switchToWindowByTitle() throws TestEngineException {
		boolean windowFound = false;
		Set<String> windowHandles = webdriver.getWindowHandles();
		for(String windowHandle : windowHandles){
			try{
				webdriver.switchTo().window(windowHandle);
				String title = getTitle();
				if(title.equals(testCaseStepEntity.getTestDataValue())){
					windowFound= true;
					break;
				}
			}catch(NoSuchWindowException e){
				throw new BrowserInstanceNotFoundException("Switching to window with window handle \""+ windowHandle + "\" has failed. No such window found");
			}

		}
		if(!windowFound){
			throw new BrowserInstanceNotFoundException("Switching to window with title \""+ testCaseStepEntity.getTestDataValue() + "\" has failed. No such window found");
		}
		return testCaseStepResult;
		
	}
	
	@Override
	public TestCaseStepResult switchToWindowByTitleContaining() throws TestEngineException {
		boolean windowFound = false;
		Set<String> windowHandles = webdriver.getWindowHandles();
		for(String windowHandle : windowHandles){
			try{
				webdriver.switchTo().window(windowHandle);
				String title = getTitle();
				if(title.contains(testCaseStepEntity.getTestDataValue())){
					windowFound= true;
					break;
				}
			}catch(NoSuchWindowException e){
				throw new BrowserInstanceNotFoundException("Switching to window with window handle \""+ windowHandle + "\" has failed. No such window found");
			}

		}
		if(!windowFound){
			throw new BrowserInstanceNotFoundException("Switching to window with title containing \""+ testCaseStepEntity.getTestDataValue() + "\" has failed. No such window found");
		}
		return testCaseStepResult;
		
	}
	
	@Override
	public void switchToWindowByTitle(String testdata) throws TestEngineException {
		boolean windowFound = false;
		Set<String> windowHandles = webdriver.getWindowHandles();
		for(String windowHandle : windowHandles){
			try{
				webdriver.switchTo().window(windowHandle);
				String title = getTitle();
				if(title.equals(testdata)){
					windowFound= true;
					break;
				}
			}catch(NoSuchWindowException e){
				throw new BrowserInstanceNotFoundException("Switching to window with window handle \""+ windowHandle + "\" has failed. No such window found");
			}

		}
		if(!windowFound){
			throw new BrowserInstanceNotFoundException("Switching to window with title \""+ testdata + "\" has failed. No such window found");
		}
		
	}
	@Override
	public TestCaseStepResult switchToWindowByIndex() throws TestEngineException {
		boolean windowFound = false;
		Set<String> windowHandles = webdriver.getWindowHandles();
		int counter=0;
		for(String windowHandle : windowHandles){
			try{
				webdriver.switchTo().window(windowHandle);
				if(counter==Integer.parseInt(testCaseStepEntity.getTestDataValue())){
					windowFound= true;
					break;
				}
				counter++;
			}catch(NoSuchWindowException e){
				throw new BrowserInstanceNotFoundException("Switching to window with window handle \""+ windowHandle + "\" has failed. No such window found");
			}

		}
		if(!windowFound){
			throw new BrowserInstanceNotFoundException("Switching to window with index\""+ testCaseStepEntity.getTestDataValue() + "\" has failed. No such window found");
		}
		return testCaseStepResult;
		
	}
	@Override
	public void switchToWindowByIndex(String testdata) throws TestEngineException {
		boolean windowFound = false;
		Set<String> windowHandles = webdriver.getWindowHandles();
		int counter=0;
		for(String windowHandle : windowHandles){
			try{
				webdriver.switchTo().window(windowHandle);
				if(counter==Integer.parseInt(testdata)){
					windowFound= true;
					break;
				}
				counter++;
			}catch(NoSuchWindowException e){
				throw new BrowserInstanceNotFoundException("Switching to window with window handle \""+ windowHandle + "\" has failed. No such window found");
			}

		}
		if(!windowFound){
			throw new BrowserInstanceNotFoundException("Switching to window with index\""+ testdata + "\" has failed. No such window found");
		}
		
	}
	@Override
	public TestCaseStepResult switchToFrame() throws TestEngineException {
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		try {
			webdriver.switchTo().frame(targetElement);
		}catch(Exception e){
			throw new TestsigmaWebDriverException("Unable to switch to the frame identified by the locator details given ");
		}
		return testCaseStepResult;

	}
	@Override
	public void switchToFrame(LocatorType locatortype, String locator) throws TestEngineException {		
		WebElement targetElement=getElement(locatortype, locator);
		try {
			webdriver.switchTo().frame(targetElement);
		}catch(NoSuchFrameException e){
			logger.error("Unable to switch to the frame identified by the locator details given , locator type :\""+ locatortype 
					+"\"  , locator :\""+ locator + "\"");
			throw new TestsigmaWebDriverException("Unable to switch to the frame identified by the locator details given , locator type :\""+ locatortype 
					+"\"  , locator :\""+ locator + "\"");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	@Override
	public TestCaseStepResult switchToDefaultContents() throws TestEngineException {
		try{
			webdriver.switchTo().defaultContent();
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Switching to default contents has encountered an exception");
		}
		return testCaseStepResult;		
	}
/*	@Override
	public void switchToDefaultContents() throws TestEngineException {
		try{
			webdriver.switchTo().defaultContent();
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Switching to default contents has encountered an exception");
		}
	}*/
	public void verifyTagName(LocatorType locatortype, String locator,String testdata) throws TestEngineException {
		Verifier.verifyEquals(getTagName(locatortype, locator), testdata);
		logger.info("The TagName of the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is \""+getTagName(locatortype, locator)+"\" same as expected \""+testdata+"\"");
	}
	
	public TestCaseStepResult verifyTagName() throws  TestEngineException {
		Verifier.verifyEquals(getTagName(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;

	}
	public void verifyClassName(LocatorType locatortype, String locator, String testdata) throws  TestEngineException {
		Verifier.verifyEquals(getClassName(locatortype, locator), testdata);
		logger.info("The ClassName of the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is \""+getClassName(locatortype, locator)+"\" same as expected \""+testdata+"\"");
	}
	public TestCaseStepResult verifyClassName() throws  TestEngineException {
		Verifier.verifyEquals(getClassName(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
		
	}
	@Override
	public void verifyCSSValue(LocatorType locatortype, String locator, String propertyName,String testdata) throws  TestEngineException {
		Verifier.verifyEquals(testdata, getCSSValue(locatortype, locator, propertyName), "The element should have the CSS property name\""+propertyName+"\" with value as \""+testdata + "\"");
		logger.info("The CssValue for the property Name\""+propertyName+"\" of the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is \""+getCSSValue(locatortype, locator, propertyName)+"\" same as expected \""+testdata+"\"");
	}
	@Override
	public TestCaseStepResult verifyCSSValue() throws  TestEngineException {
		LocatorType locatortype = LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy());
		String locator = testCaseStepEntity.getFieldDefinition();
		String propertyName= testCaseStepEntity.getAttribute();
		String testdata = testCaseStepEntity.getTestDataValue();
		Verifier.verifyEquals(testdata, getCSSValue(locatortype, locator, propertyName), "The element should have the CSS property name\""+propertyName+"\" with value as \""+testdata + "\"");
		return testCaseStepResult;

	}
	@Override
	public String getCSSValue(LocatorType locatortype, String locator,String propertyName) throws  TestEngineException {
		WebElement targetElement=getElement(locatortype, locator);
		try{
			return targetElement.getCssValue(propertyName);
		}catch(WebDriverException e){
			throw new TestsigmaWebDriverException("Testsigma webdriver exception, details :"+ e.getMessage());
		}		
	}
	@Override
	public void mouseOver(LocatorType locatortype, String locator)
			throws  TestEngineException {
		Actions actions = new Actions(webdriver);
		WebElement targetElement=getElement(locatortype, locator);
		actions.moveToElement(targetElement).build().perform();		
	}
	@Override
	public TestCaseStepResult mouseOver() throws  TestEngineException {
		Actions actions = new Actions(webdriver);
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		actions.moveToElement(targetElement).build().perform();
		return testCaseStepResult;		
	
	}
	@Override 
	public void verifyEnabled(LocatorType locatortype, String locator) 	throws  TestEngineException {
		Verifier.verifyTrue(isEnabled(locatortype, locator), "The element should be enabled.");
		logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is found to be Enabled.");
	}
	private boolean isEnabled(LocatorType locatortype, String locator) throws  TestEngineException {
		WebElement targetElement = getElement(locatortype, locator);
		return targetElement.isEnabled();
	}
	@Override
	public TestCaseStepResult verifyEnabled() throws  TestEngineException {
		Verifier.verifyTrue(isEnabled(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition()), "The element should be enabled.");
		return testCaseStepResult;
		
	}
	@Override
	public void verifyDisabled(LocatorType locatortype, String locator) throws  TestEngineException {
		Verifier.verifyFalse(isEnabled(locatortype, locator), "The element should be disabled.");
		logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is found to be Disabled.");
	}
	@Override
	public TestCaseStepResult verifyDisabled() 	throws  TestEngineException {
		Verifier.verifyFalse(isEnabled(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition()), "The element should be disabled.");
		return testCaseStepResult;
		
	}
	@Override
	public void dragAndDrop(LocatorType locatortypeFrom, String locatorFrom, LocatorType locatortypeTo, String locatorTo) throws  TestEngineException {
		   Actions builder = new Actions(webdriver);
		   Action dragAndDrop = builder.clickAndHold(getElement(locatortypeFrom, locatorFrom))
		       .moveToElement(getElement(locatortypeTo, locatorTo))
		       .release(getElement(locatortypeTo, locatorTo))
		       .build();

		   dragAndDrop.perform();
		   //TODO::
	}
	public TestCaseStepResult dragAndDrop() throws  TestEngineException {
		return testCaseStepResult;
		   //TODO:: This keyword requires information about two locators, i.e. two elements. TestcaseStepEntity needs to hold this info
		// take a look at dragAndDrop(LocatorType locatortypeFrom, String locatorFrom, LocatorType locatortypeTo, String locatorTo)
/*		   Actions builder = new Actions(webdriver);
		   Action dragAndDrop = builder.clickAndHold(getElement(locatortypeFrom, locatorFrom))
		       .moveToElement(getElement(locatortypeTo, locatorTo))
		       .release(getElement(locatortypeTo, locatorTo))
		       .build();

		   dragAndDrop.perform();*/

	}
	@Override
	public void verifyChecked(LocatorType locatortype, String locator) 	throws TestEngineException {
		Verifier.verifyTrue(isChecked(locatortype ,locator),  "The checkbox state should be checked");
		logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is verified as checked.");
	}
	
	private boolean isChecked(LocatorType locatortype, String locator) throws TestEngineException{
		WebElement targetElement=getElement(locatortype, locator);
		return targetElement.isSelected();
	}
	
	private boolean isListSelected(LocatorType locatortype, String locator) throws TestEngineException{
		if(isSelectBox(locatortype, locator)){
			WebElement targetElement=getElement(locatortype, locator);
			Select list = new Select(targetElement);
			return list.getAllSelectedOptions().size()>0;
		}else{
			throw new TestsigmaNotSelectElementException("The element corresponding to the locator given is not a select element");
		}
	}
	
	private boolean isListMultiple(LocatorType locatortype, String locator) throws TestEngineException{
		if(isSelectBox(locatortype, locator)){
			Select targetElement= new Select(getElement(locatortype, locator));
			return targetElement.isMultiple();
		}else{
			throw new TestsigmaNotSelectElementException("The element corresponding to the locator given is not a select element");
		}
	}
	@Override
	public TestCaseStepResult verifyChecked() throws  TestEngineException {
		Verifier.verifyTrue(isChecked(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()) , testCaseStepEntity.getFieldDefinition()),  "The checkbox state should be checked");
		return testCaseStepResult;

	}
	@Override
	public void verifyUnchecked(LocatorType locatortype, String locator) throws  TestEngineException {
		Verifier.verifyFalse(isChecked(locatortype, locator),  "The checkbox state should be unchecked");
		logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is verified as Unchecked.");
	}
	@Override
	public TestCaseStepResult verifyUnchecked() 	throws  TestEngineException {
		Verifier.verifyFalse(isChecked(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()) , testCaseStepEntity.getFieldDefinition()),  "The checkbox state should be unchecked");
		return testCaseStepResult;

	}
	@Override
	public void verifyOptionsCount(LocatorType locatortype, String locator, String testdata) throws TestEngineException {
		Verifier.verifyEquals(Integer.parseInt(testdata), getOptionsCount(locatortype, locator), "The list should have the options count \""+ testdata + "\"");
		logger.info("The Options count of the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is \""+getOptionsCount(locatortype, locator)+"\" same as expected \""+testdata+"\"");
	}
	private int getOptionsCount(LocatorType locatortype, String locator) throws TestEngineException {
		 if(isSelectBox(locatortype, locator)){
			Select selectElement = new Select(getElement(locatortype, locator));
			int optionsCount = selectElement.getOptions().size();
			logger.info("The Options count of the element corresponding to the locator type \""+locatortype+" \" and locator \""+locator+"\" is \""+optionsCount+"\"");
			return optionsCount;
		 }else{
			 logger.error("The element is not of html tag - SELECT , Invalid type for getOptionsCount.");
			 throw new TestsigmaNotSelectElementException("The element corresponding to the locator given is not a select element");
		 }
	}
	@Override
	public TestCaseStepResult verifyOptionsCount() throws TestEngineException {
		Verifier.verifyEquals(Integer.parseInt(testCaseStepEntity.getTestDataValue()), getOptionsCount(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), "The list should have the options count \""+ testCaseStepEntity.getTestDataValue() + "\"");
		return testCaseStepResult;
		
	}
	@Override
	public void verifyListIsSelected(LocatorType locatortype, String locator) throws TestEngineException {
		Verifier.verifyTrue(isListSelected(locatortype, locator), "The list should have some option selected");
		logger.info("The List is Selected for the element corresponding to the locator type \""+locatortype+" \" and locator \""+locator+"\"");
		
	}
	@Override
	public TestCaseStepResult verifyListIsSelected() throws TestEngineException {
		Verifier.verifyTrue(isListSelected(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), "The list should have some option selected");
		return testCaseStepResult;

	}
	@Override
	public void verifyListIsMultiple(LocatorType locatortype, String locator) throws TestEngineException {
		Verifier.verifyTrue(isListMultiple(locatortype, locator), "The list should allow multiple selection of options");
		logger.info("The List contains multiple Selections for the element corresponding to the locator type \""+locatortype+" \" and locator \""+locator+"\"");
	}
	@Override
	public TestCaseStepResult verifyListIsMultiple() throws TestEngineException {
		Verifier.verifyTrue(isListMultiple(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), "The list should allow multiple selection of options");
		return testCaseStepResult;
		
	}
	@Override
	public void closeWindowUsingIndex(String testdata) throws TestEngineException {
		Set<String> windowHandles = webdriver.getWindowHandles();
		int counter=0;
		boolean windowClosed =false;
		for(String windowHandle : windowHandles){
			webdriver.switchTo().window(windowHandle);
			if(Integer.parseInt(testdata)==counter){
				logger.info("The window with given index \""+ testdata +"\" is found.");
				webdriver.close(); //TODO:: Exceptions are not handled here
				windowClosed= true;
				break;
				//TODO::log stuff
			}
			counter++;

		}
		if(!windowClosed){
			throw new BrowserInstanceNotFoundException("Window with index \"" + testdata + "\"  not found , that needs to be closed as expected" );
		}
		
	}
	@Override
	public TestCaseStepResult closeWindowUsingIndex() throws TestEngineException {
		closeWindowUsingIndex(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}
	@Override
	public void closeWindowWithTitle(String testdata) 	throws TestEngineException {
		Set<String> windowHandles = webdriver.getWindowHandles();
		boolean windowClosed =false;
		for(String windowHandle : windowHandles){
			webdriver.switchTo().window(windowHandle);
			if(webdriver.getTitle().equals(testdata)){
				logger.info("The window with given title \""+ testdata +"\" is found.");
				webdriver.close();
				windowClosed= true;
				break;
				//TODO::log stuff
			}

		}
		if(!windowClosed){
			throw new BrowserInstanceNotFoundException("Window with given title \"" + testdata + "\"  not found , that needs to be closed as expected" );
		}
		
	}
	@Override
	public TestCaseStepResult closeWindowWithTitle() throws TestEngineException {
		closeWindowWithTitle(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}
/*	@Override
	public void deleteAllCookies() throws TestEngineException {
		webdriver.manage().deleteAllCookies();
		
	}*/
	@Override
	public TestCaseStepResult deleteAllCookies() throws TestEngineException {
		webdriver.manage().deleteAllCookies();
		return testCaseStepResult;
		
	}

	@Override
	public TestCaseStepResult storeTitle() throws TestEngineException {
		RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, testCaseStepEntity.getTestDataValue(), webdriver.getTitle().trim());
		return testCaseStepResult;

	}

	@Override
	public void storeTitle(String testdata) throws TestEngineException {
		RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, testdata, webdriver.getTitle().trim());
	}

	@Override
	public TestCaseStepResult storeText() throws TestEngineException {
		storeText(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public void storeText(LocatorType locatortype , String locator, String testdata) throws TestEngineException {
		RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, testdata, getText(locatortype, locator));
	}
	
	private WebDriverWait getWaiter() throws TestEngineException{
		WebDriverWait waiter = new WebDriverWait(webdriver,30); //// ToDo: need to change the constants, in settings		
		return waiter;
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilElementIsVisible(java.lang.String,java.lang.String)
	 */
	public void waitUntilElementIsVisible(LocatorType locatortype, String locator) throws TestEngineException {		
		try{
			getWaiter().until(ExpectedConditions.visibilityOfElementLocated(getBy(locatortype, locator)));
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" found to be visible on the DOM of the page.");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found,No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element Not Found Exception.");
		}catch(StaleElementReferenceException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" no longer exists on the DOM of the Page.");
			throw new TestsigmaStaleElementReferenceException("Element no nonger exists on the page.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for an element to be visible corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is timed out.");
			throw new TestsigmaElementTimeoutException("Element time out is timed out. Details :"+ e.getMessage());
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilElementIsNotVisible(java.lang.String,java.lang.String)
	 */
	public void waitUntilElementIsNotVisible(LocatorType locatortype, String locator) throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.invisibilityOfElementLocated(getBy(locatortype, locator)));
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not visible on the DOM of the page.");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found,No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element not found Exception.");
		}catch(StaleElementReferenceException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" no longer exists on the DOM of the page.");
			throw new TestsigmaStaleElementReferenceException("Element no Longer Exists on the page.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for an element to be not visible corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is timed out.");
			throw new TestsigmaElementTimeoutException("Element lookup is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilElementIsClickable(java.lang.String,java.lang.String)
	 */
	public void waitUntilElementIsClickable(LocatorType locatortype, String locator)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.elementToBeClickable(getBy(locatortype, locator)));
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" found to be clickable on the DOM of the page.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for an element to be clickable corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is timed out.");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");
		}catch(StaleElementReferenceException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" no longer appears on the DOM of the page.");
			throw new TestsigmaStaleElementReferenceException("Element is no longer exists on the page.");
		}catch(ElementNotVisibleException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found to be visible on the DOM of the page.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilFrameIsLoadedAndSwitchToIt(java.lang.String,java.lang.String)
	 */
	public void waitUntilFrameIsAvailableAndSwitchToIt(LocatorType locatortype, String locator)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(getBy(locatortype, locator)));
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" found in frame and switched to it.");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not available in frame,No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element not found Exception.");
		}catch(NoSuchFrameException e){
			logger.error("The Frame containing element corresponding to the locatortype \""+locatortype+"\" and locator \""+locator+"\" not found to be present on the page. ");
			throw new TestsigmaNoSuchFrameException("Unable to switch to frame.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for a frame to be present containing element, corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is timed out.");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilPageTitleIs(java.lang.String)
	 */
	public void waitUntilPageTitleIs(String testdata)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.titleIs(testdata));
			logger.info("\""+testdata+"\", page title is found to be present on the current page.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for current page title \""+testdata+"\"  to be present on the page is timed out.");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");			
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	public void waitUntilPageTitleContains(String testdata)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.titleContains(testdata));
			logger.info("\""+testdata+"\", page title is found to be present on the current page.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for current page title \""+testdata+"\"  to be present on the page is timed out.");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");			
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilPageTitleIs(java.lang.String)
	 */
	public void waitUntilCurrentURLIs(String testdata)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.urlToBe(testdata));
			logger.info("Current page URL is found to be "+"\""+testdata+"\"");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for current page URL \""+testdata+"\"  to be present on the page is timed out.");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");			
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	public void waitUntilCurrentURLContains(String testdata)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.urlContains(testdata));
			logger.info("Current page URL is found to be match "+"\""+testdata+"\"");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for current page URL containing \""+testdata+"\"  to be present on the page is timed out.");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");			
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilElementHasValue(java.lang.String,java.lang.String,java.lang.String)
	 */
	public void waitUntilElementHasValue(LocatorType locatortype, String locator,String testdata)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.textToBePresentInElementValue(getBy(locatortype, locator), testdata));
			logger.info("The value \""+testdata+"\" is found to be present on the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found, No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element not found Exception.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for element has value \""+testdata+"\" to be present is timed out, corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" ");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");
		}catch(StaleElementReferenceException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" no longer appears on the DOM of the page.");
			throw new TestsigmaStaleElementReferenceException("Element is no longer exists");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
		
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilAlertIsPresent()
	 */
	public TestCaseStepResult waitUntilAlertIsPresent()throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.alertIsPresent());
			logger.info("The Alert is found to be present and switched to it.");
		}catch(NoAlertPresentException e){
			logger.error("The Alert is not found to be present on the page.");
			throw new TestsigmaNoAlertPresentException("Alert is not found to be present.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for an alert to be present on the page is timed out.");
			throw new TestsigmaElementTimeoutException("Alert time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
		return testCaseStepResult;
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilElementIsSelected(java.lang.String,java.lang.String)
	 */
	public void waitUntilElementIsSelected(LocatorType locatortype, String locator)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.elementToBeSelected(getBy(locatortype, locator)));
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" found to be selected.");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found, No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element not found Exception.");
		}catch(StaleElementReferenceException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" no longer appears on the DOM of the page.");
			throw new TestsigmaStaleElementReferenceException("Element is no longer exists");
		}catch(TimeoutException e){
			logger.error("The allocated time for an element is selected is timed out, corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilElementIsNotSelected(java.lang.String,java.lang.String)
	 */
	public void waitUntilElementIsNotSelected(LocatorType locatortype, String locator)throws TestEngineException{
		try{
			getWaiter().until((ExpectedConditions.not(ExpectedConditions.elementToBeSelected(getBy(locatortype, locator)))));
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found to be selected.");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found, No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element not found Exception.");
		}catch(StaleElementReferenceException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" no longer appears on the DOM of the page.");
			throw new TestsigmaStaleElementReferenceException("Element is no longer exists");
		}catch(TimeoutException e){
			logger.error("The allocated time for an element is selected is timed out, corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
			throw new TestsigmaElementTimeoutException("Element time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilAlertIsAbsent()
	 */
	public TestCaseStepResult waitUntilAlertIsAbsent()throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.not(ExpectedConditions.alertIsPresent()));
			logger.info("Alert is found to be absent on the page.");
		}catch(NoAlertPresentException e){
			logger.error("The Alert is not found to be present on the page.");
			throw new TestsigmaNoAlertPresentException("Alert is not found to be present.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for an alert to be absent on the page is timed out.");
			throw new TestsigmaElementTimeoutException("Alert time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
		return testCaseStepResult;
	}
		
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilElementIsEnabled(java.lang.String,java.lang.String)
	 */
	public void waitUntilElementIsEnabled(LocatorType locatortype, String locator)throws TestEngineException{
		try{
			getWaiter().until(CustomExpectedConditions.elementIsEnabled(getBy(locatortype, locator)));
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" found to be enabled on the page.");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found, No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element not found Exception.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for an element to be enabled on the Page is timed out.");
			throw new TestsigmaElementTimeoutException("The allocated time to wait for an element to be enabled on the Page is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilElementIsDisabled(java.lang.String,java.lang.String)
	 */
	public void waitUntilElementIsDisabled(LocatorType locatortype, String locator)throws TestEngineException{
		try{
			getWaiter().until(CustomExpectedConditions.elementIsDisabled(getBy(locatortype, locator)));
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is found to be disabled on the page.");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" not found, No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element not found Exception.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for an element to be disabled on the Page is timed out.");
			throw new TestsigmaElementTimeoutException("The allocated time to wait for an element to be disabled on the Page is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
		
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilTextIsDisplayed(java.lang.String,java.lang.String,java.lang.String)
	 */
	public void waitUntilTextIsDisplayed(String testdata)throws TestEngineException{
		try{
			getWaiter().until(CustomExpectedConditions.textToBePresent(testdata));
			logger.info("\""+testdata+"\" is displayed on the page.");
		}catch(StaleElementReferenceException e){
			logger.error("\""+testdata+"\" is no longer appears on the DOM of the page.");
			throw new TestsigmaStaleElementReferenceException("Text no longer exists.");
		}catch(TimeoutException e){
			logger.error("The allocated time to wait for text to be displayed on the page is timed out.");
			throw new TestsigmaElementTimeoutException("Time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilTextIsAbsent(java.lang.String,java.lang.String,java.lang.String)
	 */
	public void waitUntilTextIsAbsent(String testdata)throws TestEngineException{
		try{
			getWaiter().until(ExpectedConditions.not(CustomExpectedConditions.textToBePresent(testdata)));
			logger.info("\""+testdata+"\" is found to be absent on the page.");
		}catch(StaleElementReferenceException e){
			logger.error("\""+testdata+"\" is no longer appears on the DOM of the page.");
			throw new TestsigmaStaleElementReferenceException("Text no longer exists.");
		}catch(TimeoutException e){
			logger.error("The time allocated to wait until text \""+testdata+"\" is absent is timed out.");
			throw new TestsigmaElementTimeoutException("Time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilPageloadIsCompleted()
	 */
	public TestCaseStepResult waitUntilPageloadIsCompleted()throws TestEngineException{
		try{
			getWaiter().until(CustomExpectedConditions.waitForPageLoadUsingJS());
			logger.info("Page loading is completed on the browser.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}	
		return testCaseStepResult;
	}
		
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilNewWindowIsPresent(java.lang.String)
	 */
	public void waitUntilNewWindowIsPresent(String testdata)throws TestEngineException{
		try{
			getWaiter().until(CustomExpectedConditions.newWindowtobePresent(getDriver().getWindowHandles().size()));
			logger.info("New window is Present on the page.");
		}catch(NoSuchWindowException e){
			logger.error("New Window is not present on the page.");
			throw new BrowserInstanceNotFoundException("New window not found.");
		}catch(TimeoutException e){
			logger.error("The time allocated to wait for a new Window to be present on the page is timed out.");
			throw new TestsigmaElementTimeoutException("Time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilAllImagesAreLoaded(java.lang.String,java.lang.String)
	 */
	public void waitUntilAllElementsAreDisplayed(LocatorType locatortype, String locator)throws TestEngineException{
		try{			
			getWaiter().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(getBy(locatortype, locator)));
			logger.info("All elements are loaded and visible on the page.");
		}catch(NoSuchElementException e){
			logger.error("The elements corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is not found, No Such Element Found.");
			throw new TestsigmaNoElementFoundException("Element is not found");
		}catch(StaleElementReferenceException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" no longer appears on the DOM of the page.");
			throw new TestsigmaStaleElementReferenceException("Element is no longer exists");
		}catch(TimeoutException e){
			logger.error("The time allocated to wait for all images to be visible is timed out, corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
			throw new TestsigmaElementTimeoutException("Time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}	
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilAllElementsOfTagnameAreDisplayed(java.lang.String)
	 */
	public void waitUntilAllElementsOfTagnameAreDisplayed(String tagname)throws TestEngineException{
		try{			
			getWaiter().until(CustomExpectedConditions.allElementsOfTagnameAreDisplayed(tagname));
			logger.info("All elements of tagName \""+tagname+"\" are loaded and displayed on the page.");
		}catch(NoSuchElementException e){
			logger.error("The elements related to the tagname \""+tagname+"\" are not found, might me invalid tagname.");
			throw new TestsigmaNoElementFoundException("Element is not found");
		}catch(TimeoutException e){
			logger.error("The time allocated to wait for elements having tagname \""+tagname+"\" is timed out.");
			throw new TestsigmaElementTimeoutException("Time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#waitUntilAllElementsOfClassNameAreDisplayed(java.lang.String)
	 */
	public void waitUntilAllElementsOfClassNameAreDisplayed(String classname)throws TestEngineException{
		try{
			getWaiter().until(CustomExpectedConditions.allElementsOfClassNameAreDisplayed(classname));
			logger.info("All elements of classname \""+classname+"\" are loaded and displayed.");
		}catch(NoSuchElementException e){
			logger.error("The elements related to the classname \""+classname+"\" are not found, might me invalid classname.");
			throw new TestsigmaNoElementFoundException("Element is not found");
		}catch(TimeoutException e){
			logger.error("The time allocated to wait for elements having classname \""+classname+"\"  is timed out.");
			throw new TestsigmaElementTimeoutException("Time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}	
	public void waitUntilElementHasPropertyChanged(LocatorType locatortype, String locator, String attribute) throws TestEngineException{
				
		String oldValue = getAttribute(locatortype, locator, attribute);
		try{					
			getWaiter().until(CustomExpectedConditions.propertytobeChanged(getBy(locatortype, locator), attribute, oldValue));
			logger.info("Element having attribute \""+attribute+"\" has property changed.");
		}catch(TimeoutException e){
			logger.error("The time allocated to wait for element having attribute \""+attribute+"\" property to be changed is timed out.");
			throw new TestsigmaElementTimeoutException("Element Time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	public void waitUntilElementHasClassChanged(LocatorType locatortype, String locator) throws TestEngineException{
				
		String oldValue = getClassName(locatortype, locator);
		try{
			getWaiter().until(CustomExpectedConditions.classtobeChanged(getBy(locatortype, locator),  oldValue));
			logger.info("Element having classname \""+oldValue+"\" has classname changed.");
		}catch(TimeoutException e){
			logger.error("The time allocated to wait for element having classname \""+oldValue+"\" to be changed is timed out.");
			throw new TestsigmaElementTimeoutException("Time out is timed out.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	private boolean isNotEmpty(LocatorType locatortype, String locator,String attribute)throws TestEngineException{
		try{
			
			WebElement targetElement = getElement(locatortype, locator);
			return !targetElement.getAttribute(attribute).isEmpty();
			/*if(isAttributeAvailable(targetElement, attribute)){
				return targetElement.getAttribute(attribute) != null;
			}else{
				logger.error("\""+attribute+"\" is not available for the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\"");
				throw new TestsigmaNoSuchAttributeNameException("Attribute Name Not Exists.");
			}		*/	
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}			
	}
	
	
	public void verifyValueNotEmpty(LocatorType locatortype, String locator)throws TestEngineException{
		boolean isNotEmpty = isNotEmpty(locatortype, locator, "value");
		Verifier.verifyTrue(isNotEmpty,"The value of the element should not be empty");
		logger.info("The Attribute value of the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" with attribute name \""+"value"+"\" is not found to be Empty.");
	}
	
	public void verifyValueEmpty(LocatorType locatortype, String locator) throws TestEngineException{
		boolean isNotEmpty = isNotEmpty(locatortype, locator, "value");
		Verifier.verifyFalse(isNotEmpty, "The value of the element should be empty" );// + getAttribute(locatortype, locator, "value") +"\" should be empty");
		logger.info("The Attribute value of the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" with attribute name \""+"value"+"\" is found to be Empty.");
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#doesLinkOpensNewWindow(java.lang.String,java.lang.String)
	 */
	private boolean doesLinkOpenNewWindow(LocatorType locatortype, String locator) throws TestEngineException {
		try{
			Set<String> getWindowHandlesBeforeClick = webdriver.getWindowHandles();
			WebElement targetElement = getElement(locatortype, locator);
			targetElement.click();
			Set<String> getWindowHandlesAfterClick = webdriver.getWindowHandles();
			if(getWindowHandlesAfterClick.size()>getWindowHandlesBeforeClick.size()){
				logger.info("After Performing click on the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" , no. of windows are increased.");
				return true;
			}else{
				logger.info("After Performing click on the element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" , no. of windows are same.");
				return false;
			}
		}catch(StaleElementReferenceException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is no longer appears on the DOM of the page. ");
			throw new TestsigmaStaleElementReferenceException("Element no longer exists.");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" is not found, No Element Found Exception. ");
			throw new TestsigmaNoElementFoundException("Browser Connection Error occurred.  Details :" + e.getMessage());
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}				  
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#verifyLinkOpensNewWindow(java.lang.String,java.lang.String)
	 */
	public void verifyLinkOpensNewWindow(LocatorType locatortype, String locator) throws TestEngineException{
		try{
			Verifier.verifyTrue(doesLinkOpenNewWindow(locatortype, locator), "New window Should be displayed.");
			logger.info("The element corresponding to the locator type \""+locatortype+"\" and locator \""+locator+"\" Opens a new window");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}
	
	/* (non-Javadoc)
	 * @see com.graylocus.graytest.keywords.WebUI#switchToParentFrame()
	 */
	public TestCaseStepResult switchToParentFrame() throws TestEngineException{		
		try {
			webdriver.switchTo().parentFrame();
			logger.info("Switching to Parent Frame is successful.");
		}catch(NoSuchFrameException e){
			logger.error("Switching to Parent Frame is failed, No Such Frame Found.");
			throw new TestsigmaNoSuchFrameException("Unable to switch to the Parent Frame.");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}		
		return testCaseStepResult;
	}
	
	 /* @see com.graylocus.graytest.keywords.WebUI#switchToFrameUsingName(Java.lang.String)
	 */
	public void switchToFrameUsingName(String frameName) throws TestEngineException{
		try {
			webdriver.switchTo().frame(frameName);
			logger.info("Switching to Frame with name \""+frameName+"\" is successful.");
		}catch(NoSuchFrameException e){
			logger.error("Switching to frame with name \""+frameName+"\" has failed, No Such Frame Found.");
			throw new TestsigmaNoSuchFrameException("Unable to switch to the frame identified by the frame name details given , frameName :\""+ frameName+"\"");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}	
	}

	@Override
	public TestCaseStepResult verifyElementPresent() throws TestEngineException {
		Verifier.verifyTrue(isElementPresent(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), "The element "+testCaseStepEntity.getFieldName() +" should be displayed ");
		return testCaseStepResult;
	}

	private boolean isElementPresent(LocatorType locatorType, String locator) throws TestEngineException {
		By by = getBy(locatorType, locator);
		WebElement targetElement;
		try {
			targetElement = webdriver.findElement(by);
			if(targetElement.isDisplayed()){
				return true;
			}else{
				logger.error("The element corresponding to the locator type \""+locatorType+"\" and locator \""+locator+"\" is not visible, might be hidden");
				throw new TestsigmaElementNotVisibleException("Element is not visible, might be hidden");
			}
		}catch(InvalidSelectorException e){
			logger.error("The element corresponding to the locator type \""+locatorType+"\" and locator \""+locator+"\" is not valid, Invalid Selector Exception.");
			throw new TestsigmaInvalidSelectorException("Invalid selector exception");
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator type \""+locatorType+"\" and locator \""+locator+"\" is not found, No Such Element Found.");
			return false;
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
		
	}
	
	private boolean isElementAbsent(LocatorType locatorType, String locator) throws TestEngineException {
		By by = getBy(locatorType, locator);
		try{
			int elementCount = webdriver.findElements(by).size();
			if(elementCount==0){
				return true;
			}else{
				return false;
			}
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}


	}

	@Override
	public void verifyElementPresent(LocatorType locatortype, String locator) 	throws TestEngineException {
		Verifier.verifyTrue(isElementPresent(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), "The element "+testCaseStepEntity.getFieldName() +" should be displayed ");
	}

	@Override
	public TestCaseStepResult verifyElementAbsent() throws TestEngineException {
		Verifier.verifyTrue(isElementAbsent(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), "The element "+testCaseStepEntity.getFieldName() +" should not be displayed ");
		return testCaseStepResult;
	}

	@Override
	public void verifyElementAbsent(LocatorType locatortype, String locator) throws TestEngineException {
		Verifier.verifyTrue(isElementAbsent(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition()), "The element "+testCaseStepEntity.getFieldName() +" should not be displayed ");
		
	}

	public void deleteCookie(String cookieName) throws TestEngineException {
		try{
			if(webdriver.manage().getCookieNamed(cookieName)!=null){
				webdriver.manage().deleteCookieNamed(cookieName);
			}else{
				throw new NoCookiePresentException("A cookie with name \""+ cookieName+ "\" is not present in the current session");
			}
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}
	}

	@Override
	public TestCaseStepResult deleteCookie() throws TestEngineException {
		deleteCookie(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementIsVisible() throws TestEngineException {
		waitUntilElementIsVisible(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition() );
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementIsNotVisible()  throws TestEngineException {
		waitUntilElementIsNotVisible(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition() );
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementIsClickable() throws TestEngineException {
		waitUntilElementIsClickable(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition() );
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilFrameIsAvailableAndSwitchToIt() throws TestEngineException {
		waitUntilFrameIsAvailableAndSwitchToIt(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition() );
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilPageTitleIs() throws TestEngineException {
		waitUntilPageTitleIs(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilPageTitleContains() throws TestEngineException {
		waitUntilPageTitleContains(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilCurrentURLIs() throws TestEngineException {
		waitUntilCurrentURLIs(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilCurrentURLContains() throws TestEngineException {
		waitUntilCurrentURLContains(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementHasValue() throws TestEngineException {
		waitUntilElementHasValue(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementIsSelected() 	throws TestEngineException {
		waitUntilElementIsSelected(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition() );
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementIsNotSelected() throws TestEngineException {
		waitUntilElementIsNotSelected(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition() );
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementIsEnabled() 	throws TestEngineException {
		waitUntilElementIsEnabled(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition() );
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementIsDisabled() 	throws TestEngineException {
		waitUntilElementIsDisabled(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),testCaseStepEntity.getFieldDefinition() );
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilTextIsDisplayed() throws TestEngineException {
		waitUntilTextIsDisplayed(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilTextIsAbsent() throws TestEngineException {
		waitUntilTextIsAbsent(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilNewWindowIsPresent() throws TestEngineException {
		waitUntilNewWindowIsPresent(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilAllImagesAreLoaded() throws TestEngineException {
		waitUntilAllElementsAreDisplayed(LocatorType.xpath, "//img");
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilAllElementsOfTagnameAreDisplayed() throws TestEngineException {
		waitUntilAllElementsOfTagnameAreDisplayed(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilAllElementsOfClassNameAreDisplayed() throws TestEngineException {
		waitUntilAllElementsOfClassNameAreDisplayed(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementHasPropertyChanged() throws TestEngineException {
		waitUntilElementHasPropertyChanged(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilElementHasClassChanged() throws TestEngineException {
		waitUntilElementHasClassChanged(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult verifyValueNotEmpty() throws TestEngineException {
		verifyValueNotEmpty(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult verifyValueEmpty() throws TestEngineException {
		verifyValueEmpty(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult verifyLinkOpensNewWindow() throws TestEngineException {
		verifyLinkOpensNewWindow(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult switchToFrameUsingName() throws TestEngineException {
		switchToFrameUsingName(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public void storeValue(LocatorType locatortype, String locator, String variableName) throws TestEngineException {
		String capturedValue = getAttribute(locatortype, locator, "value");
		RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, variableName, capturedValue);
	}

	@Override
	public TestCaseStepResult storeValue() throws TestEngineException {
		storeValue(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public void storeElementsCount(LocatorType locatortype, String locator, String variableName) throws TestEngineException {
		int elementsCount = getElementsCount(locatortype, locator);
		RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, variableName, Integer.toString(elementsCount));
	}

	private int getElementsCount(LocatorType locatortype, String locator) throws TestEngineException {
		By by = getBy(locatortype, locator);
		return webdriver.findElements(by).size();
	}

	@Override
	public TestCaseStepResult storeElementsCount() throws TestEngineException {
		storeElementsCount(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public void storeSelectedOption(LocatorType locatortype, String locator, String variableName) throws TestEngineException {
		RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, variableName, getSelectedOption(locatortype, locator));

	}

	@Override
	public TestCaseStepResult storeSelectedOption() throws TestEngineException {
		storeSelectedOption(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public void storeOptionsCount(LocatorType locatortype, String locator, String variableName) throws TestEngineException {
		int optionsCount = getOptionsCount(locatortype, locator);
		RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, variableName, Integer.toString(optionsCount));
	}

	@Override
	public TestCaseStepResult storeOptionsCount() throws TestEngineException {
		storeOptionsCount(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public void storeCurrentURL(String variableName) throws TestEngineException {
		String currentURL =webdriver.getCurrentUrl();
		 RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, variableName,currentURL);
	}

	@Override
	public TestCaseStepResult storeCurrentURL() throws TestEngineException {
		storeCurrentURL(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	@Override
	public void storeAttribute(LocatorType locatortype, String locator, String attributeName, String variableName) throws TestEngineException {
		String attributeValue = getAttribute(locatortype, locator, attributeName);
		RuntimeDataProvider.getInstance().storeRuntimeVarible(executionID, variableName,attributeValue);
	}

	@Override
	public TestCaseStepResult storeAttribute() throws TestEngineException {
		storeAttribute(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getTestDataValue(),testCaseStepEntity.getAttribute());
		return testCaseStepResult;
	}

	@Override
	public void switchToFrameByIndex(int frameIndex) throws TestEngineException {
		try {
			webdriver.switchTo().frame(frameIndex);
			logger.info("Switching to Frame with index \""+frameIndex+"\" is successful.");
		}catch(NoSuchFrameException e){
			logger.error("Switching to frame with index \""+frameIndex+"\" has failed, No Such Frame Found.");
			throw new TestsigmaNoSuchFrameException("Unable to switch to the frame identified by the frame name details given , index :\""+ frameIndex+"\"");
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :" + e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,MessageConstants.EXCEPTION_BROWSER_UNREACHABLE);			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
		}			
	}

	@Override
	public TestCaseStepResult switchToFrameByIndex() throws TestEngineException {
		switchToFrameByIndex(Integer.parseInt(testCaseStepEntity.getTestDataValue()));
	return testCaseStepResult;
	}

	@Override
	public void verifyAttribute() throws TestEngineException {
		Verifier.verifyEquals( testCaseStepEntity.getTestDataValue(), getAttribute(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(),testCaseStepEntity.getAttribute()));
	}

	@Override
	// Click uiidentifier if visible
	public TestCaseStepResult clickIfPresent() throws TestEngineException {
		try{
			if(isElementPresent(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition())){
				logger.info("Element is present, hence performing a click operation...");
				click();
			}
		}catch(TestsigmaElementNotVisibleException e){
			logger.info("Element is absent, hence skipping the operation");

		}
/*		if(isElementPresent(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition())){
			logger.info("Element is present, hence performing a click operation...");
			click();
		}else{
			logger.info("Element is absent, hence skipping the operation");
		}*/
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult clickLinkWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult clickLinkWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult clickLinkWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult clickElementWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult clickButtonWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult clickButtonWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult clickButtonWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult clickElementWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyTitleContains() throws TestEngineException {
		String titleOfThePage = getTitle().trim();
		Verifier.verifyContains(testCaseStepEntity.getTestDataValue(), titleOfThePage, "The page title \"" + titleOfThePage +"\" should be matching to expected title \"" +testCaseStepEntity.getTestDataValue()+"\"");
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyCurrentURLContains() throws TestEngineException {
		Verifier.verifyContains(testCaseStepEntity.getTestDataValue(), webdriver.getCurrentUrl(), "The Matching URL of the current window should be \""+ testCaseStepEntity.getTestDataValue() + "\"");
		return testCaseStepResult;
		
	}
	@Override
	public TestCaseStepResult verifyLinkWithTextPresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyLinkWithPartialTextPresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyButtonWithPartialTextPresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyElementWithPartialTextPresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
        return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyButtonWithTextPresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyElementWithTextPresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyImageWithAltTextPresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.IMAGE_WITH_ALT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.IMAGE_WITH_ALT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyLinkWithTitlePresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyButtonWithTitlePresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyElementWithTitlePresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult verifyImageWithAltTitlePresent() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.IMAGE_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.IMAGE_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		verifyElementPresent();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoTextboxWithLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoTextboxWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoTextboxWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoTextboxWithPlaceholder() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoTextboxWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoPasswordTextboxWithLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoPasswordTextboxWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoPasswordTextboxWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoPasswordTextboxWithPlaceholder() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoPasswordTextboxWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoEmailTextboxWithLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoEmailTextboxWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoEmailTextboxWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoEmailTextboxWithPlaceholder() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoEmailTextboxWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult typeIntoDateTextboxWithLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoDateTextboxWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoDateTextboxWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoDateTextboxWithPlaceholder() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoDateTextboxWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult typeIntoTextAreaWithLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoTextAreaWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter(); 
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoTextAreaWithPlaceHolder() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoTextAreaWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult listBoxWithLabelByIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByIndex();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult listBoxWithLabelByText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByVisibleText();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult listBoxWithLabelByValue() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByValue();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult listBoxWithTextByIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByIndex();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult listBoxWithTextByVisbleText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByVisibleText();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult listBoxWithTextByValue() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByValue();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult listBoxWithTitleByIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByIndex();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult listBoxWithTitleByText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByVisibleText();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult listBoxWithTitleByValue() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByValue();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult updateParameterWithElementText() throws TestEngineException {	
		String textFromElement = getText(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());	
		Map<String, String> output = testCaseStepResult.getOutputData();
		output.put(testCaseStepEntity.getTestDataName(), textFromElement);
		testCaseStepResult.setOutputData(output);;
		
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult updateParameterWithElementAttribute() throws TestEngineException {
		String textFromElement = getAttribute(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition(), testCaseStepEntity.getAttribute());
		Map<String, String> output = testCaseStepResult.getOutputData();
		output.put(testCaseStepEntity.getTestDataName(), textFromElement);
		testCaseStepResult.setOutputData(output);;
		
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult checkCheckboxWithLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkCheckboxWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkCheckboxWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkCheckboxWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkCheckboxWithPartialLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkCheckboxWithPartialText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkRadioButtonWithLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkRadioButtonWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkRadioButtonWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkRadioButtonWithIndex() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_INDEX.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_INDEX.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkRadioButtonWithPartialLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult checkRadioButtonWithPartialText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilLinkWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilLinkWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilElementWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilButtonWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilButtonWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult waitUntilElementWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilLinkWithPartialTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilButtonWithPartialTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.BUTTON_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.BUTTON_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilElementWithPartialTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
        return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilImageWithAltTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.IMAGE_WITH_ALT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.IMAGE_WITH_ALT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilImageWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.IMAGE_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.IMAGE_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilTextboxWithLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilTextboxWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilTextboxWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilTextboxWithPlaceholderIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilPasswordTextboxWithLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilPasswordTextboxWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilPasswordTextboxWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilPasswordTextboxWithPlaceholderIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilEmailTextboxWithLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilEmailTextboxWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilEmailTextboxWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilEmailTextboxWithPlaceholderIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilDateTextboxWithLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilDateTextboxWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilDateTextboxWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilDateTextboxWithPlaceholderIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult waitUntilTextareaWithLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilTextareaWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilTextareaWithPlaceholderIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilListboxWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilListboxWithLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilListboxWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult waitUntilCheckboxWithLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilCheckboxWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilCheckboxWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilCheckboxWithPartialLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilCheckboxWithPartialTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.CHECKBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.CHECKBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilRadioButtonWithLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilRadioButtonWithTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilRadioButtonWithTitleIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilRadioButtonWithPartialLabelIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult waitUntilRadioButtonWithPartialTextIsVisible() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.RADIOBUTTON_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.RADIOBUTTON_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		waitUntilElementIsVisible();
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult typeIntoElementWithLabel() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TYPE_ELEMENT_WITH_LABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TYPE_ELEMENT_WITH_LABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoElementWithTitle() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TYPE_ELEMENT_WITH_TITLE.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TYPE_ELEMENT_WITH_TITLE.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoElementWithText() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TYPE_ELEMENT_WITH_TEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TYPE_ELEMENT_WITH_TEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}
	@Override
	public TestCaseStepResult typeIntoElementWithPlaceholder() throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TYPE_ELEMENT_WITH_PLACEHOLDER.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TYPE_ELEMENT_WITH_PLACEHOLDER.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoTextboxWithLabelContaining()
			throws TestEngineException {
		
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoTextboxWithTextContaining()
			throws TestEngineException {
		
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoPasswordTextboxWithLabelContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoPasswordTextboxWithTextContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.PASSWORDTEXTBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoEmailTextboxWithLabelContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoEmailTextboxWithTextContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.EMAILTEXTBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.EMAILTEXTBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoDateTextboxWithLabelContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoDateTextboxWithTextContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.DATETEXTBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.DATETEXTBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoElementWithLabelContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TYPE_ELEMENT_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TYPE_ELEMENT_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoElementWithTextContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TYPE_ELEMENT_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TYPE_ELEMENT_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoTextAreaWithLabelContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult typeIntoTextAreaWithTextContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.TEXTAREA_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.TEXTAREA_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		enter();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult clickLinkWithTextContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LINK_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LINK_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult clickElementWithTextContaining()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.ELEMENT_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getTestDataValue());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.ELEMENT_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getTestDataValue());
		click();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult listBoxWithPartialLabelByIndex()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByIndex();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult listBoxWithPartialLabelByText()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByVisibleText();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult listBoxWithPartialLabelByValue()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_PARTIALLABEL.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_PARTIALLABEL.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByValue();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult listBoxWithPartialTextByIndex()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByIndex();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult listBoxWithPartialTextByVisbleText()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByVisibleText();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult listBoxWithPartialTextByValue()
			throws TestEngineException {
		testCaseStepEntity.setLocatorStrategy(LocatorType.xpath.toString());
		String xpath = DynamicLocatorTypes.LISTBOX_WITH_PARTIALTEXT.getXpathSyntax().replace("$PARAMETER", testCaseStepEntity.getAttribute());
		testCaseStepEntity.setFieldDefinition(xpath);
		testCaseStepEntity.setFieldName(DynamicLocatorTypes.LISTBOX_WITH_PARTIALTEXT.getDisplayText()+":"+testCaseStepEntity.getAttribute());
		selectOptionByValue();
		return testCaseStepResult;
	}
		
	public void getTargetElement(String locatorType,By targetElementLocator,WebElement parentElement)throws TestEngineException {
		
		String fieldDefinition = testCaseStepEntity.getFieldDefinition();
		WebElement targetElement = null;
		try {
			targetElement = parentElement.findElement(targetElementLocator);
			if(targetElement.isDisplayed()){
				targetElement.click();
			}else{
				logger.error("The element corresponding to the locator \""+fieldDefinition+"\" is not visible, might be hidden");
				throw new TestsigmaElementNotVisibleException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_NOT_VISIBLE, new Object[]{locatorType,fieldDefinition}));
			}
		}catch(InvalidSelectorException e){
			logger.error("The element corresponding to the locator \""+fieldDefinition+"\" is not valid, Invalid Selector Exception.");
			throw new TestsigmaInvalidSelectorException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_INVALID_SELECTOR, new Object[]{locatorType,fieldDefinition}));
		}catch(NoSuchElementException e){
			logger.error("The element corresponding to the locator \""+fieldDefinition+"\" is not found, No Such Element Found.");
			throw new TestsigmaNoElementFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_NOT_FOUND, new Object[]{locatorType,fieldDefinition}));
		}catch(UnreachableBrowserException e){
			logger.error("Browser Connection Error occurred.  Details :"+ e.getMessage());
			throw new TestsigmaBrowserConnectionException(ErrorCodes.BROWSER_CLOSED,"Browser Connection Error occurred.  Details :"+ e.getMessage());			
		}catch(WebDriverException e){
			logger.error("Testsigma WebDriver Exception. Details :" + e.getMessage());
			
			if(e.getMessage().contains(MessageConstants.EXCEPTION_ELEMENT_NOTCLICKABLE)){
				throw new TestsigmaElementNotClickableException(StringUtil.getMessage(MessageConstants.EXCEPTION_ELEMENT_NOTCLICKABLE_EXCEPTION_MESSAGE, testCaseStepEntity.getFieldName()));
			}else{
			throw new TestsigmaWebDriverException(StringUtil.getMessage(MessageConstants.EXCEPTION_WEBDRIVER_UNKNOWN,  e.getMessage()));
			}		

		}

	}
	
	@Override
	public TestCaseStepResult clickElementRowWithText() throws TestEngineException {
		String testData = testCaseStepEntity.getTestDataValue();
		String fieldDefinition = testCaseStepEntity.getFieldDefinition();
		String parentElementLocator = "//*[.='"+testData+"']/ancestor::tr[1]";
		
		String locatorType = LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()).getDispName();
		if(locatorType.equals("xpath")){
			fieldDefinition = "."+fieldDefinition;
		}
		
		By targetElementLocator = getBy(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), fieldDefinition);
	
		WebElement parentElement = getElement(LocatorType.xpath, parentElementLocator);
		
		getTargetElement(locatorType, targetElementLocator, parentElement);
		return testCaseStepResult;
	}
	
	@Override
	public TestCaseStepResult clickElementRowWithTextContaining() throws TestEngineException {
		String testData = testCaseStepEntity.getTestDataValue();
		String fieldDefinition = testCaseStepEntity.getFieldDefinition();
		String parentElementLocator = "//*[contains(.,'"+testData+"')]/ancestor::tr[1]";
		
		String locatorType = LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()).getDispName();
		
		if(locatorType.equals("xpath")){
			fieldDefinition = "."+fieldDefinition;
		}
		By targetElementLocator = getBy(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), fieldDefinition);
		
		WebElement parentElement = getElement(LocatorType.xpath, parentElementLocator);
		
		getTargetElement(locatorType, targetElementLocator, parentElement);
		return testCaseStepResult;
	}
	
	@Override
	public void clickUntilTextVisible() throws TestEngineException {
		try {
			By by = getBy(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()),	testCaseStepEntity.getFieldDefinition());
			List<WebElement> list = webdriver.findElements(By.xpath("//*[contains(text(),'" + testCaseStepEntity.getTestDataValue() + "')]"));
			for (int i = 0;; i++) {
				if (list.size() == 0) {
					webdriver.findElement(by).click();
					if(!(webdriver.findElement(by).isEnabled())){
						logger.error("Given text "+testCaseStepEntity.getTestDataValue() +" is not visible");
						throw new TestsigmaNoElementFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_TEXT_NOT_FOUND, new Object[]{testCaseStepEntity.getTestDataValue()}));
					}
				} else {
					logger.debug("click until page is visible " + i);
					break;
				}
				list = webdriver.findElements(By.xpath("//*[contains(text(),'" + testCaseStepEntity.getTestDataValue() + "')]"));
			}
		} catch (NoSuchElementException e) {
			logger.error("Given text "+testCaseStepEntity.getTestDataValue() +" is not visible");
			throw new TestsigmaNoElementFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_TEXT_NOT_FOUND, new Object[]{testCaseStepEntity.getTestDataValue()}));

		}
	}
}
