package com.testsigma.testengine.keywords;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class CustomExpectedConditions {
	
	 private final static Logger logger = Logger.getLogger(CustomExpectedConditions.class.getName());
	  
	  public static ExpectedCondition<Boolean> elementIsEnabled(final By by) {
		    return new ExpectedCondition<Boolean>() {
		      public Boolean apply(WebDriver driver) {
		    	  if(driver.findElement(by).isEnabled()){
						return true;
		    	  }else{
		    		   return false;
		    	  }
		      }
		      public String toString() {
		          return "state of element located by " + by.toString();
		        }
		    };
		  }
	  
	  public static ExpectedCondition<Boolean> elementIsDisabled(final By by) {
		    return new ExpectedCondition<Boolean>() {
		      public Boolean apply(WebDriver driver) {
		    	  if(!(driver.findElement(by).isEnabled())){
						return true;
		    	  }else{
		    		   return false;
		    	  }
		      }
		      public String toString() {
		          return "state of element located by " + by.toString();
		        }
		    };
		  }
	  
	  public static ExpectedCondition<Boolean> textToBePresent(final String text) {
		    return new ExpectedCondition<Boolean>() {
		    	public Boolean apply(WebDriver driver) {
		           try {		              
		            	String elementText = driver.findElement(By.tagName("body")).getText();
		              return elementText.contains(text);
		            } catch (StaleElementReferenceException e) {
		              return false; // return null is changed to return false// TODO::
		            }
		          }
		      public String toString() {
		          return "state of text located by " +toString();
		        }
		    };
		  }
	
	  public static ExpectedCondition<Boolean> waitForPageLoadUsingJS(){
		  	return new ExpectedCondition<Boolean>() {
		  		public Boolean apply(WebDriver driver) {
		  			try {
		  				Object readyState= ((JavascriptExecutor) driver).executeScript("return document.readyState;");
		  				return readyState.toString().equalsIgnoreCase("complete");
		  			}catch(UnreachableBrowserException e){
			              return false; // return null is changed to return false// TODO::
		  			}
				}
		  };
	  }
	  
	  
	  
	  public static ExpectedCondition<Boolean> newWindowtobePresent(final int windowCount){
		  return new ExpectedCondition<Boolean>() {
			  public Boolean apply(WebDriver driver) {
				  try{
					 // int windowsSize = Integer.parseInt(windowCount);
					  Set<String> getWindowHandles = driver.getWindowHandles();
					  if(getWindowHandles.size()>windowCount){
						  return true;
					  }else{
						  return false;
					  }		
				  }catch(NoSuchWindowException e){
		              return false; // return null is changed to return false// TODO::
				  }
			  }			  
		  };
	  }
	  
	  //TODO:: This method needs to be rewritten to return true/false
	  public static ExpectedCondition<List<WebElement>> allElementsOfTagnameAreDisplayed(final String tagname){
		  return new ExpectedCondition<List<WebElement>>() {
			public List<WebElement> apply(WebDriver driver) {
				List<WebElement> allElementsOfTagname = driver.findElements(By.tagName(tagname));
				for(WebElement element : allElementsOfTagname){
					if(!element.isDisplayed()){
						return null;
					}
				}
				return allElementsOfTagname.size() > 0 ? allElementsOfTagname : null;				
			}			
		  };
	  }
	  
	  public static ExpectedCondition<List<WebElement>> allElementsOfClassNameAreDisplayed(final String classname){
		  return new ExpectedCondition<List<WebElement>>() {
			public List<WebElement> apply(WebDriver driver) {
				List<WebElement> allElementsOfClassName = driver.findElements(By.className(classname));
				for(WebElement element : allElementsOfClassName){
					if(!element.isDisplayed()){
						return null;
					}
				}
				return allElementsOfClassName.size() > 0 ? allElementsOfClassName : null;				
			}			
		  };
	  }
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  public static ExpectedCondition<Boolean> propertytobeChanged(final By by,final String attribute, final String oldValue) {
		  
				    return new ExpectedCondition<Boolean>() {	    	
			    	public Boolean apply(WebDriver driver) {
			           if((driver.findElement(by).getAttribute(attribute)).equals(oldValue)==false){
			        	   return true;
			           }else{
			        	   return false;
			           }
			          }
			    	public String toString() {
			    		return "state of text located by " +by.toString();
			        }
			    };
			  }
		  
		  public static ExpectedCondition<Boolean> classtobeChanged(final By by,  final String oldValue){
			  			  
			  return new ExpectedCondition<Boolean>() {
				  public Boolean apply(WebDriver driver) {				
					  if(driver.findElement(by).getAttribute("class").equals(oldValue) ==false){
						  return true;
					  }else{
						  return false;
					  }				  
				  }			  
			  };
		  }

}
