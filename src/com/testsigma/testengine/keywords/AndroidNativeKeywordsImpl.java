/**
 * 
 */
package com.testsigma.testengine.keywords;

import java.util.Map;

import org.apache.log4j.Logger;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.drivers.AndroidNativeDriverManager;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;





public class AndroidNativeKeywordsImpl extends AndroidKeywordsImpl {
	static Logger logger = Logger.getLogger(AndroidNativeKeywordsImpl.class);
	
	public AndroidNativeKeywordsImpl(ExecutionType exeType, String executionID){
		super(exeType, executionID);
	}
	
	public AndroidNativeKeywordsImpl( String executionID, TestCaseStepEntity testCaseStepEntity) throws TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
		super.setDriver(AndroidNativeDriverManager.getInstance().getDriver(executionID));
	}

	@Override
	public void androidOnlyKeyword(String testdata) throws TestEngineException {
		// TODO Auto-generated method stub
		
	}
	
	
}
