/**
 * 
 */
package com.testsigma.testengine.keywords;

import java.util.Map;

import org.apache.log4j.Logger;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.drivers.AndroidWebDriverManager;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;



/**
 * @author krn
 *
 */
public class AndroidWebKeywordsImpl extends AndroidKeywordsImpl  {
	static Logger logger = Logger.getLogger(AndroidWebKeywordsImpl.class);
	
	public AndroidWebKeywordsImpl(ExecutionType exeType, String executionID){
		super(exeType, executionID);
	}
	
	public AndroidWebKeywordsImpl(String executionID, TestCaseStepEntity testCaseStepEntity) throws TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
		super.setDriver(AndroidWebDriverManager.getInstance().getDriver(executionID));
	}

	
	
	
	
}
