package com.testsigma.testengine.keywords;

import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;



public interface MobileKeywords extends Keywords{
	
	public TestCaseStepResult launchApp()throws TestEngineException;
	
	public TestCaseStepResult closeApp()throws TestEngineException;
	
	public TestCaseStepResult installApp()throws TestEngineException;
	
	public TestCaseStepResult hideKeyboard() throws TestEngineException;
	
	public TestCaseStepResult setOrientationAsPortrait() throws TestEngineException;
	
	public TestCaseStepResult setOrientationAsLandscape() throws TestEngineException;
	
	public TestCaseStepResult verifyOrientationIsPortrait() throws TestEngineException;
	
	public TestCaseStepResult verifyOrientationIsLandscape() throws TestEngineException;
	
	public TestCaseStepResult removeApp() throws TestEngineException;
	
	public TestCaseStepResult verifyAppIsInstalled() throws TestEngineException;
	
	public TestCaseStepResult verifyScreenIsLocked() throws TestEngineException;
	
	public TestCaseStepResult tap() throws TestEngineException;
	
	public TestCaseStepResult swipe() throws TestEngineException;
	
	public TestCaseStepResult pinch() throws TestEngineException;
	
	public TestCaseStepResult zoom() throws TestEngineException;
	
	public TestCaseStepResult runAppInBackground() throws TestEngineException;
	
	public TestCaseStepResult resetApp() throws TestEngineException;	
	
}
