/**
 * 
 */
package com.testsigma.testengine.keywords;

import java.util.Map;

import org.apache.log4j.Logger;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.drivers.IOSNativeDriverManager;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;



/**
 * @author krnaidu
 *
 */
public class IOSNativeKeywordsImpl extends IOSKeywordsImpl {
	static Logger logger = Logger.getLogger(IOSNativeKeywordsImpl.class);
	
	public IOSNativeKeywordsImpl(ExecutionType exeType, String executionID){
		super(exeType, executionID);
	}
	
	public IOSNativeKeywordsImpl(String executionID, TestCaseStepEntity testCaseStepEntity) throws TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
		super.setDriver(IOSNativeDriverManager.getInstance().getDriver(executionID));
	}		
}
