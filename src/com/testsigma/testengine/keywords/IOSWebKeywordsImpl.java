/**
 * 
 */
package com.testsigma.testengine.keywords;

import java.util.Map;

import org.apache.log4j.Logger;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.drivers.IOSWebDriverManager;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;




/**
 * @author krn
 *
 */
public class IOSWebKeywordsImpl extends MobileKeywordsImpl  {
	static Logger logger = Logger.getLogger(IOSWebKeywordsImpl.class);
	
	public IOSWebKeywordsImpl(ExecutionType exeType, String executionID){
		super(exeType, executionID);
	}
	
	public IOSWebKeywordsImpl(String executionID, TestCaseStepEntity testCaseStepEntity) throws TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
		super.setDriver(IOSWebDriverManager.getInstance().getDriver(executionID));
	}
}
