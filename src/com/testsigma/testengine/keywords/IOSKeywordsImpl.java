/**
 * 
 */
package com.testsigma.testengine.keywords;

import io.appium.java_client.ios.IOSDriver;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.LocatorType;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestsigmaWebDriverException;
import com.testsigma.testengine.exceptions.TestEngineException;



/**
 * @author krn
 *
 */
public class IOSKeywordsImpl extends MobileKeywordsImpl implements IOSKeywords {
	static Logger logger = Logger.getLogger(IOSKeywordsImpl.class);
	
	public IOSKeywordsImpl(ExecutionType exeType, String executionID){
		super(exeType, executionID);
	}
	
	public IOSKeywordsImpl(ExecutionType exeType, String executionID, TestCaseStepEntity testCaseStepEntity, Map<String,String> envSettings) throws TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
	}
	public IOSKeywordsImpl(String executionID, TestCaseStepEntity testCaseStepEntity) throws TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
	}

	@Override
	public void iOSOnlyKeyword(String testdata) throws TestEngineException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public TestCaseStepResult lockScreen() throws TestEngineException {
		lockScreen(Integer.parseInt(testCaseStepEntity.getTestDataValue()));
		return testCaseStepResult;
	}
	
	private void lockScreen(int seconds) throws TestEngineException {
		Duration time = Duration.ofSeconds(seconds);
		((IOSDriver)getDriver()).lockDevice(time);
	}
	
	@Override
	public TestCaseStepResult scrollTo() throws TestEngineException {
		boolean bodytext = getDriver().getPageSource().contains(testCaseStepEntity.getTestDataValue());
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
		if(bodytext)
		{
			HashMap scrollObject = new HashMap<>();
	        scrollObject.put("predicateString", "value == '" + testCaseStepEntity.getTestDataValue() + "'");
	        js.executeScript("mobile: scroll", scrollObject);
	     }
		else
		{
			throw new TestsigmaWebDriverException("The text \""+testCaseStepEntity.getTestDataValue()+"\" is not present.");
		}
		return testCaseStepResult;
	}
	
	private void scrollTo(String text) throws TestEngineException{
		boolean bodytext = getDriver().getPageSource().contains(text);
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		if(bodytext)
		{
		    HashMap scrollObject = new HashMap<>();
	        scrollObject.put("predicateString", "value == '" + text + "'");
	        js.executeScript("mobile: scroll", scrollObject);
		}
		else
		{
			throw new TestsigmaWebDriverException("The text \""+text+"\" is not present.");
		}
	}	
	
	@Override
	public TestCaseStepResult scrollToElement() throws TestEngineException {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		RemoteWebElement scrollelement = (RemoteWebElement)targetElement;
		
		HashMap<String , String>scrollObject = new HashMap<String , String>();
	    scrollObject.put("element", scrollelement.getId());
	    scrollObject.put("toVisible", scrollelement.getId());
	    js.executeScript("mobile: scroll", scrollObject);
		return testCaseStepResult;
	}
	
	private void scrollToElement(LocatorType locatortype, String locator) throws TestEngineException{
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		WebElement targetElement=getElement(locatortype, locator);
		RemoteWebElement scrollelement = (RemoteWebElement)targetElement;
		
		HashMap<String , String>scrollObject = new HashMap<String , String>();
		scrollObject.put("element", scrollelement.getId());
        scrollObject.put("toVisible", scrollelement.getId());
        js.executeScript("mobile: scroll", scrollObject);
		
	}
	
}
