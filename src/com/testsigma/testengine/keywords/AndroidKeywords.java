/**
 * 
 */
package com.testsigma.testengine.keywords;

import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;


/**
 * @author krn
 *
 */
public interface AndroidKeywords {
	
	public void androidOnlyKeyword(String testdata) throws TestEngineException;
	

	public TestCaseStepResult lockScreen() throws TestEngineException;
	
	public TestCaseStepResult scrollTo() throws TestEngineException;
	
	public TestCaseStepResult scrollToExact() throws TestEngineException;
	
	public TestCaseStepResult launchAppWith(String details) throws TestEngineException;
	
	

	
	//public void 
	
	
	
	
}
