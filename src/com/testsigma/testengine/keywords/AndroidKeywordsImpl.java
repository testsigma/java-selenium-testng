/**
 * 
 */
package com.testsigma.testengine.keywords;

import io.appium.java_client.android.AndroidDriver;

import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.MessageConstants;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestsigmaOptionNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.utilities.StringUtil;


/**
 * @author krn
 *
 */
public class AndroidKeywordsImpl extends MobileKeywordsImpl implements AndroidKeywords {
	static Logger logger = Logger.getLogger(AndroidKeywordsImpl.class);
	
	public AndroidKeywordsImpl(ExecutionType exeType, String executionID){
		super(exeType, executionID);
	}
	
	public AndroidKeywordsImpl(String executionID, TestCaseStepEntity testCaseStepEntity) throws TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
	}

	@Override
	public void androidOnlyKeyword(String testdata) throws TestEngineException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public TestCaseStepResult lockScreen() throws TestEngineException {
		lockScreen(Integer.parseInt(testCaseStepEntity.getTestDataValue()));
		return testCaseStepResult;
	}
	
	private void lockScreen(int seconds) throws TestEngineException {
		((AndroidDriver)getDriver()).lockDevice();
	}
	
	@Override
	public TestCaseStepResult scrollTo() throws TestEngineException {
		try {
			String uiSelector = "new UiSelector().textContains(\"" + testCaseStepEntity.getTestDataValue() + "\")";
			 String command = "new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("+ uiSelector + ");";
			 ((AndroidDriver)getDriver()).findElementByAndroidUIAutomator(command);
			return testCaseStepResult;
		} catch (NoSuchElementException e) {
			throw new TestsigmaOptionNotFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_TEXT_NOT_FOUND, new Object[]{testCaseStepEntity.getTestDataValue()}));
		}
	}
	
	private void scrollTo(String text) throws TestEngineException{
		try {
			String uiSelector = "new UiSelector().textContains(\"" + text + "\")";
			 String command = "new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("+ uiSelector + ");";
			 ((AndroidDriver)getDriver()).findElementByAndroidUIAutomator(command);
		} catch (NoSuchElementException e) {
			throw new TestsigmaOptionNotFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_TEXT_NOT_FOUND, new Object[]{text}));
		}
	}	
	
	@Override
	public TestCaseStepResult scrollToExact() throws TestEngineException {
		try {
			String uiSelector = "new UiSelector().textMatches(\"" + testCaseStepEntity.getTestDataValue() + "\")";
			String command = "new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("+ uiSelector + ");";
			((AndroidDriver)getDriver()).findElementByAndroidUIAutomator(command);
			return testCaseStepResult;
		} catch (NoSuchElementException e) {
			throw new TestsigmaOptionNotFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_TEXT_NOT_FOUND, new Object[]{testCaseStepEntity.getTestDataValue()}));
			
		}
	}
	
	private void scrollToExact(String Exacttext) throws TestEngineException{
		try {
			String uiSelector = "new UiSelector().textMatches(\"" + Exacttext + "\")";
			String command = "new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView("+ uiSelector + ");";
			((AndroidDriver)getDriver()).findElementByAndroidUIAutomator(command);
		} catch (NoSuchElementException e) {
			throw new TestsigmaOptionNotFoundException(StringUtil.getMessage(MessageConstants.EXCEPTION_TEXT_NOT_FOUND, new Object[]{Exacttext}));
			
		}
	}

	@Override
	public TestCaseStepResult launchAppWith(String details) throws TestEngineException {
		if(StringUtil.isEmpty(details)){
			throw new TestEngineException("Invalid AppName/AppPackage provided.");
		}
		
		String[] args = details.split(",");
		if(args.length != 2){
			throw new TestEngineException("Invalid AppName/AppPackage provided.");
		}
		getDriver().getCapabilities();
		return testCaseStepResult;
	}
	
}
