/**
 * 
 */
package com.testsigma.testengine.keywords;
import java.util.Map;

import org.apache.log4j.Logger;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;



/**
 * @author krn
 *
 */
public class AndroidHybridKeywordsImpl extends AndroidKeywordsImpl {
	static Logger logger = Logger.getLogger(AndroidHybridKeywordsImpl.class);
	
	public AndroidHybridKeywordsImpl(ExecutionType exeType, String executionID){
		super(exeType, executionID);
	}
	
	public AndroidHybridKeywordsImpl(String executionID, TestCaseStepEntity testCaseStepEntity) throws TestsigmaDriverSessionNotFoundException{
		super(executionID, testCaseStepEntity);
	}

	public void navigateTo(String testdata) throws TestEngineException {
		// TODO Auto-generated method stub
		
	}
	
	
}
