package com.testsigma.testengine.keywords;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;

import com.testsigma.testengine.constants.ExecutionType;
import com.testsigma.testengine.constants.LocatorType;
import com.testsigma.testengine.entity.TestCaseStepEntity;
import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestsigmaDriverSessionNotFoundException;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.testengine.utilities.Verifier;



public class MobileKeywordsImpl extends WebdriverKeywordsImpl implements MobileKeywords {

	static Logger logger = Logger.getLogger(MobileKeywordsImpl.class);
	
	public MobileKeywordsImpl(ExecutionType exeType, String executionID){
		super(executionID);
	}
	
	public MobileKeywordsImpl( String executionID,TestCaseStepEntity testcaseStep)throws TestsigmaDriverSessionNotFoundException {
		// TODO Auto-generated constructor stub
		super(executionID, testcaseStep);
	}
	
	public AppiumDriver<WebElement> getDriver(){
		return (AppiumDriver<WebElement>) super.getDriver();
	}
	
	@Override
	public TestCaseStepResult launchApp() throws TestEngineException {
		getDriver().launchApp();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult closeApp() throws TestEngineException {
		getDriver().closeApp();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult hideKeyboard() throws TestEngineException {
		getDriver().hideKeyboard();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult setOrientationAsPortrait() throws TestEngineException {
		setOrientation(ScreenOrientation.PORTRAIT);
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult setOrientationAsLandscape() throws TestEngineException {
		setOrientation(ScreenOrientation.LANDSCAPE);
		return testCaseStepResult;
		
	}
	
	private void setOrientation(ScreenOrientation orientation) throws TestEngineException {
		getDriver().rotate(orientation);
	}

	@Override
	public TestCaseStepResult installApp() throws TestEngineException {
		installApp(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}
	
	private void installApp(String appPath) throws TestEngineException {
		getDriver().installApp(appPath);
	}

	@Override
	public TestCaseStepResult removeApp() throws TestEngineException {
		removeApp(testCaseStepEntity.getTestDataValue());
		return testCaseStepResult;
	}

	private void removeApp(String bundleID) throws TestEngineException {
		getDriver().removeApp(bundleID);
	}
	@Override
	public TestCaseStepResult verifyAppIsInstalled() throws TestEngineException {
		Verifier.verifyTrue(isAppInstalled(testCaseStepEntity.getTestDataValue()), "The app with bundle ID \""+testCaseStepEntity.getTestDataValue() +"\" should be installed");
		return testCaseStepResult;
	}
	
	private boolean isAppInstalled(String bundleID) throws TestEngineException {
		return getDriver().isAppInstalled(bundleID);
	}

	@Override
	public TestCaseStepResult verifyScreenIsLocked() throws TestEngineException {
		// TODO Auto-generated method stub
		return testCaseStepResult;
	}

	
	@Override
	public TestCaseStepResult tap() throws TestEngineException {
		//WebElement button=((WebElement)(getDriver().findElementById("submitBtn")));
		TouchAction action = new TouchAction(getDriver());
		
		WebElement targetElement=getElement(LocatorType.valueOf(testCaseStepEntity.getLocatorStrategy()), testCaseStepEntity.getFieldDefinition());
		action.tap(targetElement).perform();
		
		return testCaseStepResult;
	}


	private void tap(LocatorType locatortype, String locator) throws TestEngineException{
		WebElement targetElement = getElement(locatortype, locator);
		new TouchAction(getDriver()).tap(targetElement).perform();

	}
	
	@Override
	public TestCaseStepResult swipe() throws TestEngineException {
		// TODO Auto-generated method stub
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult pinch() throws TestEngineException {
		// TODO Auto-generated method stub
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult zoom() throws TestEngineException {
		// TODO Auto-generated method stub
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult runAppInBackground() throws TestEngineException {
		runAppInBackground(Integer.parseInt(testCaseStepEntity.getTestDataValue()));
		return testCaseStepResult;
	}

	private void runAppInBackground(int seconds) {
		//getDriver().runAppInBackground(seconds);
	}

	@Override
	public TestCaseStepResult resetApp() throws TestEngineException {
		getDriver().resetApp();
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult verifyOrientationIsPortrait() 	throws TestEngineException {
		 ScreenOrientation orientation = getDriver().getOrientation();
		 Verifier.verifyTrue(orientation.equals(ScreenOrientation.PORTRAIT),"The screen orientation should be Potrait.") ;
		return testCaseStepResult;
	}

	@Override
	public TestCaseStepResult verifyOrientationIsLandscape() 	throws TestEngineException {
		ScreenOrientation orientation = getDriver().getOrientation();
		Verifier.verifyTrue(orientation.equals(ScreenOrientation.LANDSCAPE),"The screen orientation should be Landscape.") ;
		return testCaseStepResult;
	}

	
}
