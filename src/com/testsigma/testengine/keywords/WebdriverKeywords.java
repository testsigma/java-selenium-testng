/**
 * 
 */
package com.testsigma.testengine.keywords;

import org.openqa.selenium.Cookie;

import com.testsigma.testengine.constants.LocatorType;
import com.testsigma.testengine.entity.TestCaseStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;




public interface WebdriverKeywords extends Keywords{
	

	public TestCaseStepResult navigateTo() throws TestEngineException;
		
	public TestCaseStepResult navigateBack() throws TestEngineException;
	
	public TestCaseStepResult navigateForward() throws TestEngineException;

	public TestCaseStepResult closeCurrentWindow() throws TestEngineException;
	
	public TestCaseStepResult closeAllWindows() throws TestEngineException;
	
	public TestCaseStepResult reloadCurrentPage() throws TestEngineException;
	
	public String getTitle() throws TestEngineException;
	
	public TestCaseStepResult maximizeWindow() throws TestEngineException;
	
	//public void maximizeWindow(TestCaseStepEntity testCaseStepEntity) throws TestEngineException;
	
	public TestCaseStepResult resizeWindow() throws TestEngineException;
	
	//public void resizeWindow(TestCaseStepEntity testCaseStepEntity) throws TestEngineException;
	
	public void enter(LocatorType locatortype, String locator,  String testdata) throws TestEngineException;
	
	public TestCaseStepResult enter() throws TestEngineException;

	public void clear(LocatorType locatortype, String locator) throws TestEngineException;

	public TestCaseStepResult clear() throws TestEngineException;

	public void click(LocatorType locatortype, String locator) throws TestEngineException;

	public TestCaseStepResult click() throws TestEngineException;
	
	public TestCaseStepResult clickIfPresent() throws TestEngineException;

	public void check(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult check() throws TestEngineException;
	
	public void unCheck(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult unCheck() throws TestEngineException;
	
	public String getAttribute(LocatorType locatortype, String locator, String attribute) throws TestEngineException;
	
	public String getText(LocatorType locatortype, String locator) throws TestEngineException;
	
	public void rightClick(LocatorType locatortype, String locator) throws TestEngineException;

	public TestCaseStepResult rightClick() throws TestEngineException;

	public void doubleClick(LocatorType locatortype, String locator) throws  TestEngineException;
	
	public TestCaseStepResult doubleClick() throws TestEngineException;

	public void verifyTextPresent(String testdata) throws TestEngineException;
	
	public void verifyAttribute() throws TestEngineException;

	
	public TestCaseStepResult verifyTextPresent() throws TestEngineException;

	public void verifyTextAbsent(String testdata) throws TestEngineException;
	
	public TestCaseStepResult verifyTextAbsent() throws TestEngineException;
	
	public void verifyTitle(String testdata) throws TestEngineException;

	public TestCaseStepResult verifyTitle() throws TestEngineException;

	public void verifyText(LocatorType locatortype, String locator,  String testdata) throws TestEngineException ;
	
	public TestCaseStepResult verifyText() throws TestEngineException ;
	
	public void verifyValue(LocatorType locatortype, String locator,  String testdata) throws TestEngineException ;
	
	public TestCaseStepResult verifyValue() throws TestEngineException ;

	public void selectOptionByVisibleText(LocatorType locatortype, String locator,  String testdata) throws TestEngineException ;
	
	public TestCaseStepResult selectOptionByVisibleText() throws TestEngineException ;
	
	public void selectOptionByIndex(LocatorType locatortype, String locator,  String testdata) throws TestEngineException ;
	
	public TestCaseStepResult selectOptionByIndex() throws TestEngineException ;

	public void selectOptionByValue(LocatorType locatortype, String locator,  String testdata) throws TestEngineException ;
	
	public TestCaseStepResult selectOptionByValue() throws TestEngineException ;

	public void verifySelectedOption(LocatorType locatortype, String locator,  String testdata) throws TestEngineException ;
	
	public TestCaseStepResult verifySelectedOption() throws TestEngineException ;
	
	//public void verifyAlertPresent() throws TestEngineException ;
	
	public TestCaseStepResult verifyAlertPresent() throws TestEngineException ;
	
	//public void verifyAlertAbsent() throws TestEngineException ;
	
	public TestCaseStepResult verifyAlertAbsent() throws TestEngineException ;
	
	public void verifyAlertText(String testdata) throws TestEngineException;
	
	public TestCaseStepResult verifyAlertText() throws TestEngineException;
	
	//public void acceptAlert() throws TestEngineException;
	
	public TestCaseStepResult acceptAlert() throws TestEngineException;
	
	//public void dismissAlert() throws TestEngineException;
	
	public TestCaseStepResult dismissAlert() throws TestEngineException;
	
	public void addCookie(Cookie cookie) throws TestEngineException;
	
	public TestCaseStepResult addCookie() throws TestEngineException;
	
	public void deleteCookieByName(String testdata) throws TestEngineException;
	
	public TestCaseStepResult deleteCookieByName() throws TestEngineException;
	
	public void verifyCurrentURL(String testdata) throws TestEngineException;
	
	public TestCaseStepResult verifyCurrentURL() throws TestEngineException;
	
	public TestCaseStepResult switchToWindowByTitle() throws TestEngineException;
	
	public void switchToWindowByTitle(String testdata) throws TestEngineException;
	
	public TestCaseStepResult switchToWindowByIndex() throws TestEngineException;
	
	public void switchToWindowByIndex(String testdata) throws TestEngineException;

	public TestCaseStepResult switchToFrame() throws TestEngineException;
	
	public void switchToFrame(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult switchToDefaultContents() throws TestEngineException;
	
	//public void switchToDefaultContents() throws TestEngineException;
	
	public void verifyTagName(LocatorType locatortype, String locator,  String testdata) throws TestEngineException;
	
	public TestCaseStepResult verifyTagName() throws TestEngineException;

	public void verifyClassName(LocatorType locatortype, String locator,  String testdata) throws TestEngineException;
	
	public TestCaseStepResult verifyClassName() throws TestEngineException;
	
	public void verifyCSSValue(LocatorType locatortype, String locator, String propertyName, String testdata) throws TestEngineException;
	
	public TestCaseStepResult verifyCSSValue() throws TestEngineException;
	
	public String getCSSValue(LocatorType locatortype, String locator,  String propertyName) throws TestEngineException;
	
	public void mouseOver(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult mouseOver() throws TestEngineException;
	
	public void verifyEnabled(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult verifyEnabled() throws TestEngineException;
	
	public void verifyDisabled(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult verifyDisabled() throws TestEngineException;
	
	public void dragAndDrop(LocatorType locatortypeFrom, String locatorFrom ,LocatorType locatortypeTo, String locatorTo ) throws TestEngineException;
	
	public TestCaseStepResult dragAndDrop() throws TestEngineException;
	
	public void verifyChecked(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult verifyChecked() throws TestEngineException;

	public void verifyUnchecked(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult verifyUnchecked() throws TestEngineException;
	
	public void verifyOptionsCount(LocatorType locatortype, String locator, String testdata) throws TestEngineException;
	
	public TestCaseStepResult verifyOptionsCount() throws TestEngineException;
	
	public void verifyListIsSelected(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult verifyListIsSelected() throws TestEngineException;

	public void verifyListIsMultiple(LocatorType locatortype, String locator) throws TestEngineException ;
	
	public TestCaseStepResult verifyListIsMultiple() throws TestEngineException ;
	
	public void closeWindowUsingIndex(String testdata) throws TestEngineException ;

	public TestCaseStepResult closeWindowUsingIndex() throws TestEngineException ;
	
	public void closeWindowWithTitle(String testdata) throws TestEngineException ;
	
	public TestCaseStepResult closeWindowWithTitle() throws TestEngineException ;
	
	//public void deleteAllCookies() throws TestEngineException;
	
	public TestCaseStepResult deleteAllCookies() throws TestEngineException;
	
	public TestCaseStepResult storeTitle() throws TestEngineException;
	
	public void storeTitle(String testdata) throws TestEngineException;
	
	public TestCaseStepResult storeText() throws TestEngineException;
	
	public void storeText(LocatorType locatortype , String locator, String testdata) throws TestEngineException;
	
	public TestCaseStepResult think() throws TestEngineException, InterruptedException;	
	
	public String getTagName(LocatorType locatortype, String locator) throws TestEngineException;
	
	public void deleteCookie(String cookieName) throws TestEngineException;
	
	public TestCaseStepResult deleteCookie() throws TestEngineException;
	
	public void waitUntilElementIsVisible(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementIsVisible() throws TestEngineException;
	
	public void waitUntilElementIsNotVisible(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementIsNotVisible() throws TestEngineException;
	
	public void waitUntilElementIsClickable(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementIsClickable() throws TestEngineException;
	
	public void waitUntilFrameIsAvailableAndSwitchToIt(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult waitUntilFrameIsAvailableAndSwitchToIt() throws TestEngineException;
	
	public void waitUntilPageTitleIs(String testdata)throws TestEngineException;
	
	public TestCaseStepResult waitUntilPageTitleIs() throws TestEngineException;
	
	public void waitUntilElementHasValue(LocatorType locatortype, String locator,String testdata)throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementHasValue() throws TestEngineException;
	
	public TestCaseStepResult waitUntilAlertIsPresent()throws TestEngineException;
	
	public void waitUntilElementIsSelected(LocatorType locatortype, String locator)throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementIsSelected()throws TestEngineException;

	public void waitUntilElementIsNotSelected(LocatorType locatortype, String locator)throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementIsNotSelected()throws TestEngineException;

	public TestCaseStepResult waitUntilAlertIsAbsent()throws TestEngineException;
	
	public void waitUntilElementIsEnabled(LocatorType locatortype, String locator)throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementIsEnabled()throws TestEngineException;

	public void waitUntilElementIsDisabled(LocatorType locatortype, String locator)throws TestEngineException;

	public TestCaseStepResult waitUntilElementIsDisabled()throws TestEngineException;

	public void waitUntilTextIsDisplayed(String testdata)throws TestEngineException;
	
	public TestCaseStepResult waitUntilTextIsDisplayed()throws TestEngineException;
	
	public void waitUntilTextIsAbsent(String testdata)throws TestEngineException;
	
	public TestCaseStepResult waitUntilTextIsAbsent()throws TestEngineException;

	public TestCaseStepResult waitUntilPageloadIsCompleted()throws TestEngineException;
	
	public void waitUntilNewWindowIsPresent(String testdata)throws TestEngineException;
	
	public TestCaseStepResult waitUntilNewWindowIsPresent()throws TestEngineException;
	
	public void waitUntilAllElementsAreDisplayed(LocatorType locatortype, String locator)throws TestEngineException;
	
	public TestCaseStepResult waitUntilAllImagesAreLoaded()throws TestEngineException;

	public void waitUntilAllElementsOfTagnameAreDisplayed(String tagname)throws TestEngineException;
	
	public TestCaseStepResult waitUntilAllElementsOfTagnameAreDisplayed()throws TestEngineException;
	
	public void waitUntilAllElementsOfClassNameAreDisplayed(String classname)throws TestEngineException;

	public TestCaseStepResult waitUntilAllElementsOfClassNameAreDisplayed()throws TestEngineException;
	
	public void waitUntilElementHasPropertyChanged(LocatorType locatortype, String locator, String attribute) throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementHasPropertyChanged()throws TestEngineException;

	public void waitUntilElementHasClassChanged(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult waitUntilElementHasClassChanged()throws TestEngineException;

	public void verifyValueNotEmpty(LocatorType locatortype, String locator)throws TestEngineException;
	
	public TestCaseStepResult verifyValueNotEmpty()throws TestEngineException;

	public void verifyValueEmpty(LocatorType locatortype, String locator) throws TestEngineException;

	public TestCaseStepResult verifyValueEmpty()throws TestEngineException;

	public void verifyLinkOpensNewWindow(LocatorType locatortype, String locator) throws TestEngineException,Exception;
	
	public TestCaseStepResult verifyLinkOpensNewWindow()throws TestEngineException;

	public TestCaseStepResult switchToParentFrame() throws TestEngineException;
	
	public void switchToFrameUsingName(String frameName) throws TestEngineException;
	
	public TestCaseStepResult switchToFrameUsingName() throws TestEngineException;
	
	public void switchToFrameByIndex(int frameIndex) throws TestEngineException;
	
	public TestCaseStepResult switchToFrameByIndex() throws TestEngineException;
	
	public TestCaseStepResult verifyElementPresent() throws TestEngineException;
	
	public void verifyElementPresent(LocatorType locatortype, String locator) throws TestEngineException;
	
	public TestCaseStepResult verifyElementAbsent() throws TestEngineException;
	
	public void verifyElementAbsent(LocatorType locatortype, String locator) throws TestEngineException;
	
	public void storeValue(LocatorType locatortype, String locator, String variableName) throws TestEngineException;
	
	public TestCaseStepResult storeValue() throws TestEngineException;
	
	public void storeElementsCount(LocatorType locatortype, String locator, String variableName) throws TestEngineException;
	
	public TestCaseStepResult storeElementsCount() throws TestEngineException;
	
	public void storeSelectedOption(LocatorType locatortype, String locator, String variableName) throws TestEngineException;
	
	public TestCaseStepResult storeSelectedOption() throws TestEngineException;
	
	public void storeOptionsCount(LocatorType locatortype, String locator, String variableName) throws TestEngineException;
	
	public TestCaseStepResult storeOptionsCount() throws TestEngineException;
	
	public void storeCurrentURL(String variableName) throws TestEngineException;
	
	public TestCaseStepResult storeCurrentURL() throws TestEngineException;
	
	public void storeAttribute(LocatorType locatortype, String locator, String attributeName, String variableName) throws TestEngineException;
	
	public TestCaseStepResult storeAttribute() throws TestEngineException;

	public TestCaseStepResult verifySelectedValue() throws TestEngineException;

	public TestCaseStepResult clickLinkWithText() throws TestEngineException;
	
	public TestCaseStepResult typeIntoTextboxWithPlaceholder() throws TestEngineException;

	public TestCaseStepResult clickElementWithText() throws TestEngineException;

	public TestCaseStepResult listBoxWithTitleByValue() throws TestEngineException;

	public TestCaseStepResult listBoxWithTitleByText() throws TestEngineException;

	public TestCaseStepResult listBoxWithTitleByIndex() throws TestEngineException;

	public TestCaseStepResult listBoxWithLabelByValue() throws TestEngineException;

	public TestCaseStepResult listBoxWithLabelByText() throws TestEngineException;

	public TestCaseStepResult listBoxWithLabelByIndex() throws TestEngineException;

	public TestCaseStepResult typeIntoTextAreaWithIndex() throws TestEngineException;

	public TestCaseStepResult typeIntoTextAreaWithPlaceHolder()
			throws TestEngineException;

	public TestCaseStepResult typeIntoTextAreaWithText() throws TestEngineException;

	public TestCaseStepResult typeIntoTextAreaWithLabel() throws TestEngineException;

	public TestCaseStepResult typeIntoTextboxWithIndex() throws TestEngineException;

	public TestCaseStepResult typeIntoTextboxWithText() throws TestEngineException;

	public TestCaseStepResult typeIntoTextboxWithTitle() throws TestEngineException;

	public TestCaseStepResult typeIntoTextboxWithLabel() throws TestEngineException;

	public TestCaseStepResult verifyImageWithAltTitlePresent()
			throws TestEngineException;

	public TestCaseStepResult verifyElementWithTitlePresent()
			throws TestEngineException;

	public TestCaseStepResult verifyButtonWithTitlePresent()
			throws TestEngineException;

	public TestCaseStepResult verifyLinkWithTitlePresent() throws TestEngineException;

	public TestCaseStepResult verifyImageWithAltTextPresent()
			throws TestEngineException;

	public TestCaseStepResult verifyElementWithTextPresent()
			throws TestEngineException;

	public TestCaseStepResult verifyButtonWithTextPresent() throws TestEngineException;

	public TestCaseStepResult verifyElementWithPartialTextPresent()
			throws TestEngineException;

	public TestCaseStepResult verifyButtonWithPartialTextPresent()
			throws TestEngineException;

	public TestCaseStepResult verifyLinkWithPartialTextPresent()
			throws TestEngineException;

	public TestCaseStepResult verifyLinkWithTextPresent() throws TestEngineException;

	public TestCaseStepResult clickButtonWithTitle() throws TestEngineException;

	public TestCaseStepResult clickButtonWithIndex() throws TestEngineException;

	public TestCaseStepResult clickButtonWithText() throws TestEngineException;

	public TestCaseStepResult clickElementWithTitle() throws TestEngineException;

	public TestCaseStepResult clickLinkWithTitle() throws TestEngineException;

	public TestCaseStepResult clickLinkWithIndex() throws TestEngineException;

	public TestCaseStepResult verifyTitleContains() throws TestEngineException;

	public TestCaseStepResult verifyCurrentURLContains() throws TestEngineException;

	public TestCaseStepResult typeIntoPasswordTextboxWithLabel()
			throws TestEngineException;

	public TestCaseStepResult typeIntoPasswordTextboxWithTitle()
			throws TestEngineException;

	public TestCaseStepResult typeIntoPasswordTextboxWithText()
			throws TestEngineException;

	public TestCaseStepResult typeIntoPasswordTextboxWithPlaceholder()
			throws TestEngineException;

	public TestCaseStepResult typeIntoPasswordTextboxWithIndex()
			throws TestEngineException;

	public TestCaseStepResult updateParameterWithElementText() throws TestEngineException;

	public TestCaseStepResult updateParameterWithElementAttribute() throws TestEngineException;

	public TestCaseStepResult typeIntoEmailTextboxWithLabel()
			throws TestEngineException;

	public TestCaseStepResult typeIntoEmailTextboxWithTitle()
			throws TestEngineException;

	public TestCaseStepResult typeIntoEmailTextboxWithText()
			throws TestEngineException;

	public TestCaseStepResult typeIntoEmailTextboxWithPlaceholder()
			throws TestEngineException;

	public TestCaseStepResult typeIntoEmailTextboxWithIndex()
			throws TestEngineException;

	public TestCaseStepResult typeIntoDateTextboxWithLabel()
			throws TestEngineException;

	public TestCaseStepResult typeIntoDateTextboxWithTitle()
			throws TestEngineException;

	public TestCaseStepResult typeIntoDateTextboxWithText() throws TestEngineException;

	public TestCaseStepResult typeIntoDateTextboxWithPlaceholder()
			throws TestEngineException;

	public TestCaseStepResult typeIntoDateTextboxWithIndex()
			throws TestEngineException;

	public TestCaseStepResult checkCheckboxWithLabel() throws TestEngineException;

	public TestCaseStepResult checkCheckboxWithText() throws TestEngineException;

	public TestCaseStepResult checkCheckboxWithTitle() throws TestEngineException;

	public TestCaseStepResult checkCheckboxWithIndex() throws TestEngineException;

	public TestCaseStepResult checkCheckboxWithPartialLabel()
			throws TestEngineException;

	public TestCaseStepResult checkCheckboxWithPartialText()
			throws TestEngineException;

	public TestCaseStepResult checkRadioButtonWithLabel() throws TestEngineException;

	public TestCaseStepResult checkRadioButtonWithText() throws TestEngineException;

	public TestCaseStepResult checkRadioButtonWithTitle() throws TestEngineException;

	public TestCaseStepResult checkRadioButtonWithIndex() throws TestEngineException;

	public TestCaseStepResult checkRadioButtonWithPartialLabel()
			throws TestEngineException;

	public TestCaseStepResult checkRadioButtonWithPartialText()
			throws TestEngineException;

	public TestCaseStepResult waitUntilLinkWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilLinkWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilElementWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilButtonWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilButtonWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilElementWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilLinkWithPartialTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilButtonWithPartialTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilElementWithPartialTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilImageWithAltTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilImageWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilTextboxWithLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilTextboxWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilTextboxWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilTextboxWithPlaceholderIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilPasswordTextboxWithLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilPasswordTextboxWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilPasswordTextboxWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilPasswordTextboxWithPlaceholderIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilEmailTextboxWithLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilEmailTextboxWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilEmailTextboxWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilEmailTextboxWithPlaceholderIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilDateTextboxWithLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilDateTextboxWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilDateTextboxWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilDateTextboxWithPlaceholderIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilTextareaWithLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilTextareaWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilTextareaWithPlaceholderIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilListboxWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilListboxWithLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilListboxWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilCheckboxWithLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilCheckboxWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilCheckboxWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilCheckboxWithPartialLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilCheckboxWithPartialTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilRadioButtonWithLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilRadioButtonWithTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilRadioButtonWithTitleIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilRadioButtonWithPartialLabelIsVisible()
			throws TestEngineException;

	public TestCaseStepResult waitUntilRadioButtonWithPartialTextIsVisible()
			throws TestEngineException;

	public TestCaseStepResult listBoxWithTextByIndex() throws TestEngineException;

	public TestCaseStepResult listBoxWithTextByVisbleText() throws TestEngineException;

	public TestCaseStepResult listBoxWithTextByValue() throws TestEngineException;

	public TestCaseStepResult waitUntilPageTitleContains() throws TestEngineException;
	
	public TestCaseStepResult waitUntilCurrentURLIs() throws TestEngineException;

	public TestCaseStepResult waitUntilCurrentURLContains() throws TestEngineException;

	public TestCaseStepResult typeIntoElementWithLabel() throws TestEngineException;

	public TestCaseStepResult typeIntoElementWithTitle() throws TestEngineException;

	public TestCaseStepResult typeIntoElementWithText() throws TestEngineException;

	public TestCaseStepResult typeIntoElementWithPlaceholder()
			throws TestEngineException;

	public TestCaseStepResult switchToWindowByTitleContaining()
			throws TestEngineException;
	
	public TestCaseStepResult typeIntoTextboxWithLabelContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoTextboxWithTextContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoPasswordTextboxWithLabelContaining() throws TestEngineException;
	
	public TestCaseStepResult typeIntoPasswordTextboxWithTextContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoEmailTextboxWithLabelContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoEmailTextboxWithTextContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoDateTextboxWithLabelContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoDateTextboxWithTextContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoElementWithLabelContaining() throws TestEngineException;
	
	public TestCaseStepResult typeIntoElementWithTextContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoTextAreaWithLabelContaining() throws TestEngineException;

	public TestCaseStepResult typeIntoTextAreaWithTextContaining() throws TestEngineException;
	
	public TestCaseStepResult clickLinkWithTextContaining() throws TestEngineException;
	
	public TestCaseStepResult clickElementWithTextContaining() throws TestEngineException;

	public TestCaseStepResult listBoxWithPartialLabelByIndex() throws TestEngineException;

	public TestCaseStepResult listBoxWithPartialLabelByText() throws TestEngineException;
	
	public TestCaseStepResult listBoxWithPartialLabelByValue() throws TestEngineException;
	
	public TestCaseStepResult listBoxWithPartialTextByIndex() throws TestEngineException;

	public TestCaseStepResult listBoxWithPartialTextByVisbleText() throws TestEngineException;

	public TestCaseStepResult listBoxWithPartialTextByValue() throws TestEngineException;

	public TestCaseStepResult clickElementRowWithText() throws TestEngineException;

	public TestCaseStepResult clickElementRowWithTextContaining() throws TestEngineException;

	void clickUntilTextVisible() throws TestEngineException;
	
}
